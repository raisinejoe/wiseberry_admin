<?php

namespace App\Http\Controllers\Backend\Pages;

use App\Http\Controllers\Controller;
use App\Http\Requests\Backend\Pages\CreatePageRequest;
use App\Http\Requests\Backend\Pages\DeletePageRequest;
use App\Http\Requests\Backend\Pages\EditPageRequest;
use App\Http\Requests\Backend\Pages\ManagePageRequest;
use App\Http\Requests\Backend\Pages\StorePageRequest;
use App\Http\Requests\Backend\Pages\UpdatePageRequest;
use App\Http\Responses\Backend\Page\EditResponse;
use App\Http\Responses\RedirectResponse;
use App\Http\Responses\ViewResponse;
use App\Models\Page;
use App\Repositories\Backend\PagesRepository;
use Illuminate\Support\Facades\View;

class PagesController extends Controller
{
    /**
     * @var \App\Repositories\Backend\PagesRepository
     */
    protected $repository;

    /**
     * @param \App\Repositories\Backend\PagesRepository $repository
     */
    public function __construct(PagesRepository $repository)
    {
        $this->repository = $repository;
        View::share('js', ['pages']);
    }

    /**
     * @param \App\Http\Requests\Backend\Pages\ManagePageRequest $request
     *
     * @return \App\Http\Responses\ViewResponse
     */
    public function index(ManagePageRequest $request)
    {
        return new ViewResponse('backend.pages.index');
    }

    /**
     * @param \App\Http\Requests\Backend\Pages\CreatePageRequest $request
     *
     * @return \App\Http\Responses\ViewResponse
     */
    public function create(CreatePageRequest $request)
    {
        // return new ViewResponse('backend.pages.create');
        return view('backend.pages.create');
    }

    /**
     * @param \App\Http\Requests\Backend\Pages\StorePageRequest $request
     *
     * @return \App\Http\Responses\RedirectResponse
     */
    public function store(StorePageRequest $request)
    {
        $request->validate([
            'image' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:5120',
        ]);
        $request->validate([
            'image_ofc' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:5120',
        ]);
    
        $imageName = time().'.'.$request->image->extension();  
        $imageName_ofc = 'ofc_'.time().'.'.$request->image_ofc->extension();  
     
        $request->image->move(public_path('images'), $imageName);
        $request->image_ofc->move(public_path('images'), $imageName_ofc);
        $request['imagename']=$imageName;
        $request['imagename_ofc']=$imageName_ofc;
        // print_r($request);die;
        $this->repository->create($request->except(['_token', '_method']));

        return new RedirectResponse(route('admin.pages.index'), ['flash_success' => __('alerts.backend.pages.created')]);
    }

    /**
     * @param \App\Models\Page $page
     * @param \App\Http\Requests\Backend\Pages\EditPageRequest $request
     *
     * @return \App\Http\Responses\Backend\Blog\EditResponse
     */
    public function edit(Page $page, EditPageRequest $request)

    {
        // $page['url']="http://localhost/wiseberry_new/public/images/";
        $page['url']="https://szwebprofile.com/PHP/wiseberry_admin/public/images/";
        if($page['id'] == '2'){
            $pagedata=json_decode($page['pagedata']);
            $data=$pagedata->why;
            $data_ofc=$pagedata->ofc;
            $data_fp=$pagedata->fp;
            $data_fr=$pagedata->fr;

            return view('backend.pages.edit',compact('data','pagedata','page','data_ofc','data_fp','data_fr'));
        }else if($page['id'] == '4'){
            $pagedata=json_decode($page['pagedata']);
            // echo "<pre>";print_r($pagedata);echo "</pre>";die;
            return view('backend.pages.edit',compact('page','pagedata'));
        }else if($page['id'] == '5'){
            $pagedata=json_decode($page['pagedata']);
            $data=$pagedata->wf;
            $data_wmm=$pagedata->wmm;
            return view('backend.pages.edit',compact('page','pagedata','data','data_wmm'));
        }else if($page['id'] == '6'){
            $pagedata=json_decode($page['pagedata']);
            $data=$pagedata->p1;
            $data_p2=$pagedata->p2;
            return view('backend.pages.edit',compact('page','pagedata','data','data_p2'));
        }else if($page['id'] == '7'){
            $pagedata=json_decode($page['pagedata']);
            $data=$pagedata->p1;
            $data_p2=$pagedata->p2;
            $data_p3=$pagedata->p3;
            return view('backend.pages.edit',compact('page','pagedata','data','data_p2','data_p3'));
        }else if($page['id'] == '8'){
            $pagedata=json_decode($page['pagedata']);
            return view('backend.pages.edit',compact('page','pagedata'));
        }else if($page['id'] == '9'){
            $pagedata=json_decode($page['pagedata']);
            return view('backend.pages.edit',compact('page','pagedata'));
        }else if($page['id'] == '10'){
            $pagedata=json_decode($page['pagedata']);
            $data_p1=$pagedata->p1;
            $data_p2=$pagedata->p2;
            $data_p3=$pagedata->p3;
            return view('backend.pages.edit',compact('page','pagedata','data_p1','data_p2','data_p3'));
        }else if($page['id'] == '11'){
            $pagedata=json_decode($page['pagedata']);
            return view('backend.pages.edit',compact('page','pagedata'));
        }else if($page['id'] == '12'){
            $pagedata=json_decode($page['pagedata']);
            $data_p2=$pagedata->p2;
            return view('backend.pages.edit',compact('page','pagedata','data_p2'));
        }else if($page['id'] == '13'){
            $pagedata=json_decode($page['pagedata']);
            return view('backend.pages.edit',compact('page','pagedata'));
        }else if($page['id'] == '14'){
            $pagedata=json_decode($page['pagedata']);
            return view('backend.pages.edit',compact('page','pagedata'));
        }else if($page['id'] == '15'){
            $pagedata=json_decode($page['pagedata']);
            return view('backend.pages.edit',compact('page','pagedata'));
        }else if($page['id'] == '16'){
            $pagedata=json_decode($page['pagedata']);
            return view('backend.pages.edit',compact('page','pagedata'));
        }else if($page['id'] == '17'){
            $pagedata=json_decode($page['pagedata']);
            return view('backend.pages.edit',compact('page','pagedata'));
        }else if($page['id'] == '18'){
            $pagedata=json_decode($page['pagedata']);
            return view('backend.pages.edit',compact('page','pagedata'));
        }else if($page['id'] == '19'){
            $pagedata=json_decode($page['pagedata']);
            $data=$pagedata->p1;
            return view('backend.pages.edit',compact('page','pagedata','data'));
        }else if($page['id'] == '20'){
            $pagedata=json_decode($page['pagedata']);
            return view('backend.pages.edit',compact('page','pagedata'));
        }else if($page['id'] == '21'){
            $pagedata=json_decode($page['pagedata']);
            $data=$pagedata->p1;
            $data_p2=$pagedata->p2;
            return view('backend.pages.edit',compact('page','pagedata','data','data_p2'));
        }else if($page['id'] == '22'){
            $pagedata=json_decode($page['pagedata']);
            $data=$pagedata->p1;
            $data_p2=$pagedata->p2;
            return view('backend.pages.edit',compact('page','pagedata','data','data_p2'));
        }else if($page['id'] == '23'){
            $pagedata=json_decode($page['pagedata']);
            return view('backend.pages.edit',compact('page','pagedata'));
        }else if($page['id'] == '24'){
            $pagedata=json_decode($page['pagedata']);
            return view('backend.pages.edit',compact('page','pagedata'));
        }else if($page['id'] == '25'){
            $pagedata=json_decode($page['pagedata']);
            $data=$pagedata->p1;
            $data_p2=$pagedata->p2;
            $data_p3=$pagedata->p3;
            return view('backend.pages.edit',compact('page','pagedata','data','data_p2','data_p3'));
        }else if($page['id'] == '26'){
            $pagedata=json_decode($page['pagedata']);
            return view('backend.pages.edit',compact('page','pagedata'));
        }else if($page['id'] == '27'){
            $pagedata=json_decode($page['pagedata']);
            return view('backend.pages.edit',compact('page','pagedata'));
        }else if($page['id'] == '28'){
            $pagedata=json_decode($page['pagedata']);
            return view('backend.pages.edit',compact('page','pagedata'));
        }else if($page['id'] == '29'){
            $pagedata=json_decode($page['pagedata']);
            $data_p1=$pagedata->p1;
            $data_p2=$pagedata->p2;
            $data_p3=$pagedata->p3;
            $data_p4=$pagedata->p4;
            return view('backend.pages.edit',compact('page','pagedata','data_p1','data_p2','data_p3','data_p4'));
        }else if($page['id'] == '30'){
            $pagedata=json_decode($page['pagedata']);
            $data_p1=$pagedata->p1;
            return view('backend.pages.edit',compact('page','pagedata','data_p1'));
        }else if($page['id'] == '31'){
            $pagedata=json_decode($page['pagedata']);
            return view('backend.pages.edit',compact('page','pagedata'));
        }else if($page['id'] == '32'){
            $pagedata=json_decode($page['pagedata']);
            return view('backend.pages.edit',compact('page','pagedata'));
        }else if($page['id'] == '33'){
            $pagedata=json_decode($page['pagedata']);
            return view('backend.pages.edit',compact('page','pagedata'));
        }else if($page['id'] == '34'){
            $pagedata=json_decode($page['pagedata']);
            return view('backend.pages.edit',compact('page','pagedata'));
        }else if($page['id'] == '35'){
            $pagedata=json_decode($page['pagedata']);
            $data_p1=$pagedata->p1;
            $data_p2=$pagedata->p2;
            $data_p3=$pagedata->p3;
            $data_p4=$pagedata->p4;
            $data_p5=$pagedata->p5;
            return view('backend.pages.edit',compact('page','pagedata','data_p1','data_p2','data_p3','data_p4','data_p5'));
        }else if($page['id'] == '36'){
            $pagedata=json_decode($page['pagedata']);
            return view('backend.pages.edit',compact('page','pagedata'));
        }else if($page['id'] == '37'){
            $pagedata=json_decode($page['pagedata']);
            return view('backend.pages.edit',compact('page','pagedata'));
        }else if($page['id'] == '38'){
            $pagedata=json_decode($page['pagedata']);
            $data=$pagedata->p1;
            $data_p2=$pagedata->p2;
            $data_p3=$pagedata->p3;
            $data_p4=$pagedata->p4;
            return view('backend.pages.edit',compact('page','pagedata','data','data_p2','data_p3','data_p4'));
        }else if($page['id'] == '39'){
            $pagedata=json_decode($page['pagedata']);
            $data_p1=$pagedata->p1;
            $data_p2=$pagedata->p2;
            return view('backend.pages.edit',compact('page','pagedata','data_p1','data_p2'));
        }else if($page['id'] == '40'){
            $pagedata=json_decode($page['pagedata']);
            return view('backend.pages.edit',compact('page','pagedata'));
        }else if($page['id'] == '42'){
            $pagedata=json_decode($page['pagedata']);
            return view('backend.pages.edit',compact('page','pagedata'));
        }else if($page['id'] == '43'){
            $pagedata=json_decode($page['pagedata']);
            return view('backend.pages.edit',compact('page','pagedata'));
        }
        
        // return new EditResponse($page);
    }

    /**
     * @param \App\Models\Page $page
     * @param \App\Http\Requests\Backend\Pages\UpdatePageRequest $request
     *
     * @return \App\Http\Responses\RedirectResponse
     */
    public function update(Page $page, UpdatePageRequest $request)
    {

        if($page['id'] == '2'){
            if(isset($request->image)){
                $request->validate([
                'image' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:5120',
                ]);
                $imageName = time().'.'.$request->image->extension(); 
                $request->image->move(public_path('images'), $imageName);
                $request['imagename']=$imageName;
            }else{
                $request['imagename']=$request->image_1;
            }
            
            if(isset($request->image_ofc)){
                $request->validate([
                    'image_ofc' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:5120',
                ]);
                $imageName_ofc = 'ofc_'.time().'.'.$request->image_ofc->extension();   
                $request->image_ofc->move(public_path('images'), $imageName_ofc);
                $request['imagename_ofc']=$imageName_ofc;

            }else{
                $request['imagename_ofc']=$request->image_2;
            }

            if(isset($request->image_banner)){
                $request->validate([
                    'image_banner' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:5120',
                ]);
                $imageName_banner = 'b_'.time().'.'.$request->image_banner->extension();   
                $request->image_banner->move(public_path('images'), $imageName_banner);
                $request['imagename_banner']=$imageName_banner;

            }else{
                $request['imagename_banner']=$request->image_3;
            }

            if(isset($request->image_fp1)){
                $request->validate([
                    'image_fp1' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:5120',
                ]);
                $imageName_fp1 = 'fp1_'.time().'.'.$request->image_fp1->extension();   
                $request->image_fp1->move(public_path('images'), $imageName_fp1);
                $request['imagename_fp1']=$imageName_fp1;

                // print_r($imageName_fp1);die;

            }else{
                $request['imagename_fp1']=$request->image_4;
            }

            if(isset($request->image_fp2)){
                $request->validate([
                    'image_fp2' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:5120',
                ]);
                $imageName_fp2 = 'fp2_'.time().'.'.$request->image_fp2->extension();   
                $request->image_fp2->move(public_path('images'), $imageName_fp2);
                $request['imagename_fp2']=$imageName_fp2;

            }else{
                $request['imagename_fp2']=$request->image_5;
            }

            if(isset($request->image_fp3)){
                $request->validate([
                    'image_fp3' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:5120',
                ]);
                $imageName_fp3 = 'fp3_'.time().'.'.$request->image_fp3->extension();   
                $request->image_fp3->move(public_path('images'), $imageName_fp3);
                $request['imagename_fp3']=$imageName_fp3;

            }else{
                $request['imagename_fp3']=$request->image_6;
            }



            if(isset($request->image_fr1)){
                $request->validate([
                    'image_fr1' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:5120',
                ]);
                $imageName_fr1 = 'fr1_'.time().'.'.$request->image_fr1->extension();   
                $request->image_fr1->move(public_path('images'), $imageName_fr1);
                $request['imagename_fr1']=$imageName_fr1;

                // print_r($imageName_fp1);die;

            }else{
                $request['imagename_fr1']=$request->image_7;
            }

            if(isset($request->image_fr2)){
                $request->validate([
                    'image_fr2' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:5120',
                ]);
                $imageName_fr2 = 'fr2_'.time().'.'.$request->image_fr2->extension();   
                $request->image_fr2->move(public_path('images'), $imageName_fr2);
                $request['imagename_fr2']=$imageName_fr2;

            }else{
                $request['imagename_fr2']=$request->image_8;
            }

            if(isset($request->image_fr3)){
                $request->validate([
                    'image_fr3' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:5120',
                ]);
                $imageName_fr3 = 'fr3_'.time().'.'.$request->image_fr3->extension();   
                $request->image_fr3->move(public_path('images'), $imageName_fr3);
                $request['imagename_fr3']=$imageName_fr3;

            }else{
                $request['imagename_fr3']=$request->image_9;
            }





            $this->repository->update($page, $request->except(['_token', '_method']));
        }else if($page['id'] == '5'){

            if(isset($request->image_banner)){
                $request->validate([
                    'image_banner' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:5120',
                ]);
                $imageName_banner = time().'.'.$request->image_banner->extension(); 
                $request->image_banner->move(public_path('images'), $imageName_banner);
                $request['imagename_banner']=$imageName_banner;

                
            }else{
                $request['imagename_banner']=$request->image_3;
            }
            
            if(isset($request->image)){
                $request->validate([
                    'image' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:5120',
                ]);
                $imageName = time().'.'.$request->image->extension(); 
                $request->image->move(public_path('images'), $imageName);
                $request['imagename']=$imageName;

                
            }else{
                $request['imagename']=$request->image_1;
            }
            
            if(isset($request->image_ofc)){
                $request->validate([
                        'image_ofc' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:5120',
                    ]);
                $imageName_ofc = 'wmm_'.time().'.'.$request->image_ofc->extension();   
             
                $request->image_ofc->move(public_path('images'), $imageName_ofc);
                
                $request['imagename_ofc']=$imageName_ofc;
            }else{
                $request['imagename_ofc']=$request->image_2;
            }


            $this->repository->update_buy_search($page, $request->except(['_token', '_method']));
        }else if($page['id'] == '6'){
            
            if(isset($request->image_banner)){
                $request->validate([
                    'image_banner' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:5120',
                ]);
                $imageName_banner = time().'.'.$request->image_banner->extension(); 
                $request->image_banner->move(public_path('images'), $imageName_banner);
                $request['imagename_banner']=$imageName_banner;

                
            }else{
                $request['imagename_banner']=$request->image_3;
            }
            
            
            if(isset($request->image)){
                $request->validate([
                    'image' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:5120',
                ]);
                $imageName = time().'.'.$request->image->extension(); 
                $request->image->move(public_path('images'), $imageName);
                $request['imagename']=$imageName;

                
            }else{
                $request['imagename']=$request->image_1;
            }
            
            if(isset($request->image_ofc)){
                $request->validate([
                        'image_ofc' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:5120',
                    ]);
                $imageName_ofc = 'wmm_'.time().'.'.$request->image_ofc->extension();   
             
                $request->image_ofc->move(public_path('images'), $imageName_ofc);
                
                $request['imagename_ofc']=$imageName_ofc;
            }else{
                $request['imagename_ofc']=$request->image_2;
            }

            $this->repository->update_open_homes($page, $request->except(['_token', '_method']));
        }else if($page['id'] == '7'){
            if(isset($request->image_banner)){
                $request->validate([
                    'image_banner' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:5120',
                ]);
                $imageName_banner = time().'.'.$request->image_banner->extension(); 
                $request->image_banner->move(public_path('images'), $imageName_banner);
                $request['imagename_banner']=$imageName_banner;

                
            }else{
                $request['imagename_banner']=$request->image_4;
            }
            
            
            if(isset($request->image)){
                $request->validate([
                    'image' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:5120',
                ]);
                $imageName = time().'.'.$request->image->extension(); 
                $request->image->move(public_path('images'), $imageName);
                $request['imagename']=$imageName;
            }else{
                $request['imagename']=$request->image_1;
            }

            if(isset($request->image_p2)){
                $request->validate([
                    'image_p2' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:5120',
                ]);
                $imageName_p2 = 'p2_'.time().'.'.$request->image_p2->extension();
                $request->image_p2->move(public_path('images'), $imageName_p2);
                $request['imagename_p2']=$imageName_p2;
            }else{
                $request['imagename_p2']=$request->image_2;
            }

            if(isset($request->image_p3)){
                $request->validate([
                    'image_p3' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:5120',
                ]);
                $imageName_p3 = 'p3_'.time().'.'.$request->image_p3->extension();   
             
                $request->image_p3->move(public_path('images'), $imageName_p3);
                
                $request['imagename_p3']=$imageName_p3;
            }else{
                $request['imagename_p3']=$request->image_3;
            }
            
            
            // print_r($request);die;


            $this->repository->update_auctions($page, $request->except(['_token', '_method']));
        }else if($page['id'] == '8'){

            if(isset($request->image)){
                $request->validate([
                    'image' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:5120',
                ]);
                $imageName = time().'.'.$request->image->extension(); 
                $request->image->move(public_path('images'), $imageName);
                $request['imagename']=$imageName;
                
            }else{
                $request['imagename']=$request->image_1;
            }
            

            $this->repository->update_off_market($page, $request->except(['_token', '_method']));
        }else if($page['id'] == '9'){

            if(isset($request->image)){
                $request->validate([
                    'image' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:5120',
                ]);
                $imageName = time().'.'.$request->image->extension(); 
                $request->image->move(public_path('images'), $imageName);
                $request['imagename']=$imageName;
                
            }else{
                $request['imagename']=$request->image_1;
            }
            

            $this->repository->update_wisemove($page, $request->except(['_token', '_method']));
        }else if($page['id'] == '10'){
            if(isset($request->image_banner)){
                $request->validate([
                    'image_banner' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:5120',
                ]);
                $imageName_banner = time().'.'.$request->image_banner->extension(); 
                $request->image_banner->move(public_path('images'), $imageName_banner);
                $request['imagename_banner']=$imageName_banner;

                
            }else{
                $request['imagename_banner']=$request->image_3;
            }
            
            
            if(isset($request->image_p1)){
                $request->validate([
                'image_p1' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:5120',
                ]);
                $imageName_p1 = time().'.'.$request->image_p1->extension(); 
                $request->image_p1->move(public_path('images'), $imageName_p1);
                $request['imagename_p1']=$imageName_p1;
            }else{
                $request['imagename_p1']=$request->image_1;
            }
            
            if(isset($request->image_p2)){
                $request->validate([
                    'image_p2' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:5120',
                ]);
                $imageName_p2 = 'p2_'.time().'.'.$request->image_p2->extension();   
                $request->image_p2->move(public_path('images'), $imageName_p2);
                $request['imagename_p2']=$imageName_p2;

            }else{
                $request['imagename_p2']=$request->image_2;
            }


            $this->repository->update_finance($page, $request->except(['_token', '_method']));
        }else if($page['id'] == '11'){
            if(isset($request->head_image)){
                $request->validate([
                'head_image' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:5120',
                ]);
                $imageName_p1 = time().'.'.$request->head_image->extension(); 
                $request->head_image->move(public_path('images'), $imageName_p1);
                $request['imagename_p1']=$imageName_p1;
            }else{
                $request['imagename_p1']=$request->image_1;
            }
            
            if(isset($request->image)){
                $request->validate([
                    'image' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:5120',
                ]);
                $imageName = 'p2_'.time().'.'.$request->image->extension();   
                $request->image->move(public_path('images'), $imageName);
                $request['imagename']=$imageName;

            }else{
                $request['imagename']=$request->image_2;
            }


            $this->repository->update_rent($page, $request->except(['_token', '_method']));
        }else if($page['id'] == '12'){
            
            if(isset($request->image_ofc)){
                $request->validate([
                        'image_ofc' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:5120',
                    ]);
                $imageName_ofc = 'wmm_'.time().'.'.$request->image_ofc->extension();   
             
                $request->image_ofc->move(public_path('images'), $imageName_ofc);
                
                $request['imagename_2']=$imageName_ofc;
            }else{
                $request['imagename_2']=$request->image_2;
            }
            
            
            if(isset($request->head_image)){
                $request->validate([
                        'head_image' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:5120',
                    ]);
                $imageName_3 = 'wmm_'.time().'.'.$request->head_image->extension();   
             
                $request->head_image->move(public_path('images'), $imageName_3);
                
                $request['imagename_3']=$imageName_3;
            }else{
                $request['imagename_3']=$request->image_3;
            }

            $this->repository->update_rent_open_homes($page, $request->except(['_token', '_method']));
        }else if($page['id'] == '13'){

            if(isset($request->image)){
                $request->validate([
                    'image' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:5120',
                ]);
                $imageName = time().'.'.$request->image->extension(); 
                $request->image->move(public_path('images'), $imageName);
                $request['imagename']=$imageName;
                
            }else{
                $request['imagename']=$request->image_1;
            }
            

            $this->repository->update_privacy_policy($page, $request->except(['_token', '_method']));
        }else if($page['id'] == '14'){

            if(isset($request->image)){
                $request->validate([
                    'image' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:5120',
                ]);
                $imageName = time().'.'.$request->image->extension(); 
                $request->image->move(public_path('images'), $imageName);
                $request['imagename']=$imageName;
                
            }else{
                $request['imagename']=$request->image_1;
            }
            

            $this->repository->update_terms($page, $request->except(['_token', '_method']));
        }else if($page['id'] == '15'){

            if(isset($request->image)){
                $request->validate([
                    'image' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:5120',
                ]);
                $imageName = time().'.'.$request->image->extension(); 
                $request->image->move(public_path('images'), $imageName);
                $request['imagename']=$imageName;
                
            }else{
                $request['imagename']=$request->image_1;
            }
            

            $this->repository->update_search_result($page, $request->except(['_token', '_method']));
        }else if($page['id'] == '16'){

            if(isset($request->image)){
                $request->validate([
                    'image' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:5120',
                ]);
                $imageName = time().'.'.$request->image->extension(); 
                $request->image->move(public_path('images'), $imageName);
                $request['imagename']=$imageName;
                
            }else{
                $request['imagename']=$request->image_1;
            }
            

            $this->repository->update_open_home_result($page, $request->except(['_token', '_method']));
        }else if($page['id'] == '17'){

            if(isset($request->image)){
                $request->validate([
                    'image' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:5120',
                ]);
                $imageName = time().'.'.$request->image->extension(); 
                $request->image->move(public_path('images'), $imageName);
                $request['imagename']=$imageName;
                
            }else{
                $request['imagename']=$request->image_1;
            }
            

            $this->repository->update_auction_result($page, $request->except(['_token', '_method']));
        }else if($page['id'] == '18'){

            if(isset($request->image)){
                $request->validate([
                    'image' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:5120',
                ]);
                $imageName = time().'.'.$request->image->extension(); 
                $request->image->move(public_path('images'), $imageName);
                $request['imagename']=$imageName;
                
            }else{
                $request['imagename']=$request->image_1;
            }
            

            $this->repository->update_rent_search_result($page, $request->except(['_token', '_method']));
        }else if($page['id'] == '19'){
            
            if(isset($request->image_banner)){
                $request->validate([
                    'image_banner' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:5120',
                ]);
                $imageName_banner = time().'.'.$request->image_banner->extension(); 
                $request->image_banner->move(public_path('images'), $imageName_banner);
                $request['imagename_banner']=$imageName_banner;

                
            }else{
                $request['imagename_banner']=$request->image_3;
            }
            
            
            if(isset($request->image)){
                $request->validate([
                    'image' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:5120',
                ]);
                $imageName = time().'.'.$request->image->extension(); 
                $request->image->move(public_path('images'), $imageName);
                $request['imagename']=$imageName;

                
            }else{
                $request['imagename']=$request->image_1;
            }
            

            $this->repository->update_ind_rental($page, $request->except(['_token', '_method']));
        }else if($page['id'] == '20'){

            if(isset($request->image)){
                $request->validate([
                    'image' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:5120',
                ]);
                $imageName = time().'.'.$request->image->extension(); 
                $request->image->move(public_path('images'), $imageName);
                $request['imagename']=$imageName;
                
            }else{
                $request['imagename']=$request->image_1;
            }
            

            $this->repository->update_rent_open_home_result($page, $request->except(['_token', '_method']));
        }else if($page['id'] == '21'){

            if(isset($request->banner_image)){
                $request->validate([
                    'banner_image' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:5120',
                ]);
                $imageName_banner = time().'.'.$request->banner_image->extension(); 
                $request->banner_image->move(public_path('images'), $imageName_banner);
                $request['imagename_banner']=$imageName_banner;
                
            }else{
                $request['imagename_banner']=$request->image_2;
            }
            
            if(isset($request->image)){
                $request->validate([
                    'image' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:5120',
                ]);
                $imageName = time().'.'.$request->image->extension(); 
                $request->image->move(public_path('images'), $imageName);
                $request['imagename']=$imageName;
                
            }else{
                $request['imagename']=$request->image_1;
            }
            

            $this->repository->update_why_rent($page, $request->except(['_token', '_method']));
        }else if($page['id'] == '22'){
            if(isset($request->image_banner)){
                $request->validate([
                    'image_banner' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:5120',
                ]);
                $imageName_banner = time().'.'.$request->image_banner->extension(); 
                $request->image_banner->move(public_path('images'), $imageName_banner);
                $request['imagename_banner']=$imageName_banner;

                
            }else{
                $request['imagename_banner']=$request->image_3;
            }
            
            
            if(isset($request->image_p1)){
                $request->validate([
                'image_p1' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:5120',
                ]);
                $imageName_p1 = time().'.'.$request->image_p1->extension(); 
                $request->image_p1->move(public_path('images'), $imageName_p1);
                $request['imagename_p1']=$imageName_p1;
            }else{
                $request['imagename_p1']=$request->image_1;
            }
            
            

            $this->repository->update_rent_applying($page, $request->except(['_token', '_method']));
        }else if($page['id'] == '23'){

            if(isset($request->image)){
                $request->validate([
                    'image' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:5120',
                ]);
                $imageName = time().'.'.$request->image->extension(); 
                $request->image->move(public_path('images'), $imageName);
                $request['imagename']=$imageName;
                
            }else{
                $request['imagename']=$request->image_1;
            }
            

            $this->repository->update_notice_to_repair($page, $request->except(['_token', '_method']));
        }else if($page['id'] == '24'){

            if(isset($request->image)){
                $request->validate([
                    'image' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:5120',
                ]);
                $imageName = time().'.'.$request->image->extension(); 
                $request->image->move(public_path('images'), $imageName);
                $request['imagename']=$imageName;
                
            }else{
                $request['imagename']=$request->image_1;
            }
            

            $this->repository->update_notice_to_vacate($page, $request->except(['_token', '_method']));
        }else if($page['id'] == '25'){

            if(isset($request->banner_image)){
                $request->validate([
                    'banner_image' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:5120',
                ]);
                $imageName_banner = time().'.'.$request->banner_image->extension(); 
                $request->banner_image->move(public_path('images'), $imageName_banner);
                $request['imagename_banner']=$imageName_banner;
                
            }else{
                $request['imagename_banner']=$request->image_2;
            }
            
            if(isset($request->image)){
                $request->validate([
                    'image' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:5120',
                ]);
                $imageName = time().'.'.$request->image->extension(); 
                $request->image->move(public_path('images'), $imageName);
                $request['imagename']=$imageName;
                
            }else{
                $request['imagename']=$request->image_1;
            }

            if(isset($request->image_p3)){
                $request->validate([
                    'image_p3' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:5120',
                ]);
                $imageName_p3 = time().'.'.$request->image_p3->extension(); 
                $request->image_p3->move(public_path('images'), $imageName_p3);
                $request['imagename_p3']=$imageName_p3;
                
            }else{
                $request['imagename_p3']=$request->image_3;
            }
            

            $this->repository->update_sell_with_us($page, $request->except(['_token', '_method']));
        }else if($page['id'] == '26'){

            if(isset($request->image)){
                $request->validate([
                    'image' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:5120',
                ]);
                $imageName = time().'.'.$request->image->extension(); 
                $request->image->move(public_path('images'), $imageName);
                $request['imagename']=$imageName;
                
            }else{
                $request['imagename']=$request->image_1;
            }
            

            $this->repository->update_appraisal($page, $request->except(['_token', '_method']));
        }else if($page['id'] == '27'){
            if(isset($request->head_image)){
                $request->validate([
                'head_image' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:5120',
                ]);
                $imageName_p1 = time().'.'.$request->head_image->extension(); 
                $request->head_image->move(public_path('images'), $imageName_p1);
                $request['imagename_p1']=$imageName_p1;
            }else{
                $request['imagename_p1']=$request->image_1;
            }
            
            if(isset($request->image)){
                $request->validate([
                    'image' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:5120',
                ]);
                $imageName = 'p2_'.time().'.'.$request->image->extension();   
                $request->image->move(public_path('images'), $imageName);
                $request['imagename']=$imageName;

            }else{
                $request['imagename']=$request->image_2;
            }


            $this->repository->update_recently_sold($page, $request->except(['_token', '_method']));
        }else if($page['id'] == '28'){

            if(isset($request->image)){
                $request->validate([
                    'image' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:5120',
                ]);
                $imageName = time().'.'.$request->image->extension(); 
                $request->image->move(public_path('images'), $imageName);
                $request['imagename']=$imageName;
                
            }else{
                $request['imagename']=$request->image_1;
            }
            

            $this->repository->update_recently_sold_results($page, $request->except(['_token', '_method']));
        }else if($page['id'] == '29'){
            // print_r($request);die;
            if(isset($request->image_banner)){
                $request->validate([
                    'image_banner' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:5120',
                ]);
                $imageName_banner = 'p3_'.time().'.'.$request->image_banner->extension(); 
                $request->image_banner->move(public_path('images'), $imageName_banner);
                $request['imagename_banner']=$imageName_banner;

                
            }else{
                $request['imagename_banner']=$request->image_3;
            }
            
            
            if(isset($request->image_p1)){
                $request->validate([
                'image_p1' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:5120',
                ]);
                $imageName_p1 = time().'.'.$request->image_p1->extension(); 
                $request->image_p1->move(public_path('images'), $imageName_p1);
                $request['imagename_p1']=$imageName_p1;
            }else{
                $request['imagename_p1']=$request->image_1;
            }
            
            if(isset($request->image_p2)){
                $request->validate([
                    'image_p2' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:5120',
                ]);
                $imageName_p2 = 'p2_'.time().'.'.$request->image_p2->extension();   
                $request->image_p2->move(public_path('images'), $imageName_p2);
                $request['imagename_p2']=$imageName_p2;

            }else{
                $request['imagename_p2']=$request->image_2;
            }

            if(isset($request->image_p4)){
                $request->validate([
                    'image_p4' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:5120',
                ]);
                $imageName_p4 = 'p4_'.time().'.'.$request->image_p4->extension();   
                $request->image_p4->move(public_path('images'), $imageName_p4);
                $request['imagename_p4']=$imageName_p4;

            }else{
                $request['imagename_p4']=$request->image_4;
            }





            $this->repository->update_manage_with_us($page, $request->except(['_token', '_method']));
        }else if($page['id'] == '30'){

            if(isset($request->image)){
                $request->validate([
                    'image' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:5120',
                ]);
                $imageName = time().'.'.$request->image->extension(); 
                $request->image->move(public_path('images'), $imageName);
                $request['imagename']=$imageName;
                
            }else{
                $request['imagename']=$request->image_1;
            }

            if(isset($request->image_p1)){
                $request->validate([
                    'image_p1' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:5120',
                ]);
                $imageName_p1 = 'p1_'.time().'.'.$request->image_p1->extension(); 
                $request->image_p1->move(public_path('images'), $imageName_p1);
                $request['imagename_p1']=$imageName_p1;
                
            }else{
                $request['imagename_p1']=$request->image_1;
            }
            

            $this->repository->update_rental_appraisal($page, $request->except(['_token', '_method']));
        }else if($page['id'] == '31'){
            if(isset($request->head_image)){
                $request->validate([
                'head_image' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:5120',
                ]);
                $imageName_p1 = time().'.'.$request->head_image->extension(); 
                $request->head_image->move(public_path('images'), $imageName_p1);
                $request['imagename_p1']=$imageName_p1;
            }else{
                $request['imagename_p1']=$request->image_1;
            }
            
            if(isset($request->image)){
                $request->validate([
                    'image' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:5120',
                ]);
                $imageName = 'p2_'.time().'.'.$request->image->extension();   
                $request->image->move(public_path('images'), $imageName);
                $request['imagename']=$imageName;

            }else{
                $request['imagename']=$request->image_2;
            }


            $this->repository->update_recently_leased($page, $request->except(['_token', '_method']));
        }else if($page['id'] == '32'){

            if(isset($request->image)){
                $request->validate([
                    'image' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:5120',
                ]);
                $imageName = time().'.'.$request->image->extension(); 
                $request->image->move(public_path('images'), $imageName);
                $request['imagename']=$imageName;
                
            }else{
                $request['imagename']=$request->image_1;
            }
            

            $this->repository->update_recently_sold_results($page, $request->except(['_token', '_method']));
        }else if($page['id'] == '33'){

            if(isset($request->image)){
                $request->validate([
                    'image' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:5120',
                ]);
                $imageName = time().'.'.$request->image->extension(); 
                $request->image->move(public_path('images'), $imageName);
                $request['imagename']=$imageName;
                
            }else{
                $request['imagename']=$request->image_1;
            }
            

            $this->repository->update_agent_search_results($page, $request->except(['_token', '_method']));
        }else if($page['id'] == '34'){

            if(isset($request->image)){
                $request->validate([
                    'image' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:5120',
                ]);
                $imageName = time().'.'.$request->image->extension(); 
                $request->image->move(public_path('images'), $imageName);
                $request['imagename']=$imageName;
                
            }else{
                $request['imagename']=$request->image_1;
            }
            

            $this->repository->update_agent_search_results($page, $request->except(['_token', '_method']));
        }else if($page['id'] == '35'){

            if(isset($request->banner_image)){
                $request->validate([
                    'banner_image' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:5120',
                ]);
                $imageName_banner = time().'.'.$request->banner_image->extension(); 
                $request->banner_image->move(public_path('images'), $imageName_banner);
                $request['imagename_banner']=$imageName_banner;
                
            }else{
                $request['imagename_banner']=$request->image_2;
            }
            
            if(isset($request->image)){
                $request->validate([
                    'image' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:5120',
                ]);
                $imageName = "p1_".time().'.'.$request->image->extension(); 
                $request->image->move(public_path('images'), $imageName);
                $request['imagename']=$imageName;
                
            }else{
                $request['imagename']=$request->image_1;
            }

            if(isset($request->image_p2)){
                $request->validate([
                    'image_p2' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:5120',
                ]);
                $imageName_p2 = "p2_".time().'.'.$request->image_p2->extension(); 
                $request->image_p2->move(public_path('images'), $imageName_p2);
                $request['imagename_p2']=$imageName_p2;
                
            }else{
                $request['imagename_p2']=$request->image_3;
            }

            if(isset($request->image_p4)){
                $request->validate([
                    'image_p4' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:5120',
                ]);
                $imageName_p4 = "p4_".time().'.'.$request->image_p4->extension(); 
                $request->image_p4->move(public_path('images'), $imageName_p4);
                $request['imagename_p4']=$imageName_p4;
                
            }else{
                $request['imagename_p4']=$request->image_4;
            }

            if(isset($request->image_p5)){
                $request->validate([
                    'image_p5' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:5120',
                ]);
                $imageName_p5 = "p5_".time().'.'.$request->image_p5->extension(); 
                $request->image_p5->move(public_path('images'), $imageName_p5);
                $request['imagename_p5']=$imageName_p5;
                
            }else{
                $request['imagename_p5']=$request->image_5;
            }
            

            $this->repository->update_about_us($page, $request->except(['_token', '_method']));
        }else if($page['id'] == '36'){

            if(isset($request->image)){
                $request->validate([
                    'image' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:5120',
                ]);
                $imageName = time().'.'.$request->image->extension(); 
                $request->image->move(public_path('images'), $imageName);
                $request['imagename']=$imageName;
                
            }else{
                $request['imagename']=$request->image_1;
            }
            

            $this->repository->update_contact_us($page, $request->except(['_token', '_method']));
        }else if($page['id'] == '37'){

            if(isset($request->image)){
                $request->validate([
                    'image' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:5120',
                ]);
                $imageName = time().'.'.$request->image->extension(); 
                $request->image->move(public_path('images'), $imageName);
                $request['imagename']=$imageName;
                
            }else{
                $request['imagename']=$request->image_1;
            }
            

            $this->repository->update_terms($page, $request->except(['_token', '_method']));
        }else if($page['id'] == '38'){

            if(isset($request->banner_image)){
                $request->validate([
                    'banner_image' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:5120',
                ]);
                $imageName_banner = time().'.'.$request->banner_image->extension(); 
                $request->banner_image->move(public_path('images'), $imageName_banner);
                $request['imagename_banner']=$imageName_banner;
                
            }else{
                $request['imagename_banner']=$request->image_2;
            }
            
            if(isset($request->image)){
                $request->validate([
                    'image' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:5120',
                ]);
                $imageName = 'p1_'.time().'.'.$request->image->extension(); 
                $request->image->move(public_path('images'), $imageName);
                $request['imagename']=$imageName;
                
            }else{
                $request['imagename']=$request->image_1;
            }

            if(isset($request->image_p4)){
                $request->validate([
                    'image_p4' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:5120',
                ]);
                $imageName_p4 = 'p4_'.time().'.'.$request->image_p4->extension(); 
                $request->image_p4->move(public_path('images'), $imageName_p4);
                $request['imagename_p4']=$imageName_p4;
                
            }else{
                $request['imagename_p4']=$request->image_4;
            }
            

            $this->repository->update_open_business($page, $request->except(['_token', '_method']));
        }else if($page['id'] == '39'){
            if(isset($request->image_banner)){
                $request->validate([
                    'image_banner' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:5120',
                ]);
                $imageName_banner = time().'.'.$request->image_banner->extension(); 
                $request->image_banner->move(public_path('images'), $imageName_banner);
                $request['imagename_banner']=$imageName_banner;

                
            }else{
                $request['imagename_banner']=$request->image_3;
            }
            
            
            if(isset($request->image_p1)){
                $request->validate([
                'image_p1' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:5120',
                ]);
                $imageName_p1 = 'p1_'.time().'.'.$request->image_p1->extension(); 
                $request->image_p1->move(public_path('images'), $imageName_p1);
                $request['imagename_p1']=$imageName_p1;
            }else{
                $request['imagename_p1']=$request->image_1;
            }
            
            if(isset($request->image_p2)){
                $request->validate([
                    'image_p2' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:5120',
                ]);
                $imageName_p2 = 'p2_'.time().'.'.$request->image_p2->extension();   
                $request->image_p2->move(public_path('images'), $imageName_p2);
                $request['imagename_p2']=$imageName_p2;

            }else{
                $request['imagename_p2']=$request->image_2;
            }


            $this->repository->update_agent($page, $request->except(['_token', '_method']));
        }else if($page['id'] == '42'){

            if(isset($request->image)){
                $request->validate([
                    'image' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:5120',
                ]);
                $imageName = time().'.'.$request->image->extension(); 
                $request->image->move(public_path('images'), $imageName);
                $request['imagename']=$imageName;
                
            }else{
                $request['imagename']=$request->image_1;
            }
            

            $this->repository->update_agent_search_results($page, $request->except(['_token', '_method']));
        }else if($page['id'] == '43'){

            

            $this->repository->update_buy_ind_list($page, $request->except(['_token', '_method']));
        }else if($page['id'] == '40'){

            

            $this->repository->update_buy_ind_list($page, $request->except(['_token', '_method']));
        }else {
            $this->repository->update_menu($page, $request->except(['_token', '_method'])); 
        }   

        

        return new RedirectResponse(route('admin.pages.index'), ['flash_success' => __('alerts.backend.pages.updated')]);
    }

    /**
     * @param \App\Models\Page $page
     * @param \App\Http\Requests\Backend\Pages\DeletePageRequest $request
     *
     * @return \App\Http\Responses\RedirectResponse
     */
    public function destroy(Page $page, DeletePageRequest $request)
    {
        $this->repository->delete($page);

        return new RedirectResponse(route('admin.pages.index'), ['flash_success' => __('alerts.backend.pages.deleted')]);
    }
}
