<?php

namespace App\Repositories\Backend;

use App\Events\Backend\Pages\PageCreated;
use App\Events\Backend\Pages\PageDeleted;
use App\Events\Backend\Pages\PageUpdated;
use App\Exceptions\GeneralException;
use App\Models\Page;
use App\Repositories\BaseRepository;
use Illuminate\Support\Str;

class PagesRepository extends BaseRepository
{
    /**
     * Associated Repository Model.
     */
    const MODEL = Page::class;

    /**
     * Sortable.
     *
     * @var array
     */
    private $sortable = [
        'id',
        'title',
        'page_slug',
        'description',
        'seo_title',
        'status',
        'created_at',
        'updated_at',
    ];

    /**
     * Retrieve List.
     *
     * @var array
     * @return Collection
     */
    public function retrieveList(array $options = [])
    {
        $perPage = isset($options['per_page']) ? (int) $options['per_page'] : 20;
        $orderBy = isset($options['order_by']) && in_array($options['order_by'], $this->sortable) ? $options['order_by'] : 'created_at';
        $order = isset($options['order']) && in_array($options['order'], ['asc', 'desc']) ? $options['order'] : 'desc';
        $query = $this->query()
            ->with([
                'owner',
                'updater',
            ])
            ->orderBy($orderBy, $order);

        if ($perPage == -1) {
            return $query->get();
        }

        return $query->paginate($perPage);
    }

    /**
     * @return mixed
     */
    public function getForDataTable()
    {
        return $this->query()
            ->leftjoin('users', 'users.id', '=', 'pages.created_by')
            ->select([
                'pages.id',
                'pages.title',
                'pages.status',
                'users.first_name as created_by',
                'pages.created_at',
            ]);
    }

    /**
     * @param array $input
     *
     * @throws \App\Exceptions\GeneralException
     *
     * @return bool
     */
    public function create(array $input)
    {

        // print_r($input);die;
        
        // print_r($imageName);die;
        // if ($this->query()->where('title', $input['title'])->first()) {
        //     throw new GeneralException(__('exceptions.backend.pages.already_exists'));
        // }

        // $input['page_slug'] = Str::slug($input['title']);
        $input['created_by'] = auth()->user()->id;
        $input['status'] = $input['status'] ?? 0;

        $data=array();

        $data['title']=$input['title'];
        $data['why']['desc']=$input['description'];
        $data['why']['image']=$input['imagename'];
        $data['why']['button_link']=$input['button_link'];
        $data['ofc']['desc']=$input['description_ofc'];
        $data['ofc']['image']=$input['imagename_ofc'];
        $data['ofc']['button_link']=$input['button_link_ofc'];

        $j_data=json_encode($data);

        $input['pagedata']=$j_data;

        if ($page = Page::create($input)) {
            event(new PageCreated($page));

            return $page->fresh();
        }

        throw new GeneralException(__('exceptions.backend.pages.create_error'));
    }

    /**
     * Update Page.
     *
     * @param \App\Models\Page $page
     * @param array $input
     */
    public function update(Page $page, array $input)
    {
        
        $input['updated_by'] = auth()->user()->id;
        $input['status'] = $input['status'] ?? 0;

        $data=array();
        // print_r($input['image_fp1']);die;
        $data['title']=$input['title'];
        $data['meta_title']=$input['meta_title'];
        $data['meta_keywords']=$input['meta_keywords'];
        $data['meta_desc']=$input['meta_desc'];
        $data['banner']=$input['imagename_banner'];
        $data['why']['desc']=$input['description'];
        $data['why']['image']=$input['imagename'];
        $data['why']['button_link']=$input['button_link'];
        $data['why']['heading']=$input['heading_1'];
        $data['ofc']['desc']=$input['description_ofc'];
        $data['ofc']['image']=$input['imagename_ofc'];
        $data['ofc']['button_link']=$input['button_link_ofc'];
        $data['ofc']['heading']=$input['heading_2'];
        $data['ofc']['sub_heading']=$input['heading_3'];

        $data['fp']['image_1']=$input['imagename_fp1'];
        $data['fp']['image_2']=$input['imagename_fp2'];
        $data['fp']['image_3']=$input['imagename_fp3'];

        $data['fp']['add_1']=$input['add_1'];
        $data['fp']['add_2']=$input['add_2'];
        $data['fp']['add_3']=$input['add_3'];

        $data['fp']['date_1']=$input['date_1'];
        $data['fp']['date_2']=$input['date_2'];
        $data['fp']['date_3']=$input['date_3'];


        $data['fp']['bed_1']=$input['bed_1'];
        $data['fp']['bed_2']=$input['bed_2'];
        $data['fp']['bed_3']=$input['bed_3'];

        $data['fp']['bath_1']=$input['bath_1'];
        $data['fp']['bath_2']=$input['bath_2'];
        $data['fp']['bath_3']=$input['bath_3'];

        $data['fp']['car_1']=$input['car_1'];
        $data['fp']['car_2']=$input['car_2'];
        $data['fp']['car_3']=$input['car_3'];




        $data['fr']['image_1']=$input['imagename_fr1'];
        $data['fr']['image_2']=$input['imagename_fr2'];
        $data['fr']['image_3']=$input['imagename_fr3'];

        $data['fr']['add_1']=$input['add_r1'];
        $data['fr']['add_2']=$input['add_r2'];
        $data['fr']['add_3']=$input['add_r3'];

        $data['fr']['date_1']=$input['date_r1'];
        $data['fr']['date_2']=$input['date_r2'];
        $data['fr']['date_3']=$input['date_r3'];


        $data['fr']['bed_1']=$input['bed_r1'];
        $data['fr']['bed_2']=$input['bed_r2'];
        $data['fr']['bed_3']=$input['bed_r3'];

        $data['fr']['bath_1']=$input['bath_r1'];
        $data['fr']['bath_2']=$input['bath_r2'];
        $data['fr']['bath_3']=$input['bath_r3'];

        $data['fr']['car_1']=$input['car_r1'];
        $data['fr']['car_2']=$input['car_r2'];
        $data['fr']['car_3']=$input['car_r3'];

        $j_data=json_encode($data);

        $input['pagedata']=$j_data;

        if ($page->update($input)) {
            event(new PageUpdated($page));

            return $page;
        }

        throw new GeneralException(
            __('exceptions.backend.pages.update_error')
        );
    }

    public function update_buy_ind_list(Page $page, array $input)
    {
        
        $input['updated_by'] = auth()->user()->id;
        $input['status'] = $input['status'] ?? 0;

        $data=array();

        $data['title']=$input['title'];
        $data['meta_title']=$input['meta_title'];
        $data['meta_keywords']=$input['meta_keywords'];
        $data['meta_desc']=$input['meta_desc'];

        $j_data=json_encode($data);

        $input['pagedata']=$j_data;

        if ($page->update($input)) {
            event(new PageUpdated($page));

            return $page;
        }

        throw new GeneralException(
            __('exceptions.backend.pages.update_error')
        );
    }
    public function update_finance(Page $page, array $input)
        {
            
            $input['updated_by'] = auth()->user()->id;
            $input['status'] = $input['status'] ?? 0;

            $data=array();

            $data['title']=$input['title'];
            $data['meta_title']=$input['meta_title'];
            $data['meta_keywords']=$input['meta_keywords'];
            $data['meta_desc']=$input['meta_desc'];
            $data['banner']=$input['imagename_banner'];
            $data['p1']['desc']=$input['description_p1'];
            $data['p1']['image']=$input['imagename_p1'];
            $data['p1']['heading']=$input['heading_1'];
            $data['p2']['desc']=$input['description_p2'];
            $data['p2']['image']=$input['imagename_p2'];
            $data['p2']['button_link']=$input['button_link_p2'];
            $data['p2']['heading']=$input['heading_2'];
            $data['p3']['heading']=$input['heading_3'];
            $data['p3']['s1']=$input['s1'];
            $data['p3']['s2']=$input['s2'];
            $data['p3']['s3']=$input['s3'];
            $data['p3']['s4']=$input['s4'];
            $data['p3']['s5']=$input['s5'];
            $data['p3']['s6']=$input['s6'];
            $data['p3']['s7']=$input['s7'];
            $data['p3']['s8']=$input['s8'];

            $j_data=json_encode($data);

            // print_r($j_data);die;

            $input['pagedata']=$j_data;

            if ($page->update($input)) {
                event(new PageUpdated($page));

                return $page;
            }

            throw new GeneralException(
                __('exceptions.backend.pages.update_error')
            );
        }
    public function update_agent(Page $page, array $input)
        {
            
            $input['updated_by'] = auth()->user()->id;
            $input['status'] = $input['status'] ?? 0;

            $data=array();

            $data['title']=$input['title'];
            $data['meta_title']=$input['meta_title'];
            $data['meta_keywords']=$input['meta_keywords'];
            $data['meta_desc']=$input['meta_desc'];
            $data['banner']=$input['imagename_banner'];
            $data['p1']['desc']=$input['description_p1'];
            $data['p1']['image']=$input['imagename_p1'];
            $data['p1']['heading']=$input['heading_1'];
            $data['p1']['button_link']=$input['button_link_p1'];
            $data['p2']['desc']=$input['description_p2'];
            $data['p2']['image']=$input['imagename_p2'];
            $data['p2']['button_link']=$input['button_link_p2'];
            $data['p2']['heading']=$input['heading_2'];

            $j_data=json_encode($data);

            // print_r($j_data);die;

            $input['pagedata']=$j_data;

            if ($page->update($input)) {
                event(new PageUpdated($page));

                return $page;
            }

            throw new GeneralException(
                __('exceptions.backend.pages.update_error')
            );
        }


    public function update_manage_with_us(Page $page, array $input)
        {
            
            $input['updated_by'] = auth()->user()->id;
            $input['status'] = $input['status'] ?? 0;

            $data=array();

            $data['title']=$input['title'];
            $data['meta_title']=$input['meta_title'];
            $data['meta_keywords']=$input['meta_keywords'];
            $data['meta_desc']=$input['meta_desc'];
            $data['banner']=$input['imagename_banner'];
            $data['p1']['desc']=$input['description_p1'];
            $data['p1']['heading']=$input['heading_1'];
            $data['p1']['image']=$input['imagename_p1'];
            $data['p2']['desc']=$input['description_p2'];
            $data['p2']['image']=$input['imagename_p2'];
            $data['p2']['button_link']=$input['button_link_p2'];
            $data['p3']['desc']=$input['description_p3'];
            $data['p3']['desc_t1']=$input['description_t1'];
            $data['p3']['desc_t2']=$input['description_t2'];
            $data['p3']['desc_t3']=$input['description_t3'];
            $data['p3']['desc_t4']=$input['description_t4'];
            $data['p3']['desc_t5']=$input['description_t5'];
            $data['p3']['desc_t6']=$input['description_t6'];
            $data['p3']['desc_t7']=$input['description_t7'];
            $data['p3']['desc_t8']=$input['description_t8'];
            
            $data['p4']['heading']=$input['heading_2'];
            $data['p4']['desc']=$input['description_p4'];
            $data['p4']['image']=$input['imagename_p4'];
            $data['p4']['button_link']=$input['button_link_p4'];

            $j_data=json_encode($data);

            // print_r($j_data);die;

            $input['pagedata']=$j_data;

            if ($page->update($input)) {
                event(new PageUpdated($page));

                return $page;
            }

            throw new GeneralException(
                __('exceptions.backend.pages.update_error')
            );
        }
        
        public function update_rent_applying(Page $page, array $input)
        {
            
            $input['updated_by'] = auth()->user()->id;
            $input['status'] = $input['status'] ?? 0;

            $data=array();

            $data['title']=$input['title'];
            $data['meta_title']=$input['meta_title'];
            $data['meta_keywords']=$input['meta_keywords'];
            $data['meta_desc']=$input['meta_desc'];
            $data['banner']=$input['imagename_banner'];
            $data['p1']['heading']=$input['heading'];
            $data['p1']['desc']=$input['description_p1'];
            $data['p1']['image']=$input['imagename_p1'];
            $data['p1']['button_link']=$input['button_link'];
            
            $data['p2']['s1']=$input['s1'];
            $data['p2']['s2']=$input['s2'];
            $data['p2']['s3']=$input['s3'];
            $data['p2']['s4']=$input['s4'];
            $data['p2']['s5']=$input['s5'];
            $data['p2']['s6']=$input['s6'];

            $j_data=json_encode($data);

            // print_r($j_data);die;

            $input['pagedata']=$j_data;

            if ($page->update($input)) {
                event(new PageUpdated($page));

                return $page;
            }

            throw new GeneralException(
                __('exceptions.backend.pages.update_error')
            );
        }


    public function update_menu(Page $page, array $input)
    {
        
        $input['updated_by'] = auth()->user()->id;
        $input['status'] = $input['status'] ?? 0;

        $data=array();

        $data['title']=$input['title'];
        $data['menus'][0]['title']=$input['a1'];
        $data['menus'][0]['submenu'][0]['title']=$input['1a1'];
        $data['menus'][0]['submenu'][0]['link']=$input['1b1'];
        $data['menus'][0]['submenu'][1]['title']=$input['1a2'];
        $data['menus'][0]['submenu'][1]['link']=$input['1b2'];
        $data['menus'][0]['submenu'][2]['title']=$input['1a3'];
        $data['menus'][0]['submenu'][2]['link']=$input['1b3'];
        $data['menus'][0]['submenu'][3]['title']=$input['1a4'];
        $data['menus'][0]['submenu'][3]['link']=$input['1b4'];
        $data['menus'][0]['submenu'][4]['title']=$input['1a5'];
        $data['menus'][0]['submenu'][4]['link']=$input['1b5'];
        $data['menus'][0]['submenu'][5]['title']=$input['1a6'];
        $data['menus'][0]['submenu'][5]['link']=$input['1b6'];
        $data['menus'][1]['title']=$input['a2'];
        $data['menus'][1]['submenu'][0]['title']=$input['2a1'];
        $data['menus'][1]['submenu'][0]['link']=$input['2b1'];
        $data['menus'][1]['submenu'][1]['title']=$input['2a2'];
        $data['menus'][1]['submenu'][1]['link']=$input['2b2'];
        $data['menus'][1]['submenu'][2]['title']=$input['2a3'];
        $data['menus'][1]['submenu'][2]['link']=$input['2b3'];
        $data['menus'][1]['submenu'][3]['title']=$input['2a4'];
        $data['menus'][1]['submenu'][3]['link']=$input['2b4'];
        $data['menus'][1]['submenu'][4]['title']=$input['2a5'];
        $data['menus'][1]['submenu'][4]['link']=$input['2b5'];
        $data['menus'][1]['submenu'][5]['title']=$input['2a6'];
        $data['menus'][1]['submenu'][5]['link']=$input['2b6'];
        $data['menus'][2]['title']=$input['a3'];
        $data['menus'][2]['submenu'][0]['title']=$input['3a1'];
        $data['menus'][2]['submenu'][0]['link']=$input['3b1'];
        $data['menus'][2]['submenu'][1]['title']=$input['3a2'];
        $data['menus'][2]['submenu'][1]['link']=$input['3b2'];
        $data['menus'][2]['submenu'][2]['title']=$input['3a3'];
        $data['menus'][2]['submenu'][2]['link']=$input['3b3'];
        $data['menus'][3]['title']=$input['a4'];
        $data['menus'][3]['submenu'][0]['title']=$input['4a1'];
        $data['menus'][3]['submenu'][0]['link']=$input['4b1'];
        $data['menus'][3]['submenu'][1]['title']=$input['4a2'];
        $data['menus'][3]['submenu'][1]['link']=$input['4b2'];
        $data['menus'][3]['submenu'][2]['title']=$input['4a3'];
        $data['menus'][3]['submenu'][2]['link']=$input['4b3'];
        $data['menus'][3]['submenu'][3]['title']=$input['4a4'];
        $data['menus'][3]['submenu'][3]['link']=$input['4b4'];
        $data['menus'][4]['title']=$input['a5'];
        $data['menus'][4]['submenu'][0]['title']=$input['5a1'];
        $data['menus'][4]['submenu'][0]['link']=$input['5b1'];
        $data['menus'][4]['submenu'][1]['title']=$input['5a2'];
        $data['menus'][4]['submenu'][1]['link']=$input['5b2'];
        $data['menus'][4]['submenu'][2]['title']=$input['5a3'];
        $data['menus'][4]['submenu'][2]['link']=$input['5b3'];
        $data['menus'][4]['submenu'][3]['title']=$input['5a4'];
        $data['menus'][4]['submenu'][3]['link']=$input['5b4'];
        $data['menus'][4]['submenu'][4]['title']=$input['5a5'];
        $data['menus'][4]['submenu'][4]['link']=$input['5b5'];
        $data['menus'][5]['title']=$input['a6'];
        $data['menus'][5]['submenu'][0]['title']=$input['6a1'];
        $data['menus'][5]['submenu'][0]['link']=$input['6b1'];
        $data['menus'][5]['submenu'][1]['title']=$input['6a2'];
        $data['menus'][5]['submenu'][1]['link']=$input['6b2'];
        $data['menus'][5]['submenu'][2]['title']=$input['6a3'];
        $data['menus'][5]['submenu'][2]['link']=$input['6b3'];

        
        // echo "<pre>";print_r($data);echo "</pre>";die;

        $j_data=json_encode($data);

        $input['pagedata']=$j_data;

        if ($page->update($input)) {
            event(new PageUpdated($page));

            return $page;
        }

        throw new GeneralException(
            __('exceptions.backend.pages.update_error')
        );
    }


    public function update_buy_search(Page $page, array $input)
    {
        
        $input['updated_by'] = auth()->user()->id;
        $input['status'] = $input['status'] ?? 0;

        $data=array();

        $data['title']=$input['title'];
        $data['meta_title']=$input['meta_title'];
        $data['meta_keywords']=$input['meta_keywords'];
        $data['meta_desc']=$input['meta_desc'];
        $data['banner']=$input['imagename_banner'];
        $data['wf']['desc']=$input['description'];
        $data['wf']['image']=$input['imagename'];
        $data['wf']['button_link']=$input['button_link'];
        $data['wmm']['desc']=$input['description_ofc'];
        $data['wmm']['image']=$input['imagename_ofc'];
        $data['wmm']['button_link']=$input['button_link_ofc'];

        $j_data=json_encode($data);

        $input['pagedata']=$j_data;

        if ($page->update($input)) {
            event(new PageUpdated($page));

            return $page;
        }

        throw new GeneralException(
            __('exceptions.backend.pages.update_error')
        );
    }


    public function update_open_homes(Page $page, array $input)
    {
        
        $input['updated_by'] = auth()->user()->id;
        $input['status'] = $input['status'] ?? 0;

        $data=array();

        $data['title']=$input['title'];
        $data['meta_title']=$input['meta_title'];
        $data['meta_keywords']=$input['meta_keywords'];
        $data['meta_desc']=$input['meta_desc'];
        $data['banner']=$input['imagename_banner'];
        $data['p1']['desc']=$input['description'];
        $data['p1']['image']=$input['imagename'];
        $data['p1']['heading']=$input['heading_1'];
        $data['p1']['button_link']=$input['button_link'];
        $data['p2']['desc']=$input['description_ofc'];
        $data['p2']['image']=$input['imagename_ofc'];
        $data['p2']['heading']=$input['heading_2'];

        $j_data=json_encode($data);

        $input['pagedata']=$j_data;

        if ($page->update($input)) {
            event(new PageUpdated($page));

            return $page;
        }

        throw new GeneralException(
            __('exceptions.backend.pages.update_error')
        );
    }
    
    public function update_ind_rental(Page $page, array $input)
    {
        
        $input['updated_by'] = auth()->user()->id;
        $input['status'] = $input['status'] ?? 0;

        $data=array();

        $data['title']=$input['title'];
        $data['meta_title']=$input['meta_title'];
        $data['meta_keywords']=$input['meta_keywords'];
        $data['meta_desc']=$input['meta_desc'];
        $data['banner']=$input['imagename_banner'];
        $data['p1']['desc']=$input['description'];
        $data['p1']['image']=$input['imagename'];
        $data['p1']['heading']=$input['heading'];
        $data['p1']['button_link']=$input['button_link'];

        $j_data=json_encode($data);

        $input['pagedata']=$j_data;

        if ($page->update($input)) {
            event(new PageUpdated($page));

            return $page;
        }

        throw new GeneralException(
            __('exceptions.backend.pages.update_error')
        );
    }
    
    public function update_rent_open_homes(Page $page, array $input)
    {
        
        $input['updated_by'] = auth()->user()->id;
        $input['status'] = $input['status'] ?? 0;

        $data=array();

        $data['title']=$input['title'];
        $data['meta_title']=$input['meta_title'];
        $data['meta_keywords']=$input['meta_keywords'];
        $data['meta_desc']=$input['meta_desc'];
        $data['p2']['desc']=$input['description_ofc'];
        $data['p2']['heading']=$input['heading'];
        $data['p2']['image']=$input['imagename_2'];
        $data['p2']['button_link']=$input['button_link_ofc'];
        $data['banner']=$input['imagename_3'];

        $j_data=json_encode($data);

        $input['pagedata']=$j_data;

        if ($page->update($input)) {
            event(new PageUpdated($page));

            return $page;
        }

        throw new GeneralException(
            __('exceptions.backend.pages.update_error')
        );
    }
    public function update_auctions(Page $page, array $input)
    {
        
        $input['updated_by'] = auth()->user()->id;
        $input['status'] = $input['status'] ?? 0;

        $data=array();

        $data['title']=$input['title'];
        $data['meta_title']=$input['meta_title'];
        $data['meta_keywords']=$input['meta_keywords'];
        $data['meta_desc']=$input['meta_desc'];
        $data['banner']=$input['imagename_banner'];
        $data['p1']['desc']=$input['description'];
        $data['p1']['image']=$input['imagename'];
        $data['p1']['sub_head']=$input['sub_head'];
        $data['p2']['desc']=$input['description_p2'];
        $data['p2']['image']=$input['imagename_p2'];
        $data['p2']['sub_head']=$input['sub_head_p2'];
        $data['p3']['desc']=$input['description_p3'];
        $data['p3']['image']=$input['imagename_p3'];
        $data['p3']['sub_head']=$input['sub_head_p3'];

        $j_data=json_encode($data);

        $input['pagedata']=$j_data;

        if ($page->update($input)) {
            event(new PageUpdated($page));

            return $page;
        }

        throw new GeneralException(
            __('exceptions.backend.pages.update_error')
        );
    }

    public function update_off_market(Page $page, array $input)
    {
        
        $input['updated_by'] = auth()->user()->id;
        $input['status'] = $input['status'] ?? 0;

        $data=array();

        $data['title']=$input['title'];
        $data['meta_title']=$input['meta_title'];
        $data['meta_keywords']=$input['meta_keywords'];
        $data['meta_desc']=$input['meta_desc'];
        $data['banner']=$input['imagename'];
        $data['sub_title']=$input['sub_title'];

        $j_data=json_encode($data);

        $input['pagedata']=$j_data;

        if ($page->update($input)) {
            event(new PageUpdated($page));

            return $page;
        }

        throw new GeneralException(
            __('exceptions.backend.pages.update_error')
        );
    }
    
    public function update_privacy_policy(Page $page, array $input)
    {
        
        $input['updated_by'] = auth()->user()->id;
        $input['status'] = $input['status'] ?? 0;

        $data=array();

        $data['title']=$input['title'];
        $data['meta_title']=$input['meta_title'];
        $data['meta_keywords']=$input['meta_keywords'];
        $data['meta_desc']=$input['meta_desc'];
        $data['banner']=$input['imagename'];
        $data['heading']=$input['heading'];
        $data['desc']=$input['desc'];

        $j_data=json_encode($data);

        $input['pagedata']=$j_data;

        if ($page->update($input)) {
            event(new PageUpdated($page));

            return $page;
        }

        throw new GeneralException(
            __('exceptions.backend.pages.update_error')
        );
    }
    
    public function update_terms(Page $page, array $input)
    {
        
        $input['updated_by'] = auth()->user()->id;
        $input['status'] = $input['status'] ?? 0;

        $data=array();

        $data['title']=$input['title'];
        $data['meta_title']=$input['meta_title'];
        $data['meta_keywords']=$input['meta_keywords'];
        $data['meta_desc']=$input['meta_desc'];
        $data['banner']=$input['imagename'];
        $data['heading']=$input['heading'];
        $data['desc']=$input['desc'];

        $j_data=json_encode($data);

        $input['pagedata']=$j_data;

        if ($page->update($input)) {
            event(new PageUpdated($page));

            return $page;
        }

        throw new GeneralException(
            __('exceptions.backend.pages.update_error')
        );
    }

    public function update_contact_us(Page $page, array $input)
    {
        
        $input['updated_by'] = auth()->user()->id;
        $input['status'] = $input['status'] ?? 0;

        $data=array();

        $data['title']=$input['title'];
        $data['meta_title']=$input['meta_title'];
        $data['meta_keywords']=$input['meta_keywords'];
        $data['meta_desc']=$input['meta_desc'];
        $data['banner']=$input['imagename'];
        $data['heading']=$input['heading'];
        $data['desc']=$input['desc'];
        $data['address']=$input['address'];

        $j_data=json_encode($data);

        $input['pagedata']=$j_data;

        if ($page->update($input)) {
            event(new PageUpdated($page));

            return $page;
        }

        throw new GeneralException(
            __('exceptions.backend.pages.update_error')
        );
    }
    
    public function update_search_result(Page $page, array $input)
    {
        
        $input['updated_by'] = auth()->user()->id;
        $input['status'] = $input['status'] ?? 0;

        $data=array();

        $data['title']=$input['title'];
        $data['meta_title']=$input['meta_title'];
        $data['meta_keywords']=$input['meta_keywords'];
        $data['meta_desc']=$input['meta_desc'];
        $data['banner']=$input['imagename'];
        $data['heading']=$input['heading'];

        $j_data=json_encode($data);

        $input['pagedata']=$j_data;

        if ($page->update($input)) {
            event(new PageUpdated($page));

            return $page;
        }

        throw new GeneralException(
            __('exceptions.backend.pages.update_error')
        );
    }
    
    public function update_open_home_result(Page $page, array $input)
    {
        
        $input['updated_by'] = auth()->user()->id;
        $input['status'] = $input['status'] ?? 0;

        $data=array();

        $data['title']=$input['title'];
        $data['meta_title']=$input['meta_title'];
        $data['meta_keywords']=$input['meta_keywords'];
        $data['meta_desc']=$input['meta_desc'];
        $data['banner']=$input['imagename'];
        $data['heading']=$input['heading'];

        $j_data=json_encode($data);

        $input['pagedata']=$j_data;

        if ($page->update($input)) {
            event(new PageUpdated($page));

            return $page;
        }

        throw new GeneralException(
            __('exceptions.backend.pages.update_error')
        );
    }
    
    public function update_notice_to_repair(Page $page, array $input)
    {
        
        $input['updated_by'] = auth()->user()->id;
        $input['status'] = $input['status'] ?? 0;

        $data=array();

        $data['title']=$input['title'];
        $data['meta_title']=$input['meta_title'];
        $data['meta_keywords']=$input['meta_keywords'];
        $data['meta_desc']=$input['meta_desc'];
        $data['banner']=$input['imagename'];
        $data['heading']=$input['heading'];
        $data['desc']=$input['description'];

        $j_data=json_encode($data);

        $input['pagedata']=$j_data;

        if ($page->update($input)) {
            event(new PageUpdated($page));

            return $page;
        }

        throw new GeneralException(
            __('exceptions.backend.pages.update_error')
        );
    }
    
    public function update_notice_to_vacate(Page $page, array $input)
    {
        
        $input['updated_by'] = auth()->user()->id;
        $input['status'] = $input['status'] ?? 0;

        $data=array();

        $data['title']=$input['title'];
        $data['meta_title']=$input['meta_title'];
        $data['meta_keywords']=$input['meta_keywords'];
        $data['meta_desc']=$input['meta_desc'];
        $data['banner']=$input['imagename'];
        $data['heading']=$input['heading'];
        $data['desc']=$input['description'];

        $j_data=json_encode($data);

        $input['pagedata']=$j_data;

        if ($page->update($input)) {
            event(new PageUpdated($page));

            return $page;
        }

        throw new GeneralException(
            __('exceptions.backend.pages.update_error')
        );
    }
    
    public function update_auction_result(Page $page, array $input)
    {
        
        $input['updated_by'] = auth()->user()->id;
        $input['status'] = $input['status'] ?? 0;

        $data=array();

        $data['title']=$input['title'];
        $data['meta_title']=$input['meta_title'];
        $data['meta_keywords']=$input['meta_keywords'];
        $data['meta_desc']=$input['meta_desc'];
        $data['banner']=$input['imagename'];
        $data['heading']=$input['heading'];

        $j_data=json_encode($data);

        $input['pagedata']=$j_data;

        if ($page->update($input)) {
            event(new PageUpdated($page));

            return $page;
        }

        throw new GeneralException(
            __('exceptions.backend.pages.update_error')
        );
    }
    
    public function update_rent_search_result(Page $page, array $input)
    {
        
        $input['updated_by'] = auth()->user()->id;
        $input['status'] = $input['status'] ?? 0;

        $data=array();

        $data['title']=$input['title'];
        $data['meta_title']=$input['meta_title'];
        $data['meta_keywords']=$input['meta_keywords'];
        $data['meta_desc']=$input['meta_desc'];
        $data['banner']=$input['imagename'];
        $data['heading']=$input['heading'];

        $j_data=json_encode($data);

        $input['pagedata']=$j_data;

        if ($page->update($input)) {
            event(new PageUpdated($page));

            return $page;
        }

        throw new GeneralException(
            __('exceptions.backend.pages.update_error')
        );
    }
    
    public function update_rent_open_home_result(Page $page, array $input)
    {
        
        $input['updated_by'] = auth()->user()->id;
        $input['status'] = $input['status'] ?? 0;

        $data=array();

        $data['title']=$input['title'];
        $data['meta_title']=$input['meta_title'];
        $data['meta_keywords']=$input['meta_keywords'];
        $data['meta_desc']=$input['meta_desc'];
        $data['banner']=$input['imagename'];
        $data['heading']=$input['heading'];

        $j_data=json_encode($data);

        $input['pagedata']=$j_data;

        if ($page->update($input)) {
            event(new PageUpdated($page));

            return $page;
        }

        throw new GeneralException(
            __('exceptions.backend.pages.update_error')
        );
    }

    public function update_recently_sold_results(Page $page, array $input)
    {
        
        $input['updated_by'] = auth()->user()->id;
        $input['status'] = $input['status'] ?? 0;

        $data=array();

        $data['title']=$input['title'];
        $data['meta_title']=$input['meta_title'];
        $data['meta_keywords']=$input['meta_keywords'];
        $data['meta_desc']=$input['meta_desc'];
        $data['banner']=$input['imagename'];
        $data['heading']=$input['heading'];

        $j_data=json_encode($data);

        $input['pagedata']=$j_data;

        if ($page->update($input)) {
            event(new PageUpdated($page));

            return $page;
        }

        throw new GeneralException(
            __('exceptions.backend.pages.update_error')
        );
    }

    public function update_agent_search_results(Page $page, array $input)
    {
        
        $input['updated_by'] = auth()->user()->id;
        $input['status'] = $input['status'] ?? 0;

        $data=array();

        $data['title']=$input['title'];
        $data['meta_title']=$input['meta_title'];
        $data['meta_keywords']=$input['meta_keywords'];
        $data['meta_desc']=$input['meta_desc'];
        $data['banner']=$input['imagename'];

        $j_data=json_encode($data);

        $input['pagedata']=$j_data;

        if ($page->update($input)) {
            event(new PageUpdated($page));

            return $page;
        }

        throw new GeneralException(
            __('exceptions.backend.pages.update_error')
        );
    }

    public function update_rental_appraisal(Page $page, array $input)
    {
        
        $input['updated_by'] = auth()->user()->id;
        $input['status'] = $input['status'] ?? 0;

        $data=array();

        $data['title']=$input['title'];
        $data['meta_title']=$input['meta_title'];
        $data['meta_keywords']=$input['meta_keywords'];
        $data['meta_desc']=$input['meta_desc'];
        $data['banner']=$input['imagename'];
        $data['heading']=$input['heading'];
        $data['desc']=$input['desc'];
        $data['p1']['image']=$input['imagename_p1'];
        $data['p1']['heading']=$input['heading_1'];
        $data['p1']['button_link']=$input['button_link_p1'];
        $data['p1']['desc']=$input['description_p1'];

        $j_data=json_encode($data);

        $input['pagedata']=$j_data;

        if ($page->update($input)) {
            event(new PageUpdated($page));

            return $page;
        }

        throw new GeneralException(
            __('exceptions.backend.pages.update_error')
        );
    }

    public function update_appraisal(Page $page, array $input)
    {
        
        $input['updated_by'] = auth()->user()->id;
        $input['status'] = $input['status'] ?? 0;

        $data=array();

        $data['title']=$input['title'];
        $data['meta_title']=$input['meta_title'];
        $data['meta_keywords']=$input['meta_keywords'];
        $data['meta_desc']=$input['meta_desc'];
        $data['banner']=$input['imagename'];
        $data['heading']=$input['heading'];
        $data['desc']=$input['desc'];

        $j_data=json_encode($data);

        $input['pagedata']=$j_data;

        if ($page->update($input)) {
            event(new PageUpdated($page));

            return $page;
        }

        throw new GeneralException(
            __('exceptions.backend.pages.update_error')
        );
    }

    public function update_wisemove(Page $page, array $input)
    {
        
        $input['updated_by'] = auth()->user()->id;
        $input['status'] = $input['status'] ?? 0;

        $data=array();

        $data['title']=$input['title'];
        $data['meta_title']=$input['meta_title'];
        $data['meta_keywords']=$input['meta_keywords'];
        $data['meta_desc']=$input['meta_desc'];
        $data['banner']=$input['imagename'];
        $data['sub_title']=$input['sub_title'];

        $j_data=json_encode($data);

        $input['pagedata']=$j_data;

        if ($page->update($input)) {
            event(new PageUpdated($page));

            return $page;
        }

        throw new GeneralException(
            __('exceptions.backend.pages.update_error')
        );
    }

    public function update_rent(Page $page, array $input)
    {
        
        $input['updated_by'] = auth()->user()->id;
        $input['status'] = $input['status'] ?? 0;

        $data=array();

        $data['title']=$input['title'];
        $data['meta_title']=$input['meta_title'];
        $data['meta_keywords']=$input['meta_keywords'];
        $data['meta_desc']=$input['meta_desc'];
        $data['head_image']=$input['imagename_p1'];
        $data['desc']=$input['description'];
        $data['image']=$input['imagename'];
        $data['heading']=$input['heading'];
        $data['button_link']=$input['button_link'];

        $j_data=json_encode($data);

        // print_r($j_data);die;

        $input['pagedata']=$j_data;

        if ($page->update($input)) {
            event(new PageUpdated($page));

            return $page;
        }

        throw new GeneralException(
            __('exceptions.backend.pages.update_error')
        );
    }

    public function update_recently_sold(Page $page, array $input)
    {
        
        $input['updated_by'] = auth()->user()->id;
        $input['status'] = $input['status'] ?? 0;

        $data=array();

        $data['title']=$input['title'];
        $data['meta_title']=$input['meta_title'];
        $data['meta_keywords']=$input['meta_keywords'];
        $data['meta_desc']=$input['meta_desc'];
        $data['head_image']=$input['imagename_p1'];
        $data['desc']=$input['description'];
        $data['heading']=$input['heading'];
        $data['image']=$input['imagename'];

        $j_data=json_encode($data);

        // print_r($j_data);die;

        $input['pagedata']=$j_data;

        if ($page->update($input)) {
            event(new PageUpdated($page));

            return $page;
        }

        throw new GeneralException(
            __('exceptions.backend.pages.update_error')
        );
    }

    public function update_recently_leased(Page $page, array $input)
    {
        
        $input['updated_by'] = auth()->user()->id;
        $input['status'] = $input['status'] ?? 0;

        $data=array();

        $data['title']=$input['title'];
        $data['meta_title']=$input['meta_title'];
        $data['meta_keywords']=$input['meta_keywords'];
        $data['meta_desc']=$input['meta_desc'];
        $data['head_image']=$input['imagename_p1'];
        $data['desc']=$input['description'];
        $data['heading']=$input['heading'];
        $data['image']=$input['imagename'];

        $j_data=json_encode($data);

        // print_r($j_data);die;

        $input['pagedata']=$j_data;

        if ($page->update($input)) {
            event(new PageUpdated($page));

            return $page;
        }

        throw new GeneralException(
            __('exceptions.backend.pages.update_error')
        );
    }
    
    
    public function update_why_rent(Page $page, array $input)
    {
        
        $input['updated_by'] = auth()->user()->id;
        $input['status'] = $input['status'] ?? 0;

        $data=array();

        $data['title']=$input['title'];
        $data['meta_title']=$input['meta_title'];
        $data['meta_keywords']=$input['meta_keywords'];
        $data['meta_desc']=$input['meta_desc'];
        $data['banner']=$input['imagename_banner'];
        $data['p1']['desc']=$input['description'];
        $data['p1']['heading']=$input['heading_1'];
        $data['p1']['image']=$input['imagename'];
        $data['p1']['button_link']=$input['button_link'];
        $data['p2']['heading']=$input['heading_2'];
        $data['p2']['desc']=$input['description_p2'];

        $j_data=json_encode($data);

        $input['pagedata']=$j_data;

        if ($page->update($input)) {
            event(new PageUpdated($page));

            return $page;
        }

        throw new GeneralException(
            __('exceptions.backend.pages.update_error')
        );
    }


    public function update_sell_with_us(Page $page, array $input)
    {
        
        $input['updated_by'] = auth()->user()->id;
        $input['status'] = $input['status'] ?? 0;

        $data=array();

        $data['title']=$input['title'];
        $data['meta_title']=$input['meta_title'];
        $data['meta_keywords']=$input['meta_keywords'];
        $data['meta_desc']=$input['meta_desc'];
        $data['banner']=$input['imagename_banner'];
        $data['p1']['desc']=$input['description'];
        $data['p1']['heading']=$input['heading_1'];
        $data['p1']['image']=$input['imagename'];
        $data['p2']['desc']=$input['description_p2'];
        $data['p2']['sub_head1']=$input['sub_head1'];
        $data['p2']['sub_desc1']=$input['sub_desc1'];
        $data['p2']['sub_head2']=$input['sub_head2'];
        $data['p2']['sub_desc2']=$input['sub_desc2'];
        $data['p2']['sub_head3']=$input['sub_head3'];
        $data['p2']['sub_desc3']=$input['sub_desc3'];
        $data['p2']['sub_head4']=$input['sub_head4'];
        $data['p2']['sub_desc4']=$input['sub_desc4'];
        $data['p3']['desc']=$input['description_p3'];
        $data['p3']['heading']=$input['heading_1'];
        $data['p3']['image']=$input['imagename_p3'];
        $data['p3']['button_link']=$input['button_link_p3'];


        $j_data=json_encode($data);

        $input['pagedata']=$j_data;

        if ($page->update($input)) {
            event(new PageUpdated($page));

            return $page;
        }

        throw new GeneralException(
            __('exceptions.backend.pages.update_error')
        );
    }

    public function update_open_business(Page $page, array $input)
    {
        
        $input['updated_by'] = auth()->user()->id;
        $input['status'] = $input['status'] ?? 0;

        $data=array();

        $data['title']=$input['title'];
        $data['meta_title']=$input['meta_title'];
        $data['meta_keywords']=$input['meta_keywords'];
        $data['meta_desc']=$input['meta_desc'];
        $data['banner']=$input['imagename_banner'];
        $data['p1']['desc']=$input['description'];
        $data['p1']['heading']=$input['heading_1'];
        $data['p1']['image']=$input['imagename'];
        $data['p2']['desc']=$input['description_p2'];
        $data['p2']['sub_head1']=$input['sub_head1'];
        $data['p2']['sub_desc1']=$input['sub_desc1'];
        $data['p2']['sub_head2']=$input['sub_head2'];
        $data['p2']['sub_desc2']=$input['sub_desc2'];
        $data['p2']['sub_head3']=$input['sub_head3'];
        $data['p2']['sub_desc3']=$input['sub_desc3'];
        $data['p2']['sub_head4']=$input['sub_head4'];
        $data['p2']['sub_desc4']=$input['sub_desc4'];

        $data['p3']['desc']=$input['description_p3'];
        $data['p3']['sub_head1']=$input['mod_sub_head1'];
        $data['p3']['sub_desc1']=$input['mod_sub_desc1'];
        $data['p3']['sub_head2']=$input['mod_sub_head2'];
        $data['p3']['sub_desc2']=$input['mod_sub_desc2'];
        $data['p3']['sub_head3']=$input['mod_sub_head3'];
        $data['p3']['sub_desc3']=$input['mod_sub_desc3'];
        $data['p3']['sub_head4']=$input['mod_sub_head4'];
        $data['p3']['sub_desc4']=$input['mod_sub_desc4'];

        $data['p4']['heading']=$input['heading_2'];
        $data['p4']['desc']=$input['description_p4'];
        $data['p4']['image']=$input['imagename_p4'];
        $data['p4']['button_link']=$input['button_link_p4'];


        $j_data=json_encode($data);

        $input['pagedata']=$j_data;

        if ($page->update($input)) {
            event(new PageUpdated($page));

            return $page;
        }

        throw new GeneralException(
            __('exceptions.backend.pages.update_error')
        );
    }


    public function update_about_us(Page $page, array $input)
    {
        
        $input['updated_by'] = auth()->user()->id;
        $input['status'] = $input['status'] ?? 0;

        $data=array();

        $data['title']=$input['title'];
        $data['meta_title']=$input['meta_title'];
        $data['meta_keywords']=$input['meta_keywords'];
        $data['meta_desc']=$input['meta_desc'];
        $data['banner']=$input['imagename_banner'];
        $data['p1']['desc']=$input['description'];
        $data['p1']['image']=$input['imagename'];
        $data['p1']['heading']=$input['heading_1'];

        $data['p2']['desc']=$input['description_p2'];
        $data['p2']['image']=$input['imagename_p2'];
        $data['p2']['heading']=$input['heading_2'];

        $data['p3']['desc']=$input['description_p3'];
        $data['p3']['sub_head1']=$input['sub_head1'];
        $data['p3']['sub_desc1']=$input['sub_desc1'];
        $data['p3']['sub_head2']=$input['sub_head2'];
        $data['p3']['sub_desc2']=$input['sub_desc2'];
        $data['p3']['sub_head3']=$input['sub_head3'];
        $data['p3']['sub_desc3']=$input['sub_desc3'];
        $data['p3']['sub_head4']=$input['sub_head4'];
        $data['p3']['sub_desc4']=$input['sub_desc4'];

        $data['p4']['desc']=$input['description_p4'];
        $data['p4']['image']=$input['imagename_p4'];
        $data['p4']['heading']=$input['heading_3'];

        $data['p5']['desc']=$input['description_p5'];
        $data['p5']['image']=$input['imagename_p5'];
        $data['p5']['button_link']=$input['button_link_p5'];
        $data['p5']['heading']=$input['heading_4'];


        $j_data=json_encode($data);

        $input['pagedata']=$j_data;

        if ($page->update($input)) {
            event(new PageUpdated($page));

            return $page;
        }

        throw new GeneralException(
            __('exceptions.backend.pages.update_error')
        );
    }

    /**
     * @param \App\Models\Page $page
     *
     * @throws GeneralException
     *
     * @return bool
     */
    public function delete(Page $page)
    {
        if ($page->delete()) {
            event(new PageDeleted($page));

            return true;
        }

        throw new GeneralException(__('exceptions.backend.pages.delete_error'));
    }
}
