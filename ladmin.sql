-- phpMyAdmin SQL Dump
-- version 5.1.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jun 14, 2021 at 07:42 PM
-- Server version: 10.4.18-MariaDB
-- PHP Version: 7.4.18

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `ladmin`
--

-- --------------------------------------------------------

--
-- Table structure for table `blogs`
--

CREATE TABLE `blogs` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `publish_datetime` datetime NOT NULL,
  `featured_image` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `content` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `meta_title` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `cannonical_link` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `meta_description` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `meta_keywords` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` tinyint(4) NOT NULL DEFAULT 0 COMMENT '0 => InActive, 1 => Published, 2 => Draft, 3 => Scheduled',
  `created_by` int(10) UNSIGNED NOT NULL,
  `updated_by` int(10) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `blogs`
--

INSERT INTO `blogs` (`id`, `name`, `publish_datetime`, `featured_image`, `content`, `meta_title`, `cannonical_link`, `slug`, `meta_description`, `meta_keywords`, `status`, `created_by`, `updated_by`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'laborum nihil nulla', '1986-11-23 00:30:39', NULL, 'Nisi non aperiam beatae est dicta eos qui amet. Quia officiis ipsa facilis voluptates laborum aut minima. Laborum perferendis dolores nesciunt iusto officia aut. Delectus iure quo reprehenderit qui sit odit dolor. Velit sed dolor minima.', 'vel sed qui', 'http://beier.net/error-officiis-et-aut-voluptas-dolores', 'iste-dignissimos-maxime-hic-fugit-ab-sint-repudiandae', 'Rerum placeat cumque laudantium consequuntur. Molestias quasi sit quia mollitia id. Consectetur perspiciatis voluptatum quaerat minus. Aliquam sapiente et sint.', 'voluptate', 3, 5, NULL, '2021-05-28 18:01:26', '2021-05-28 18:01:26', NULL),
(2, 'aut aut vel', '1979-01-27 21:04:31', NULL, 'Minima minus dolores fugit adipisci sed. Repudiandae asperiores optio ipsum inventore. Impedit atque blanditiis id alias omnis ut quo id.', 'esse recusandae autem', 'http://www.hermiston.com/fugiat-sint-quasi-dolorem-rerum-voluptatem-mollitia', 'velit-dolorum-autem-eaque-laborum-impedit-nesciunt-quia', 'Et aliquid consectetur laudantium illo harum magni ea necessitatibus. Tempora suscipit dolorem porro et culpa ea cumque. Impedit quis impedit et expedita blanditiis voluptas molestiae.', 'totam', 2, 5, NULL, '2021-05-28 18:01:26', '2021-05-28 18:01:26', NULL),
(3, 'alias ad consectetur', '2005-01-02 19:26:12', NULL, 'Reiciendis ut quas voluptas neque vel. Suscipit illo qui dolor molestiae id aliquid. Architecto fugit quam qui voluptatem rerum recusandae minima. Est fugit aut maxime id.', 'maiores nesciunt maiores', 'http://bailey.com/qui-delectus-sit-quaerat-ducimus', 'saepe-itaque-sint-sed-porro-et-corporis', 'Perspiciatis saepe ut modi maiores. Ut autem iusto ullam quia amet inventore dolore. Odio ullam id modi ipsam doloribus sint eum.', 'culpa', 0, 5, NULL, '2021-05-28 18:01:26', '2021-05-28 18:01:26', NULL),
(4, 'eos illum eum', '1990-01-21 14:07:02', NULL, 'Id laboriosam eligendi consequatur illo. Totam nostrum autem enim hic corrupti dolores. Sint ea qui quod nulla ut. Sequi delectus magni quia rerum eaque molestiae consectetur.', 'velit eius totam', 'https://stiedemann.biz/et-minus-quae-fugit-cupiditate-eum-quia-sapiente.html', 'et-animi-hic-quo-dolorem-aliquid-laudantium', 'Qui corporis alias saepe et atque. Praesentium quasi praesentium dolorem. Impedit laboriosam voluptatem iusto dolor ratione et voluptas. Omnis odio sapiente id alias molestiae.', 'aliquid', 0, 5, NULL, '2021-05-28 18:01:26', '2021-05-28 18:01:26', NULL),
(5, 'eligendi temporibus nostrum', '1972-04-29 16:58:13', NULL, 'Dolores voluptas aliquam perferendis dolores et. Voluptas aut vitae cupiditate pariatur omnis consectetur consequuntur sapiente. Officia expedita temporibus error delectus atque saepe voluptatibus.', 'quidem et et', 'http://www.stracke.biz/et-eaque-incidunt-quis-excepturi-quisquam-eveniet-necessitatibus-culpa', 'repellat-dolore-aut-unde-aut-optio-sint', 'Sequi eum ea sed repellat. Assumenda animi deleniti incidunt asperiores est.', 'pariatur', 0, 5, NULL, '2021-05-28 18:01:26', '2021-05-28 18:01:26', NULL),
(6, 'perspiciatis tenetur autem', '1992-01-13 02:41:32', NULL, 'Dolor libero eos quam quae rerum vel quis. Magni minus beatae omnis assumenda. Est occaecati delectus optio ex. Alias aut omnis exercitationem earum earum doloribus voluptates sed.', 'nihil voluptatem corporis', 'http://www.schinner.info/unde-non-et-sequi.html', 'tempore-magnam-expedita-porro-ut-atque-itaque-explicabo-porro', 'Culpa inventore possimus quae in sed sunt qui. Voluptatem harum alias nihil cum quia similique similique sapiente. Vitae est minus blanditiis sapiente inventore perspiciatis qui.', 'molestiae', 0, 5, NULL, '2021-05-28 18:01:26', '2021-05-28 18:01:26', NULL),
(7, 'amet repellat sunt', '2011-01-08 15:04:12', NULL, 'Facilis eius aut repudiandae itaque. Cupiditate vel et culpa recusandae aut et. Aliquid quis minus dolores velit aliquid. Rerum nobis ea hic.', 'rerum amet alias', 'https://www.koss.biz/sunt-cumque-et-ipsa-non-quidem-maiores', 'consequatur-dolorum-voluptas-facere-excepturi-labore-amet-distinctio', 'Deserunt harum placeat exercitationem pariatur. Autem voluptatem iusto expedita sapiente odio. Cupiditate excepturi eum id exercitationem et incidunt ut. Ea corrupti architecto inventore neque facere quia et.', 'optio', 1, 5, NULL, '2021-05-28 18:01:26', '2021-05-28 18:01:26', NULL),
(8, 'dolor cumque neque', '1982-07-06 05:01:52', NULL, 'Minima minima odit id quisquam mollitia. Illo reprehenderit ipsam accusantium et.', 'aut architecto fugiat', 'http://dooley.com/itaque-expedita-atque-ut-debitis-sapiente-in-mollitia-voluptatem.html', 'laborum-corporis-qui-quae-omnis-quod-sunt-corporis-laborum', 'Consectetur ea necessitatibus veniam nihil dolorem non. Nostrum magnam voluptas at provident. Quam harum quam qui.', 'aut', 2, 5, NULL, '2021-05-28 18:01:26', '2021-05-28 18:01:26', NULL),
(9, 'occaecati facilis quo', '2009-07-26 17:59:09', NULL, 'Aut et et ut. Laboriosam cumque id inventore aspernatur ut. Dolorum distinctio qui maiores temporibus reiciendis sapiente.', 'inventore aliquid recusandae', 'http://cremin.com/cumque-sed-incidunt-ut-et-sit', 'adipisci-minima-assumenda-accusantium-error-pariatur-doloribus-nihil', 'Aliquid deleniti sit et quo non aut. Dolorem aut nisi qui deleniti et. Delectus voluptas quia voluptas voluptas. Laboriosam suscipit similique deserunt soluta voluptates.', 'at', 2, 5, NULL, '2021-05-28 18:01:26', '2021-05-28 18:01:26', NULL),
(10, 'voluptatem iusto quasi', '1971-01-13 21:57:29', NULL, 'Reiciendis fugiat laudantium provident id saepe. Sed itaque iste consequatur voluptatem tenetur. Soluta iste provident quia.', 'praesentium repellendus debitis', 'http://haley.info/eius-non-esse-maxime-sed-magni-libero-aut.html', 'fugiat-hic-vel-eos-deleniti-nihil-cumque', 'Quas inventore cumque similique repellendus incidunt minus. Quae aut minima nulla quis sint ratione. Nostrum error accusamus quae itaque.', 'sunt', 1, 5, NULL, '2021-05-28 18:01:26', '2021-05-28 18:01:26', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `blog_categories`
--

CREATE TABLE `blog_categories` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT 1,
  `created_by` int(10) UNSIGNED NOT NULL,
  `updated_by` int(10) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `blog_categories`
--

INSERT INTO `blog_categories` (`id`, `name`, `status`, `created_by`, `updated_by`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'dignissimos est tenetur', 0, 5, NULL, '2021-05-28 18:01:26', '2021-05-28 18:01:26', NULL),
(2, 'alias sed inventore', 1, 5, NULL, '2021-05-28 18:01:27', '2021-05-28 18:01:27', NULL),
(3, 'officiis rerum veniam', 1, 5, NULL, '2021-05-28 18:01:27', '2021-05-28 18:01:27', NULL),
(4, 'voluptatum nihil odio', 0, 5, NULL, '2021-05-28 18:01:27', '2021-05-28 18:01:27', NULL),
(5, 'qui id soluta', 0, 5, NULL, '2021-05-28 18:01:27', '2021-05-28 18:01:27', NULL),
(6, 'optio id ut', 1, 5, NULL, '2021-05-28 18:01:27', '2021-05-28 18:01:27', NULL),
(7, 'qui blanditiis quo', 1, 5, NULL, '2021-05-28 18:01:27', '2021-05-28 18:01:27', NULL),
(8, 'quia nulla molestias', 0, 5, NULL, '2021-05-28 18:01:27', '2021-05-28 18:01:27', NULL),
(9, 'explicabo ut iste', 0, 5, NULL, '2021-05-28 18:01:27', '2021-05-28 18:01:27', NULL),
(10, 'delectus aut voluptatem', 1, 5, NULL, '2021-05-28 18:01:27', '2021-05-28 18:01:27', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `blog_map_categories`
--

CREATE TABLE `blog_map_categories` (
  `id` int(10) UNSIGNED NOT NULL,
  `blog_id` int(10) UNSIGNED NOT NULL,
  `category_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `blog_map_categories`
--

INSERT INTO `blog_map_categories` (`id`, `blog_id`, `category_id`) VALUES
(1, 1, 1),
(2, 2, 2),
(3, 3, 3),
(4, 4, 4),
(5, 5, 5),
(6, 6, 6),
(7, 7, 7),
(8, 8, 8),
(9, 9, 9),
(10, 10, 10);

-- --------------------------------------------------------

--
-- Table structure for table `blog_map_tags`
--

CREATE TABLE `blog_map_tags` (
  `id` int(10) UNSIGNED NOT NULL,
  `blog_id` int(10) UNSIGNED NOT NULL,
  `tag_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `blog_map_tags`
--

INSERT INTO `blog_map_tags` (`id`, `blog_id`, `tag_id`) VALUES
(1, 1, 1),
(2, 2, 2),
(3, 3, 3),
(4, 4, 4),
(5, 5, 5),
(6, 6, 6),
(7, 7, 7),
(8, 8, 8),
(9, 9, 9),
(10, 10, 10);

-- --------------------------------------------------------

--
-- Table structure for table `blog_tags`
--

CREATE TABLE `blog_tags` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT 1,
  `created_by` int(10) UNSIGNED NOT NULL,
  `updated_by` int(10) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `blog_tags`
--

INSERT INTO `blog_tags` (`id`, `name`, `status`, `created_by`, `updated_by`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'hic', 0, 5, NULL, '2021-05-28 18:01:27', '2021-05-28 18:01:27', NULL),
(2, 'ad', 1, 5, NULL, '2021-05-28 18:01:27', '2021-05-28 18:01:27', NULL),
(3, 'consequatur', 0, 5, NULL, '2021-05-28 18:01:27', '2021-05-28 18:01:27', NULL),
(4, 'aliquid', 1, 5, NULL, '2021-05-28 18:01:27', '2021-05-28 18:01:27', NULL),
(5, 'cum', 1, 5, NULL, '2021-05-28 18:01:27', '2021-05-28 18:01:27', NULL),
(6, 'sint', 0, 5, NULL, '2021-05-28 18:01:27', '2021-05-28 18:01:27', NULL),
(7, 'nisi', 1, 5, NULL, '2021-05-28 18:01:27', '2021-05-28 18:01:27', NULL),
(8, 'asperiores', 1, 5, NULL, '2021-05-28 18:01:27', '2021-05-28 18:01:27', NULL),
(9, 'ipsam', 0, 5, NULL, '2021-05-28 18:01:27', '2021-05-28 18:01:27', NULL),
(10, 'voluptatem', 1, 5, NULL, '2021-05-28 18:01:27', '2021-05-28 18:01:27', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `cache`
--

CREATE TABLE `cache` (
  `key` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `value` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `expiration` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `email_templates`
--

CREATE TABLE `email_templates` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `content` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT 1,
  `created_by` int(10) UNSIGNED NOT NULL,
  `updated_by` int(10) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `email_templates`
--

INSERT INTO `email_templates` (`id`, `title`, `slug`, `content`, `status`, `created_by`, `updated_by`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'Quam omnis perspiciatis quo.', 'quam-omnis-perspiciatis-quo', 'Occaecati sit et itaque aspernatur. Molestiae voluptatem itaque quod illo. Iure aut eos non non pariatur et hic.', 1, 1, NULL, '2021-05-22 01:34:38', NULL, NULL),
(2, 'Nesciunt deserunt nesciunt.', 'nesciunt-deserunt-nesciunt', 'Molestiae eius velit sed dolorum optio ut. Praesentium impedit sunt amet et numquam. Quibusdam fugiat suscipit sint nemo ipsa et facere.', 1, 1, NULL, '2021-05-27 11:48:13', NULL, NULL),
(3, 'Omnis illum id culpa perferendis.', 'omnis-illum-id-culpa-perferendis', 'Voluptas a odio voluptas minima rerum omnis sed. Eveniet et voluptatum sequi quia a neque. Ipsam minus ut quia est et laudantium culpa.', 1, 2, NULL, '2021-05-21 14:46:49', NULL, NULL),
(4, 'Iste sit et.', 'iste-sit-et', 'Placeat odio tempora repellat atque excepturi magni. Aut officiis omnis id veniam iure ut. Assumenda iure cumque reiciendis atque.', 0, 2, NULL, '2021-05-28 11:57:25', NULL, NULL),
(5, 'Dolor molestiae voluptatibus est dignissimos.', 'dolor-molestiae-voluptatibus-est-dignissimos', 'Quia adipisci est quibusdam. Optio aliquam tempore velit eum velit perspiciatis sint. Iste nihil non magnam suscipit reprehenderit ut ipsa. Quia ut dolorum voluptatem recusandae quae iste ut.', 0, 2, NULL, '2021-05-25 16:32:31', NULL, NULL),
(6, 'Placeat doloribus porro.', 'placeat-doloribus-porro', 'Mollitia quidem qui assumenda optio. Qui dolorum ea doloribus et incidunt sunt. Dignissimos consequatur similique non.', 0, 2, NULL, '2021-05-22 23:04:06', NULL, NULL),
(7, 'Pariatur cupiditate deleniti.', 'pariatur-cupiditate-deleniti', 'Est fuga dignissimos quia minima ut quis quasi. Nulla quas quo accusamus provident iste aut et. Ut et voluptate iusto iure voluptatem. Pariatur est aspernatur ipsum fugit vero.', 0, 2, NULL, '2021-05-24 23:45:57', NULL, NULL),
(8, 'Voluptas itaque odit possimus omnis.', 'voluptas-itaque-odit-possimus-omnis', 'Fugit in vel voluptatem et ut aliquid ea. Nemo expedita ratione vitae ab repellat esse a. Dignissimos voluptas numquam id. Perspiciatis quia eius reprehenderit et impedit ad nihil. Suscipit est rerum assumenda nulla et.', 0, 1, NULL, '2021-05-20 16:44:28', NULL, NULL),
(9, 'Officiis non non quas.', 'officiis-non-non-quas', 'Qui consequuntur porro id enim. Deserunt et perferendis quam est quo. Quia incidunt et perspiciatis autem dolor molestias reiciendis. Quasi non vel natus perspiciatis distinctio.', 0, 1, NULL, '2021-05-25 07:02:24', NULL, NULL),
(10, 'Tenetur dolorem molestiae sit.', 'tenetur-dolorem-molestiae-sit', 'Iusto quo consequatur et fugiat earum perferendis magnam. Ex culpa dolorem neque atque natus dolorem quaerat. Dolorem omnis ullam laboriosam laborum similique. Dolores iusto aliquid praesentium.', 1, 1, NULL, '2021-05-26 00:54:03', NULL, NULL),
(11, 'Rerum error enim ea sunt.', 'rerum-error-enim-ea-sunt', 'Alias facilis quia voluptas minima quaerat cumque nihil dolor. Ea sunt eum deserunt natus. Non voluptatibus eius debitis veniam. Dolor dolor dolor eum totam.', 0, 1, NULL, '2021-05-25 01:02:37', NULL, NULL),
(12, 'Voluptatum inventore ducimus minus ullam quos.', 'voluptatum-inventore-ducimus-minus-ullam-quos', 'Rerum recusandae ut voluptas illo et laboriosam. Commodi est possimus unde quo omnis error reprehenderit illum. Voluptate maxime expedita aut sit exercitationem illo.', 0, 1, NULL, '2021-05-19 00:50:13', NULL, NULL),
(13, 'In et voluptatem omnis blanditiis.', 'in-et-voluptatem-omnis-blanditiis', 'Quaerat illum officiis suscipit in nemo. Consequatur nihil perspiciatis ex voluptate. Quis iure impedit et deleniti sint. Omnis veritatis debitis commodi consequuntur.', 1, 2, NULL, '2021-05-25 08:32:09', NULL, NULL),
(14, 'Molestiae dolor non molestiae.', 'molestiae-dolor-non-molestiae', 'Labore impedit numquam ut praesentium dolorum earum. Natus qui fuga nesciunt quae nemo. Qui incidunt quae harum officia. Suscipit illo iure soluta placeat unde. Voluptate ad dignissimos aut vitae enim.', 1, 1, NULL, '2021-05-20 12:57:27', NULL, NULL),
(15, 'Eos ut voluptatibus.', 'eos-ut-voluptatibus', 'Accusamus et non optio qui voluptas nihil. Ipsum quidem id fuga consectetur corporis exercitationem reprehenderit quia. Quis quasi ipsum numquam dolores quia pariatur nesciunt.', 1, 2, NULL, '2021-05-23 07:52:05', NULL, NULL),
(16, 'Et assumenda voluptates error excepturi.', 'et-assumenda-voluptates-error-excepturi', 'Minus voluptates odio ut nihil laudantium. Provident est eius enim nesciunt porro sunt. Explicabo cumque voluptatem officia facere voluptate dolorum rerum. Ut repellat repudiandae animi facilis.', 1, 1, NULL, '2021-05-27 11:55:26', NULL, NULL),
(17, 'Odio sed iure ut est.', 'odio-sed-iure-ut-est', 'Quia sapiente culpa at voluptatem quia. Aut impedit dolor illo doloremque. Esse iure blanditiis accusantium aut.', 0, 1, NULL, '2021-05-25 12:01:37', NULL, NULL),
(18, 'Aliquid quae mollitia sapiente.', 'aliquid-quae-mollitia-sapiente', 'Quae quo dignissimos voluptate eligendi. Necessitatibus labore dolor iste perspiciatis dolor sapiente. Aut aut quia eos itaque nihil quaerat illo. Ratione tenetur et molestiae. Sunt mollitia quaerat quia nisi.', 1, 2, NULL, '2021-05-21 18:18:22', NULL, NULL),
(19, 'Rerum rerum accusantium qui.', 'rerum-rerum-accusantium-qui', 'Et est eligendi velit illum. Eaque distinctio quo rerum ut consequuntur. Accusantium veniam necessitatibus deleniti officiis.', 1, 2, NULL, '2021-05-25 02:09:53', NULL, NULL),
(20, 'Id dolores voluptates.', 'id-dolores-voluptates', 'Earum repellendus enim maxime sed qui qui voluptate. Id debitis asperiores aliquam iusto. Eum ut sint natus voluptates quibusdam aliquam.', 1, 1, NULL, '2021-05-28 16:20:43', NULL, NULL),
(21, 'Veniam accusamus facere voluptatem perferendis.', 'veniam-accusamus-facere-voluptatem-perferendis', 'Ut soluta inventore accusantium exercitationem perferendis repudiandae aut voluptatum. Quas asperiores pariatur non voluptas. Et adipisci aliquam sequi dolore magnam fugit possimus. Quos velit eveniet consequatur.', 1, 1, NULL, '2021-05-22 14:46:26', NULL, NULL),
(22, 'Nulla et labore reiciendis ex.', 'nulla-et-labore-reiciendis-ex', 'Quis sapiente omnis et voluptatem. Rerum corrupti fuga laboriosam tempore cumque sit tempora. Ut suscipit autem quos molestiae placeat.', 1, 2, NULL, '2021-05-26 12:14:02', NULL, NULL),
(23, 'Et aut accusantium.', 'et-aut-accusantium', 'Aut quibusdam nobis qui id voluptas voluptate. Ut mollitia cum aut minus. Laboriosam optio eaque iusto.', 0, 1, NULL, '2021-05-20 02:15:30', NULL, NULL),
(24, 'Eaque in assumenda.', 'eaque-in-assumenda', 'Nemo dolore occaecati libero rem non et quam. Sed odit laboriosam facere accusantium dolorum id. Repellendus vitae nihil ab voluptates reprehenderit.', 1, 1, NULL, '2021-05-22 07:41:18', NULL, NULL),
(25, 'Nihil natus ducimus sunt ratione rerum.', 'nihil-natus-ducimus-sunt-ratione-rerum', 'Et voluptatem sit beatae qui sed omnis culpa et. Fuga necessitatibus corrupti qui tempora. Quia non et corporis qui repellendus sapiente.', 0, 1, NULL, '2021-05-24 20:05:05', NULL, NULL),
(26, 'Sequi distinctio corrupti dolores.', 'sequi-distinctio-corrupti-dolores', 'Aut sed deserunt quo enim consectetur consequatur. Nemo voluptatem nesciunt sint qui sint consectetur omnis. Ipsum saepe deserunt dolores necessitatibus. Debitis illo ipsa in sint eveniet.', 0, 2, NULL, '2021-05-20 21:34:08', NULL, NULL),
(27, 'Est et impedit eos aut doloremque.', 'est-et-impedit-eos-aut-doloremque', 'Ad aspernatur dolorum delectus quod voluptatibus odit odio maiores. Repudiandae repellendus voluptates quas. Nobis pariatur inventore quis odit minus.', 1, 2, NULL, '2021-05-20 15:33:40', NULL, NULL),
(28, 'Voluptate earum est non expedita.', 'voluptate-earum-est-non-expedita', 'Nemo quibusdam id ut sed debitis architecto. Ut ea aut et facere nostrum amet. Ipsa aperiam tenetur voluptatibus dolorum. Magni excepturi totam iure.', 0, 2, NULL, '2021-05-24 00:08:21', NULL, NULL),
(29, 'Id laboriosam culpa enim ea a.', 'id-laboriosam-culpa-enim-ea-a', 'Ut ullam rerum eius at quam velit ad. Voluptatem veniam ad aperiam aspernatur. Id sit sunt quod odit deserunt magnam et. Et sit rerum quod sequi.', 1, 2, NULL, '2021-05-19 21:17:59', NULL, NULL),
(30, 'Reprehenderit amet voluptatem sint.', 'reprehenderit-amet-voluptatem-sint', 'Qui nam cum sit dolores. Et adipisci explicabo magnam nemo delectus velit maiores eos. Provident dolores at optio non.', 0, 2, NULL, '2021-05-20 05:58:14', NULL, NULL),
(31, 'Id soluta maiores.', 'id-soluta-maiores', 'Vel nisi omnis sunt ratione asperiores accusamus consequuntur repudiandae. Enim quam quas facilis doloremque asperiores.', 0, 1, NULL, '2021-05-27 07:06:01', NULL, NULL),
(32, 'Dolorem aut porro dolorem.', 'dolorem-aut-porro-dolorem', 'Quia veniam aut rerum ut. Ratione est velit quia aliquid. Consectetur omnis accusamus temporibus praesentium pariatur. Eveniet ex commodi vel quia numquam voluptas.', 0, 1, NULL, '2021-05-25 09:50:02', NULL, NULL),
(33, 'Quo aperiam dolores esse dolores.', 'quo-aperiam-dolores-esse-dolores', 'Voluptatem molestiae quis accusantium eius vitae. Fugiat nam placeat magnam et voluptatem. Sed est voluptas impedit et. Consequatur esse impedit occaecati. Cum fugit est laborum in aut.', 1, 2, NULL, '2021-05-19 23:52:44', NULL, NULL),
(34, 'Distinctio quia rerum ut repudiandae distinctio.', 'distinctio-quia-rerum-ut-repudiandae-distinctio', 'Eum in rerum est iste optio. Sed esse voluptates quasi quidem voluptatem consequatur sunt. Ullam assumenda est at ut omnis.', 0, 2, NULL, '2021-05-22 14:15:14', NULL, NULL),
(35, 'Aut velit qui adipisci.', 'aut-velit-qui-adipisci', 'Et reiciendis delectus maiores magni. Quidem qui sint eos magni. Et sunt exercitationem explicabo ut nihil.', 0, 1, NULL, '2021-05-22 17:34:30', NULL, NULL),
(36, 'Voluptatem provident est omnis.', 'voluptatem-provident-est-omnis', 'Cupiditate est consequatur quam voluptates doloribus sit hic. Molestiae laudantium officia assumenda facere fugiat ab tempora. Et ut enim doloremque. Modi facere modi architecto.', 0, 1, NULL, '2021-05-23 07:54:50', NULL, NULL),
(37, 'Aperiam omnis ex fugiat exercitationem illum.', 'aperiam-omnis-ex-fugiat-exercitationem-illum', 'Dolorem a quis nulla necessitatibus consequatur eveniet porro cumque. Eos animi et quo libero quidem doloribus qui beatae. Minima voluptatem molestiae pariatur tenetur recusandae placeat. Autem aut ducimus nemo et.', 0, 2, NULL, '2021-05-21 02:53:17', NULL, NULL),
(38, 'Est neque recusandae ad.', 'est-neque-recusandae-ad', 'Deleniti ullam autem fuga explicabo iure aut. Exercitationem magni quod dolorem et. Labore iste sunt et ea. Aut voluptatem quia cum temporibus neque voluptates distinctio. Excepturi sed atque ea est similique et dolor fuga.', 1, 2, NULL, '2021-05-19 07:44:32', NULL, NULL),
(39, 'Accusamus rerum quisquam qui.', 'accusamus-rerum-quisquam-qui', 'Quas odio omnis ut odio quisquam in hic. Omnis quo architecto accusantium quia. Et accusamus praesentium dolorum qui velit quia.', 1, 2, NULL, '2021-05-20 10:37:03', NULL, NULL),
(40, 'Eos autem in qui nam rerum.', 'eos-autem-in-qui-nam-rerum', 'Consequuntur sed voluptate non eum corporis qui voluptatem facere. Alias ipsum nobis quaerat deserunt ipsam corporis. Et iste voluptas et.', 0, 1, NULL, '2021-05-21 19:33:21', NULL, NULL),
(41, 'Modi eum nihil quos ut.', 'modi-eum-nihil-quos-ut', 'Qui quas veritatis ea et. Dignissimos et qui aperiam cum magni ea voluptate. Non provident voluptas iusto et in perferendis.', 1, 2, NULL, '2021-05-19 13:19:03', NULL, NULL),
(42, 'Ullam omnis tenetur veritatis beatae.', 'ullam-omnis-tenetur-veritatis-beatae', 'Quasi odio est nesciunt voluptas dolorem. Rerum fuga ut quidem vitae cumque. Et qui id laboriosam et alias.', 0, 1, NULL, '2021-05-28 05:31:49', NULL, NULL),
(43, 'Pariatur sed quam sunt.', 'pariatur-sed-quam-sunt', 'Et ab nihil sunt est. Corporis voluptatum dolorem vitae et quam quis. Quidem qui est velit. Et provident praesentium est tempore.', 0, 1, NULL, '2021-05-24 20:44:15', NULL, NULL),
(44, 'Officia voluptatibus aliquam animi et.', 'officia-voluptatibus-aliquam-animi-et', 'Eum ratione fugiat odit. Quae sed non ipsum dolores. Quaerat omnis voluptas quia ut ipsam aut voluptatem.', 0, 2, NULL, '2021-05-26 07:12:19', NULL, NULL),
(45, 'Vel optio ducimus error.', 'vel-optio-ducimus-error', 'Voluptatum recusandae cum aut et vel. Blanditiis veniam sit unde tenetur sequi nostrum sapiente. Nihil tempora explicabo quis aut delectus beatae voluptatibus eum. Cupiditate magnam velit non inventore exercitationem eaque voluptatibus.', 1, 1, NULL, '2021-05-21 19:29:02', NULL, NULL),
(46, 'Dignissimos itaque id enim ut explicabo.', 'dignissimos-itaque-id-enim-ut-explicabo', 'Ducimus est qui eius vero. Perferendis fugit consequatur assumenda autem asperiores ut. Quam libero fuga architecto et asperiores ea. Mollitia adipisci reiciendis atque enim fuga autem minus.', 1, 2, NULL, '2021-05-19 08:55:17', NULL, NULL),
(47, 'Cupiditate quia maiores natus.', 'cupiditate-quia-maiores-natus', 'Accusamus cumque ducimus totam praesentium fugit adipisci. Eum dolorum veniam veniam animi. Repellendus nesciunt hic saepe perferendis iure dolorem. Alias porro dignissimos et quasi molestiae officiis alias.', 0, 1, NULL, '2021-05-20 14:22:46', NULL, NULL),
(48, 'Est corrupti sint.', 'est-corrupti-sint', 'Id id beatae voluptatem sint quod dignissimos fuga. Veritatis ea voluptate molestiae reiciendis quod consequuntur fugiat. In porro dolor rerum iusto voluptatem.', 0, 2, NULL, '2021-05-26 17:40:03', NULL, NULL),
(49, 'Eveniet voluptatibus consequatur recusandae.', 'eveniet-voluptatibus-consequatur-recusandae', 'Ut fugit cumque aut consequatur a. Eum nihil rem assumenda illum illum est. Voluptatem et modi consequatur. Sit est tenetur est perferendis est.', 1, 2, NULL, '2021-05-27 21:34:36', NULL, NULL),
(50, 'Delectus repudiandae cupiditate.', 'delectus-repudiandae-cupiditate', 'Sed error sit reiciendis et voluptas vel et. Eius magnam dignissimos dignissimos qui vitae. Ea quia rerum deserunt est.', 0, 1, NULL, '2021-05-22 21:50:55', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `failed_jobs`
--

CREATE TABLE `failed_jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `faqs`
--

CREATE TABLE `faqs` (
  `id` int(10) UNSIGNED NOT NULL,
  `question` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `answer` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT 0,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `faqs`
--

INSERT INTO `faqs` (`id`, `question`, `answer`, `status`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'Porro deleniti exercitationem odio veritatis libero.', 'Nihil non officiis sit sunt blanditiis neque et laborum. Voluptatem veniam et aut aut fugit perspiciatis. Doloribus quas ea nihil inventore at nobis. Et aperiam repellat esse rem quia id.', 1, '2021-05-28 18:01:26', '2021-05-28 18:01:26', NULL),
(2, 'Eum a sed nulla quasi ratione dolores excepturi.', 'Dolor sint nisi qui est. Fugiat ut culpa ut.', 1, '2021-05-28 18:01:26', '2021-05-28 18:01:26', NULL),
(3, 'Sunt nobis ipsa autem dicta.', 'Dolor cupiditate eligendi sapiente repellat est asperiores quibusdam eligendi. Et aut vitae quod saepe nisi ratione consectetur. Esse et optio quis repellendus suscipit facere aut.', 1, '2021-05-28 18:01:26', '2021-05-28 18:01:26', NULL),
(4, 'Sequi laborum a temporibus dolore accusantium ducimus quia.', 'Qui quas cumque non dolorum. Repellat aut voluptate facilis placeat consequatur quam nihil. Delectus occaecati dolores dolorem error vel magni deserunt. Cumque et adipisci quia et sint libero ipsa sint.', 0, '2021-05-28 18:01:26', '2021-05-28 18:01:26', NULL),
(5, 'Suscipit dignissimos ut odio recusandae.', 'Dolor quam quod aspernatur delectus molestiae. Necessitatibus nemo et quae et. Voluptatem adipisci a rerum similique qui cum.', 0, '2021-05-28 18:01:26', '2021-05-28 18:01:26', NULL),
(6, 'Occaecati adipisci esse ut debitis id culpa perferendis.', 'Et modi ratione quam voluptas. Aut esse voluptatum aperiam aut voluptatem dicta. Quasi et sed quibusdam ut autem aut. In delectus libero sequi perferendis dolore aut vel. Quae quia ut suscipit voluptatibus.', 0, '2021-05-28 18:01:26', '2021-05-28 18:01:26', NULL),
(7, 'Blanditiis necessitatibus maxime nobis et consequatur exercitationem.', 'Quo dolorem sit aut id asperiores eius. Dolorem veritatis excepturi accusamus dolorum praesentium at. Nisi illum cum commodi eos.', 0, '2021-05-28 18:01:26', '2021-05-28 18:01:26', NULL),
(8, 'Sunt consectetur consequuntur earum officia sequi esse.', 'Dolor cum hic et ea amet voluptatem sit saepe. Incidunt nihil ut maxime accusantium. Recusandae eaque consequuntur soluta aliquam est totam.', 0, '2021-05-28 18:01:26', '2021-05-28 18:01:26', NULL),
(9, 'Sunt itaque iste aliquid.', 'Recusandae dolorum explicabo et sit. Sit tempora quo corporis officia voluptates eos. Iusto at quo non.', 0, '2021-05-28 18:01:26', '2021-05-28 18:01:26', NULL),
(10, 'Consectetur maxime vitae aut iure tempora et architecto.', 'Sed nemo id nihil dolore non minus. Omnis quaerat assumenda soluta consequatur in eos accusantium quia. Nam iure et distinctio illo fuga rerum. Recusandae rem id molestiae eum ratione tempore quisquam quibusdam. Et quis in alias deleniti qui fugit ipsum.', 1, '2021-05-28 18:01:26', '2021-05-28 18:01:26', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `jobs`
--

CREATE TABLE `jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `queue` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `attempts` tinyint(3) UNSIGNED NOT NULL,
  `reserved_at` int(10) UNSIGNED DEFAULT NULL,
  `available_at` int(10) UNSIGNED NOT NULL,
  `created_at` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `ledgers`
--

CREATE TABLE `ledgers` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_type` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `user_id` bigint(20) UNSIGNED DEFAULT NULL,
  `recordable_type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `recordable_id` bigint(20) UNSIGNED NOT NULL,
  `context` tinyint(3) UNSIGNED NOT NULL,
  `event` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `properties` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `modified` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `pivot` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `extra` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `url` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ip_address` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `user_agent` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `signature` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `ledgers`
--

INSERT INTO `ledgers` (`id`, `user_type`, `user_id`, `recordable_type`, `recordable_id`, `context`, `event`, `properties`, `modified`, `pivot`, `extra`, `url`, `ip_address`, `user_agent`, `signature`, `created_at`, `updated_at`) VALUES
(1, 'App\\Models\\Auth\\User', 1, 'App\\Models\\Auth\\User', 1, 4, 'updated', '{\"id\":1,\"uuid\":\"5b4daf56-8c54-4925-8b61-373b44fea9d9\",\"first_name\":\"Alan\",\"last_name\":\"Whitmore\",\"email\":\"admin@admin.com\",\"avatar_type\":\"gravatar\",\"avatar_location\":null,\"password\":\"$2y$10$q\\/gvy0lznSFuYbl69WKCFewNyG\\/cSzaOPlRu5kxOYyLkzaWwDMk8O\",\"password_changed_at\":null,\"active\":1,\"confirmation_code\":\"520c5c05d62d62fffecd28fb3b7bed6b\",\"confirmed\":1,\"timezone\":null,\"last_login_at\":null,\"last_login_ip\":null,\"to_be_logged_out\":0,\"status\":1,\"created_by\":1,\"updated_by\":null,\"is_term_accept\":0,\"remember_token\":\"R2uhEQz4ydDhwRGUdDgvLHIpkk85iCOymYH83SUu6i6cuFL4GrfezFDXvNoQ\",\"created_at\":\"2021-05-28 15:39:26\",\"updated_at\":\"2021-05-28 15:39:26\",\"deleted_at\":null}', '[\"remember_token\"]', '[]', '[]', 'http://127.0.0.1:8000/login', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/90.0.4430.212 Safari/537.36', '2164e2fe58a18b8cd164db13ca1ed45dfd8ab2a0a22530b04b2c0007acdbe09e3fae2fd1cae6b6c5bd996bf8113532a96e0b833b2c37b2a7b0c388ba85f3a812', '2021-05-28 19:40:04', '2021-05-28 19:40:04'),
(2, 'App\\Models\\Auth\\User', 1, 'App\\Models\\Auth\\User', 1, 4, 'updated', '{\"id\":1,\"uuid\":\"5b4daf56-8c54-4925-8b61-373b44fea9d9\",\"first_name\":\"Alan\",\"last_name\":\"Whitmore\",\"email\":\"admin@admin.com\",\"avatar_type\":\"gravatar\",\"avatar_location\":null,\"password\":\"$2y$10$q\\/gvy0lznSFuYbl69WKCFewNyG\\/cSzaOPlRu5kxOYyLkzaWwDMk8O\",\"password_changed_at\":null,\"active\":1,\"confirmation_code\":\"520c5c05d62d62fffecd28fb3b7bed6b\",\"confirmed\":1,\"timezone\":\"America\\/New_York\",\"last_login_at\":\"2021-05-28 15:40:04\",\"last_login_ip\":\"127.0.0.1\",\"to_be_logged_out\":0,\"status\":1,\"created_by\":1,\"updated_by\":null,\"is_term_accept\":0,\"remember_token\":\"R2uhEQz4ydDhwRGUdDgvLHIpkk85iCOymYH83SUu6i6cuFL4GrfezFDXvNoQ\",\"created_at\":\"2021-05-28 15:39:26\",\"updated_at\":\"2021-05-28 15:40:05\",\"deleted_at\":null}', '[\"timezone\",\"last_login_at\",\"last_login_ip\",\"updated_at\"]', '[]', '[]', 'http://127.0.0.1:8000/login', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/90.0.4430.212 Safari/537.36', '892144033600e1c63fa1952ff20e5f8bf8eeab80227ac5cf0b5b3959c0b30162888e6b05cb6908f80139f5123c219c66be52f24bc3bedebf335a85d294c9f086', '2021-05-28 19:40:05', '2021-05-28 19:40:05'),
(3, 'App\\Models\\Auth\\User', 1, 'App\\Models\\Auth\\User', 1, 4, 'updated', '{\"id\":1,\"uuid\":\"5b4daf56-8c54-4925-8b61-373b44fea9d9\",\"first_name\":\"Alan\",\"last_name\":\"Whitmore\",\"email\":\"admin@admin.com\",\"avatar_type\":\"gravatar\",\"avatar_location\":null,\"password\":\"$2y$10$q\\/gvy0lznSFuYbl69WKCFewNyG\\/cSzaOPlRu5kxOYyLkzaWwDMk8O\",\"password_changed_at\":null,\"active\":1,\"confirmation_code\":\"520c5c05d62d62fffecd28fb3b7bed6b\",\"confirmed\":1,\"timezone\":\"America\\/New_York\",\"last_login_at\":\"2021-05-28 15:40:04\",\"last_login_ip\":\"127.0.0.1\",\"to_be_logged_out\":0,\"status\":1,\"created_by\":1,\"updated_by\":null,\"is_term_accept\":0,\"remember_token\":\"sMcA75efQtfxGB1XUgwuNuOddBJZEsz5XRZDktVGqf6ondGsl3EZPweAUT4I\",\"created_at\":\"2021-05-28 15:39:26\",\"updated_at\":\"2021-05-28 15:40:05\",\"deleted_at\":null}', '[\"remember_token\"]', '[]', '[]', 'http://127.0.0.1:8000/logout', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/90.0.4430.212 Safari/537.36', '1ef11c4ca6395a5b8197a241c4c4e9feb2cc0489b9fd2ee79c1d76bc911530dde660f55b5a5ad8141ec324d777c718ec7ad720cce132aad9b642eb0c655af27f', '2021-05-31 13:49:16', '2021-05-31 13:49:16'),
(4, 'App\\Models\\Auth\\User', 1, 'App\\Models\\Auth\\User', 1, 4, 'updated', '{\"id\":1,\"uuid\":\"5b4daf56-8c54-4925-8b61-373b44fea9d9\",\"first_name\":\"Alan\",\"last_name\":\"Whitmore\",\"email\":\"admin@admin.com\",\"avatar_type\":\"gravatar\",\"avatar_location\":null,\"password\":\"$2y$10$q\\/gvy0lznSFuYbl69WKCFewNyG\\/cSzaOPlRu5kxOYyLkzaWwDMk8O\",\"password_changed_at\":null,\"active\":1,\"confirmation_code\":\"520c5c05d62d62fffecd28fb3b7bed6b\",\"confirmed\":1,\"timezone\":\"America\\/New_York\",\"last_login_at\":\"2021-05-31 09:49:32\",\"last_login_ip\":\"127.0.0.1\",\"to_be_logged_out\":0,\"status\":1,\"created_by\":1,\"updated_by\":null,\"is_term_accept\":0,\"remember_token\":\"sMcA75efQtfxGB1XUgwuNuOddBJZEsz5XRZDktVGqf6ondGsl3EZPweAUT4I\",\"created_at\":\"2021-05-28 15:39:26\",\"updated_at\":\"2021-05-31 09:49:32\",\"deleted_at\":null}', '[\"last_login_at\",\"updated_at\"]', '[]', '[]', 'http://127.0.0.1:8000/login', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/90.0.4430.212 Safari/537.36', '3e78b37bad59c35193184f7b6917c73b82789a12e284a8d653af3c1ea0272ced2cde1e0eab1b40a0d0f48569962e6a5a49397449c84a00ec906b6224f5a749c8', '2021-05-31 13:49:32', '2021-05-31 13:49:32'),
(5, 'App\\Models\\Auth\\User', 1, 'App\\Models\\Auth\\User', 1, 4, 'updated', '{\"id\":1,\"uuid\":\"5b4daf56-8c54-4925-8b61-373b44fea9d9\",\"first_name\":\"Alan\",\"last_name\":\"Whitmore\",\"email\":\"admin@admin.com\",\"avatar_type\":\"gravatar\",\"avatar_location\":null,\"password\":\"$2y$10$q\\/gvy0lznSFuYbl69WKCFewNyG\\/cSzaOPlRu5kxOYyLkzaWwDMk8O\",\"password_changed_at\":null,\"active\":1,\"confirmation_code\":\"520c5c05d62d62fffecd28fb3b7bed6b\",\"confirmed\":1,\"timezone\":\"America\\/New_York\",\"last_login_at\":\"2021-05-31 09:49:32\",\"last_login_ip\":\"127.0.0.1\",\"to_be_logged_out\":0,\"status\":1,\"created_by\":1,\"updated_by\":null,\"is_term_accept\":0,\"remember_token\":\"Ui5UTCuLEmUHSLoortZNGkov7EF1a83uhhDjb3LJBhEzokRJlC2AJSohn74Y\",\"created_at\":\"2021-05-28 15:39:26\",\"updated_at\":\"2021-05-31 09:49:32\",\"deleted_at\":null}', '[\"remember_token\"]', '[]', '[]', 'http://127.0.0.1:8000/logout', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/90.0.4430.212 Safari/537.36', '7f900a0ec44e43ad84ed5163935e29d0cd239a79ef173b172e9ac38ff60d1e62e278e138ca504b365f0472119f94068b1eb4e28619b2d3f90314536ad8b6038a', '2021-05-31 13:51:09', '2021-05-31 13:51:09'),
(6, 'App\\Models\\Auth\\User', 1, 'App\\Models\\Auth\\User', 1, 4, 'updated', '{\"id\":1,\"uuid\":\"5b4daf56-8c54-4925-8b61-373b44fea9d9\",\"first_name\":\"Alan\",\"last_name\":\"Whitmore\",\"email\":\"admin@admin.com\",\"avatar_type\":\"gravatar\",\"avatar_location\":null,\"password\":\"$2y$10$q\\/gvy0lznSFuYbl69WKCFewNyG\\/cSzaOPlRu5kxOYyLkzaWwDMk8O\",\"password_changed_at\":null,\"active\":1,\"confirmation_code\":\"520c5c05d62d62fffecd28fb3b7bed6b\",\"confirmed\":1,\"timezone\":\"America\\/New_York\",\"last_login_at\":\"2021-05-31 09:51:21\",\"last_login_ip\":\"127.0.0.1\",\"to_be_logged_out\":0,\"status\":1,\"created_by\":1,\"updated_by\":null,\"is_term_accept\":0,\"remember_token\":\"Ui5UTCuLEmUHSLoortZNGkov7EF1a83uhhDjb3LJBhEzokRJlC2AJSohn74Y\",\"created_at\":\"2021-05-28 15:39:26\",\"updated_at\":\"2021-05-31 09:51:21\",\"deleted_at\":null}', '[\"last_login_at\",\"updated_at\"]', '[]', '[]', 'http://127.0.0.1:8000/login', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/90.0.4430.212 Safari/537.36', '8a6f94f32851b62e1c561060a93422ef1c91b23b944ec882ef473765da4fb99a4c72797b73f2578b3a79c027461854ecacab26a8acff3f7defffc74b821914e2', '2021-05-31 13:51:21', '2021-05-31 13:51:21'),
(7, 'App\\Models\\Auth\\User', 1, 'App\\Models\\Auth\\User', 1, 4, 'updated', '{\"id\":1,\"uuid\":\"5b4daf56-8c54-4925-8b61-373b44fea9d9\",\"first_name\":\"Alan\",\"last_name\":\"Whitmore\",\"email\":\"admin@admin.com\",\"avatar_type\":\"gravatar\",\"avatar_location\":null,\"password\":\"$2y$10$q\\/gvy0lznSFuYbl69WKCFewNyG\\/cSzaOPlRu5kxOYyLkzaWwDMk8O\",\"password_changed_at\":null,\"active\":1,\"confirmation_code\":\"520c5c05d62d62fffecd28fb3b7bed6b\",\"confirmed\":1,\"timezone\":\"America\\/New_York\",\"last_login_at\":\"2021-05-31 09:51:21\",\"last_login_ip\":\"127.0.0.1\",\"to_be_logged_out\":0,\"status\":1,\"created_by\":1,\"updated_by\":null,\"is_term_accept\":0,\"remember_token\":\"ZEADbc1CzYpP6ZRp08f1Z9zjgse4Jtg3F1IaavEYzMK0144XVKmM2mZv9MFD\",\"created_at\":\"2021-05-28 15:39:26\",\"updated_at\":\"2021-05-31 09:51:21\",\"deleted_at\":null}', '[\"remember_token\"]', '[]', '[]', 'http://127.0.0.1:8000/logout', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/90.0.4430.212 Safari/537.36', 'f1456831512d911eb1a504d189fcbddc314d255cdef4b6ce4e4058b89906e22e5d0573b2a31d1ab509ab683b3d3f7015586818764021bf29cb626f0fa924aea0', '2021-05-31 13:51:29', '2021-05-31 13:51:29'),
(8, 'App\\Models\\Auth\\User', 1, 'App\\Models\\Auth\\User', 1, 4, 'updated', '{\"id\":1,\"uuid\":\"5b4daf56-8c54-4925-8b61-373b44fea9d9\",\"first_name\":\"Alan\",\"last_name\":\"Whitmore\",\"email\":\"admin@admin.com\",\"avatar_type\":\"gravatar\",\"avatar_location\":null,\"password\":\"$2y$10$q\\/gvy0lznSFuYbl69WKCFewNyG\\/cSzaOPlRu5kxOYyLkzaWwDMk8O\",\"password_changed_at\":null,\"active\":1,\"confirmation_code\":\"520c5c05d62d62fffecd28fb3b7bed6b\",\"confirmed\":1,\"timezone\":\"America\\/New_York\",\"last_login_at\":\"2021-05-31 10:04:50\",\"last_login_ip\":\"127.0.0.1\",\"to_be_logged_out\":0,\"status\":1,\"created_by\":1,\"updated_by\":null,\"is_term_accept\":0,\"remember_token\":\"ZEADbc1CzYpP6ZRp08f1Z9zjgse4Jtg3F1IaavEYzMK0144XVKmM2mZv9MFD\",\"created_at\":\"2021-05-28 15:39:26\",\"updated_at\":\"2021-05-31 10:04:50\",\"deleted_at\":null}', '[\"last_login_at\",\"updated_at\"]', '[]', '[]', 'http://127.0.0.1:8000/login', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/90.0.4430.212 Safari/537.36', '160d518dcd6dcc448f6c1a234fc832d079960bf073d435ec81ad3704eae3bded3e66a9d600eefafaf02278cfd274752289fe388bc1c735d86d0d9e1136a7343b', '2021-05-31 14:04:50', '2021-05-31 14:04:50'),
(9, 'App\\Models\\Auth\\User', 1, 'App\\Models\\Auth\\User', 1, 4, 'updated', '{\"id\":1,\"uuid\":\"5b4daf56-8c54-4925-8b61-373b44fea9d9\",\"first_name\":\"Wiseberry\",\"last_name\":\"Admin\",\"email\":\"admin@admin.com\",\"avatar_type\":\"gravatar\",\"avatar_location\":null,\"password\":\"$2y$10$q\\/gvy0lznSFuYbl69WKCFewNyG\\/cSzaOPlRu5kxOYyLkzaWwDMk8O\",\"password_changed_at\":null,\"active\":1,\"confirmation_code\":\"520c5c05d62d62fffecd28fb3b7bed6b\",\"confirmed\":1,\"timezone\":\"America\\/New_York\",\"last_login_at\":\"2021-05-31 10:04:50\",\"last_login_ip\":\"127.0.0.1\",\"to_be_logged_out\":0,\"status\":1,\"created_by\":1,\"updated_by\":null,\"is_term_accept\":0,\"remember_token\":\"EZJdqY49L5P38LeP9S5UTig7OyUhGykaMqcuqxQRiKPMrpvrTekfGCB9gOCF\",\"created_at\":\"2021-05-28 15:39:26\",\"updated_at\":\"2021-05-31 10:04:50\",\"deleted_at\":null}', '[\"remember_token\"]', '[]', '[]', 'http://127.0.0.1:8000/logout', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/90.0.4430.212 Safari/537.36', '91c4fd6f554dd00c142cf99518c9f95f623a71a4ec68ce3a2e2fa010cb8abab4fbe964beb5980cb130db1f04690ad818fa6cfe9cec6528ddc2d431b700ec42b6', '2021-06-01 08:13:30', '2021-06-01 08:13:30'),
(10, 'App\\Models\\Auth\\User', 1, 'App\\Models\\Auth\\User', 1, 4, 'updated', '{\"id\":1,\"uuid\":\"5b4daf56-8c54-4925-8b61-373b44fea9d9\",\"first_name\":\"Wiseberry\",\"last_name\":\"Admin\",\"email\":\"admin@admin.com\",\"avatar_type\":\"gravatar\",\"avatar_location\":null,\"password\":\"$2y$10$q\\/gvy0lznSFuYbl69WKCFewNyG\\/cSzaOPlRu5kxOYyLkzaWwDMk8O\",\"password_changed_at\":null,\"active\":1,\"confirmation_code\":\"520c5c05d62d62fffecd28fb3b7bed6b\",\"confirmed\":1,\"timezone\":\"America\\/New_York\",\"last_login_at\":\"2021-06-01 04:17:28\",\"last_login_ip\":\"127.0.0.1\",\"to_be_logged_out\":0,\"status\":1,\"created_by\":1,\"updated_by\":null,\"is_term_accept\":0,\"remember_token\":\"EZJdqY49L5P38LeP9S5UTig7OyUhGykaMqcuqxQRiKPMrpvrTekfGCB9gOCF\",\"created_at\":\"2021-05-28 15:39:26\",\"updated_at\":\"2021-06-01 04:17:28\",\"deleted_at\":null}', '[\"last_login_at\",\"updated_at\"]', '[]', '[]', 'http://127.0.0.1:8000/login', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/90.0.4430.212 Safari/537.36', '125d5cb5424834a1267bcafa4573d10b838791254ca4033ce501a7a95e07efc88145fdaa1fac38d540ef9cb5026177e73ff4b5fe382e8e51bbab7e33c1f67db8', '2021-06-01 08:17:28', '2021-06-01 08:17:28'),
(11, 'App\\Models\\Auth\\User', 1, 'App\\Models\\Auth\\User', 1, 4, 'updated', '{\"id\":1,\"uuid\":\"5b4daf56-8c54-4925-8b61-373b44fea9d9\",\"first_name\":\"Wiseberry\",\"last_name\":\"Admin\",\"email\":\"admin@admin.com\",\"avatar_type\":\"gravatar\",\"avatar_location\":null,\"password\":\"$2y$10$q\\/gvy0lznSFuYbl69WKCFewNyG\\/cSzaOPlRu5kxOYyLkzaWwDMk8O\",\"password_changed_at\":null,\"active\":1,\"confirmation_code\":\"520c5c05d62d62fffecd28fb3b7bed6b\",\"confirmed\":1,\"timezone\":\"America\\/New_York\",\"last_login_at\":\"2021-06-01 04:17:28\",\"last_login_ip\":\"127.0.0.1\",\"to_be_logged_out\":0,\"status\":1,\"created_by\":1,\"updated_by\":null,\"is_term_accept\":0,\"remember_token\":\"aL2kFjkvwOfUbhjrGLpjRcRCtco0Mnd1m79vm6H4dnlrtCvJYvPVhT7U5MT1\",\"created_at\":\"2021-05-28 15:39:26\",\"updated_at\":\"2021-06-01 04:17:28\",\"deleted_at\":null}', '[\"remember_token\"]', '[]', '[]', 'http://127.0.0.1:8000/logout', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/90.0.4430.212 Safari/537.36', 'a5070da0c519bb1e2b343805458331898b97715dd2e61a46a138c97c33b66357df3dcba877b300ebbbd31d22f167a32f776a017bfd968883e39bf2406f2e67cf', '2021-06-01 08:18:19', '2021-06-01 08:18:19'),
(12, 'App\\Models\\Auth\\User', 1, 'App\\Models\\Auth\\User', 1, 4, 'updated', '{\"id\":1,\"uuid\":\"5b4daf56-8c54-4925-8b61-373b44fea9d9\",\"first_name\":\"Wiseberry\",\"last_name\":\"Admin\",\"email\":\"admin@admin.com\",\"avatar_type\":\"gravatar\",\"avatar_location\":null,\"password\":\"$2y$10$q\\/gvy0lznSFuYbl69WKCFewNyG\\/cSzaOPlRu5kxOYyLkzaWwDMk8O\",\"password_changed_at\":null,\"active\":1,\"confirmation_code\":\"520c5c05d62d62fffecd28fb3b7bed6b\",\"confirmed\":1,\"timezone\":\"America\\/New_York\",\"last_login_at\":\"2021-06-01 04:23:41\",\"last_login_ip\":\"127.0.0.1\",\"to_be_logged_out\":0,\"status\":1,\"created_by\":1,\"updated_by\":null,\"is_term_accept\":0,\"remember_token\":\"aL2kFjkvwOfUbhjrGLpjRcRCtco0Mnd1m79vm6H4dnlrtCvJYvPVhT7U5MT1\",\"created_at\":\"2021-05-28 15:39:26\",\"updated_at\":\"2021-06-01 04:23:41\",\"deleted_at\":null}', '[\"last_login_at\",\"updated_at\"]', '[]', '[]', 'http://127.0.0.1:8000/login', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/90.0.4430.212 Safari/537.36', 'a6220fd50801438b48766aee80cdc1e43d4d2df9b4c3e59e8f6542ab4493bd140d8304246b073b3808acca7421d0ec68bf4c2f2e3aae98ec5883d369eed7d020', '2021-06-01 08:23:41', '2021-06-01 08:23:41'),
(13, 'App\\Models\\Auth\\User', 1, 'App\\Models\\Auth\\User', 1, 4, 'updated', '{\"id\":1,\"uuid\":\"5b4daf56-8c54-4925-8b61-373b44fea9d9\",\"first_name\":\"Wiseberry\",\"last_name\":\"Admin\",\"email\":\"admin@admin.com\",\"avatar_type\":\"gravatar\",\"avatar_location\":null,\"password\":\"$2y$10$q\\/gvy0lznSFuYbl69WKCFewNyG\\/cSzaOPlRu5kxOYyLkzaWwDMk8O\",\"password_changed_at\":null,\"active\":1,\"confirmation_code\":\"520c5c05d62d62fffecd28fb3b7bed6b\",\"confirmed\":1,\"timezone\":\"America\\/New_York\",\"last_login_at\":\"2021-06-01 04:23:41\",\"last_login_ip\":\"127.0.0.1\",\"to_be_logged_out\":0,\"status\":1,\"created_by\":1,\"updated_by\":null,\"is_term_accept\":0,\"remember_token\":\"PACbsLfHzyW9Cj1nYCJAj4ak3AKItGKxfQREkSPifnr3JXquWzRFL1ShOpdI\",\"created_at\":\"2021-05-28 15:39:26\",\"updated_at\":\"2021-06-01 04:23:41\",\"deleted_at\":null}', '[\"remember_token\"]', '[]', '[]', 'http://127.0.0.1:8000/logout', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/90.0.4430.212 Safari/537.36', 'd8c7edeb07c6b6adba1b770edfbff770adf79f9aff68b665470d2645947ac74b454c7507162911f9b770c5ca5bf66a153130210355cbbef224580643b858b9f9', '2021-06-01 09:21:40', '2021-06-01 09:21:40'),
(14, 'App\\Models\\Auth\\User', 1, 'App\\Models\\Auth\\User', 1, 4, 'updated', '{\"id\":1,\"uuid\":\"5b4daf56-8c54-4925-8b61-373b44fea9d9\",\"first_name\":\"Wiseberry\",\"last_name\":\"Admin\",\"email\":\"admin@admin.com\",\"avatar_type\":\"gravatar\",\"avatar_location\":null,\"password\":\"$2y$10$q\\/gvy0lznSFuYbl69WKCFewNyG\\/cSzaOPlRu5kxOYyLkzaWwDMk8O\",\"password_changed_at\":null,\"active\":1,\"confirmation_code\":\"520c5c05d62d62fffecd28fb3b7bed6b\",\"confirmed\":1,\"timezone\":\"America\\/New_York\",\"last_login_at\":\"2021-06-01 06:26:25\",\"last_login_ip\":\"127.0.0.1\",\"to_be_logged_out\":0,\"status\":1,\"created_by\":1,\"updated_by\":null,\"is_term_accept\":0,\"remember_token\":\"PACbsLfHzyW9Cj1nYCJAj4ak3AKItGKxfQREkSPifnr3JXquWzRFL1ShOpdI\",\"created_at\":\"2021-05-28 15:39:26\",\"updated_at\":\"2021-06-01 06:26:25\",\"deleted_at\":null}', '[\"last_login_at\",\"updated_at\"]', '[]', '[]', 'http://127.0.0.1:8000/login', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/90.0.4430.212 Safari/537.36', '8f95b61d5d00185d45d3104d60cad06b307e4d8fc63ae72289bf78555e9832f51b7a10c79b2edef09794ef9bbfedb1bcdeedd6f5b625e5c33321a1fb615e1481', '2021-06-01 10:26:26', '2021-06-01 10:26:26'),
(15, 'App\\Models\\Auth\\User', 1, 'App\\Models\\Auth\\User', 1, 4, 'updated', '{\"id\":1,\"uuid\":\"5b4daf56-8c54-4925-8b61-373b44fea9d9\",\"first_name\":\"Wiseberry\",\"last_name\":\"Admin\",\"email\":\"admin@admin.com\",\"avatar_type\":\"gravatar\",\"avatar_location\":null,\"password\":\"$2y$10$q\\/gvy0lznSFuYbl69WKCFewNyG\\/cSzaOPlRu5kxOYyLkzaWwDMk8O\",\"password_changed_at\":null,\"active\":1,\"confirmation_code\":\"520c5c05d62d62fffecd28fb3b7bed6b\",\"confirmed\":1,\"timezone\":\"America\\/New_York\",\"last_login_at\":\"2021-06-01 06:26:25\",\"last_login_ip\":\"127.0.0.1\",\"to_be_logged_out\":0,\"status\":1,\"created_by\":1,\"updated_by\":null,\"is_term_accept\":0,\"remember_token\":\"jtI1jagAe7UC4QrfZnRJgizJDIGTmCwRlmOnrvxdKHh9Pie8ZKL6vLU2xdjO\",\"created_at\":\"2021-05-28 15:39:26\",\"updated_at\":\"2021-06-01 06:26:25\",\"deleted_at\":null}', '[\"remember_token\"]', '[]', '[]', 'http://127.0.0.1:8000/logout', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/90.0.4430.212 Safari/537.36', '11906d7b56a3e2bdeb6b67e7cbb482c7ec2969440a2c228c1cb13621931c6dae8c88e2ff560bb193dff28434e5a907887438c22939b31e96307e3ec61e204fe6', '2021-06-03 18:27:31', '2021-06-03 18:27:31'),
(16, 'App\\Models\\Auth\\User', 1, 'App\\Models\\Auth\\User', 1, 4, 'updated', '{\"id\":1,\"uuid\":\"5b4daf56-8c54-4925-8b61-373b44fea9d9\",\"first_name\":\"Wiseberry\",\"last_name\":\"Admin\",\"email\":\"admin@admin.com\",\"avatar_type\":\"gravatar\",\"avatar_location\":null,\"password\":\"$2y$10$q\\/gvy0lznSFuYbl69WKCFewNyG\\/cSzaOPlRu5kxOYyLkzaWwDMk8O\",\"password_changed_at\":null,\"active\":1,\"confirmation_code\":\"520c5c05d62d62fffecd28fb3b7bed6b\",\"confirmed\":1,\"timezone\":\"America\\/New_York\",\"last_login_at\":\"2021-06-03 14:27:43\",\"last_login_ip\":\"127.0.0.1\",\"to_be_logged_out\":0,\"status\":1,\"created_by\":1,\"updated_by\":null,\"is_term_accept\":0,\"remember_token\":\"jtI1jagAe7UC4QrfZnRJgizJDIGTmCwRlmOnrvxdKHh9Pie8ZKL6vLU2xdjO\",\"created_at\":\"2021-05-28 15:39:26\",\"updated_at\":\"2021-06-03 14:27:44\",\"deleted_at\":null}', '[\"last_login_at\",\"updated_at\"]', '[]', '[]', 'http://127.0.0.1:8000/login', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/90.0.4430.212 Safari/537.36', 'b98a3cc3f98bb0e8c2f8c05f23ada1a65bdb48d42139ef81725167c8e1b7d51d826324333c13e82f8e6749d9f948ccf965cf1290e8361934369df99a34e76266', '2021-06-03 18:27:44', '2021-06-03 18:27:44');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2016_06_01_000001_create_oauth_auth_codes_table', 1),
(4, '2016_06_01_000002_create_oauth_access_tokens_table', 1),
(5, '2016_06_01_000003_create_oauth_refresh_tokens_table', 1),
(6, '2016_06_01_000004_create_oauth_clients_table', 1),
(7, '2016_06_01_000005_create_oauth_personal_access_clients_table', 1),
(8, '2017_09_03_144628_create_permission_tables', 1),
(9, '2017_09_11_174816_create_social_accounts_table', 1),
(10, '2017_09_26_140332_create_cache_table', 1),
(11, '2017_09_26_140528_create_sessions_table', 1),
(12, '2017_09_26_140609_create_jobs_table', 1),
(13, '2017_11_02_060149_create_blog_categories_table', 1),
(14, '2017_11_02_060149_create_blog_map_categories_table', 1),
(15, '2017_11_02_060149_create_blog_map_tags_table', 1),
(16, '2017_11_02_060149_create_blog_tags_table', 1),
(17, '2017_11_02_060149_create_blogs_table', 1),
(18, '2017_11_02_060149_create_faqs_table', 1),
(19, '2017_11_02_060149_create_pages_table', 1),
(20, '2018_04_08_033256_create_password_histories_table', 1),
(21, '2018_11_21_000001_create_ledgers_table', 1),
(22, '2019_08_19_000000_create_failed_jobs_table', 1),
(23, '2020_06_11_080530_create_email_templates_table', 1),
(24, '2020_06_18_060624_add_foreign_key_constraints_to_acl_tables', 1);

-- --------------------------------------------------------

--
-- Table structure for table `oauth_access_tokens`
--

CREATE TABLE `oauth_access_tokens` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` bigint(20) UNSIGNED DEFAULT NULL,
  `client_id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `scopes` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `revoked` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `expires_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `oauth_auth_codes`
--

CREATE TABLE `oauth_auth_codes` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `client_id` bigint(20) UNSIGNED NOT NULL,
  `scopes` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `revoked` tinyint(1) NOT NULL,
  `expires_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `oauth_clients`
--

CREATE TABLE `oauth_clients` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED DEFAULT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `secret` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `provider` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `redirect` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `personal_access_client` tinyint(1) NOT NULL,
  `password_client` tinyint(1) NOT NULL,
  `revoked` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `oauth_clients`
--

INSERT INTO `oauth_clients` (`id`, `user_id`, `name`, `secret`, `provider`, `redirect`, `personal_access_client`, `password_client`, `revoked`, `created_at`, `updated_at`) VALUES
(1, NULL, 'Laravel Starter Personal Access Client', 'MTniBJSzunStqzYSGZqYK34OSC1cYgjA8987hiZu', NULL, 'http://localhost', 1, 0, 0, '2021-05-28 18:01:06', '2021-05-28 18:01:06'),
(2, NULL, 'Laravel Starter Password Grant Client', 'Dylk4unA8pL8o95YCgPUK5rNYoJRG0AfeJ8LtS60', 'users', 'http://localhost', 0, 1, 0, '2021-05-28 18:01:06', '2021-05-28 18:01:06');

-- --------------------------------------------------------

--
-- Table structure for table `oauth_personal_access_clients`
--

CREATE TABLE `oauth_personal_access_clients` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `client_id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `oauth_personal_access_clients`
--

INSERT INTO `oauth_personal_access_clients` (`id`, `client_id`, `created_at`, `updated_at`) VALUES
(1, 1, '2021-05-28 18:01:06', '2021-05-28 18:01:06');

-- --------------------------------------------------------

--
-- Table structure for table `oauth_refresh_tokens`
--

CREATE TABLE `oauth_refresh_tokens` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `access_token_id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `revoked` tinyint(1) NOT NULL,
  `expires_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `pages`
--

CREATE TABLE `pages` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `page_slug` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `cannonical_link` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `seo_title` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `seo_keyword` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `seo_description` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` tinyint(1) NOT NULL DEFAULT 1,
  `created_by` int(10) UNSIGNED NOT NULL,
  `updated_by` int(10) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `pagedata` text COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `pages`
--

INSERT INTO `pages` (`id`, `title`, `page_slug`, `description`, `cannonical_link`, `seo_title`, `seo_keyword`, `seo_description`, `status`, `created_by`, `updated_by`, `created_at`, `updated_at`, `deleted_at`, `pagedata`) VALUES
(2, 'Home', '', '<p>Wiseberry is a real estate company with a difference. Our core objective is to make the buying and selling process enjoyable both emotionally and financially for all parties involved. We aim to foster an environment where clients can expect speed and swiftness of service at the highest level of professionalism and dependability.</p>\r\n<p>Wiseberry does not believe that large sums of money should be spent in attracting clients, but that money should instead be spent in attracting and training the right people in real estate, ultimately benefiting all parties involved in real estate transactions.</p>', NULL, NULL, NULL, NULL, 0, 1, 1, '2021-06-05 05:46:25', '2021-06-08 04:20:37', NULL, '{\"title\":\"Home\",\"why\":{\"desc\":\"<p>Wiseberry is a real estate company with a difference. Our core objective is to make the buying and selling process enjoyable both emotionally and financially for all parties involved. We aim to foster an environment where clients can expect speed and swiftness of service at the highest level of professionalism and dependability.<\\/p>\\r\\n<p>Wiseberry does not believe that large sums of money should be spent in attracting clients, but that money should instead be spent in attracting and training the right people in real estate, ultimately benefiting all parties involved in real estate transactions.<\\/p>\",\"image\":\"1623111637.jpeg\",\"button_link\":\"http:\\/\\/szwebprofile.com\\/PHP\\/wiseberry\\/public\\/homepage\"},\"ofc\":{\"desc\":\"<p>If you ask any one of our offices what sets Wiseberry apart from other real estate groups, you will receive an unanimous answer; our culture.<\\/p>\\r\\n<p>All aspects of Wiseberry reflect our underlying culture of conducting business in a caring, trustworthy, transparent, ethical and honest and professional manner.<\\/p>\",\"image\":\"ofc_1623111637.jpeg\",\"button_link\":\"http:\\/\\/szwebprofile.com\\/PHP\\/wiseberry\\/public\\/homepages\"}}'),
(4, 'Menu', '2', '', NULL, NULL, NULL, NULL, 0, 1, 1, '2021-06-05 05:46:25', '2021-06-11 04:29:28', NULL, '{\"title\":\"Menu\",\"menus\":[{\"title\":\"Buy\",\"submenu\":[{\"title\":\"Search\",\"link\":\"http:\\/\\/szwebprofile.com\\/PHP\\/wiseberry\\/public\\/search\"},{\"title\":\"Open homes\",\"link\":\"http:\\/\\/szwebprofile.com\\/PHP\\/wiseberry\\/public\\/search\"},{\"title\":\"Auctions\",\"link\":\"http:\\/\\/szwebprofile.com\\/PHP\\/wiseberry\\/public\\/search\"},{\"title\":\"Off Market\",\"link\":\"http:\\/\\/szwebprofile.com\\/PHP\\/wiseberry\\/public\\/search\"},{\"title\":\"Wise Move\",\"link\":\"http:\\/\\/szwebprofile.com\\/PHP\\/wiseberry\\/public\\/search\"},{\"title\":\"Finance Solutions\",\"link\":\"http:\\/\\/szwebprofile.com\\/PHP\\/wiseberry\\/public\\/search\"}]},{\"title\":\"RENT\",\"submenu\":[{\"title\":\"Search\",\"link\":\"http:\\/\\/szwebprofile.com\\/PHP\\/wiseberry\\/public\\/search\"},{\"title\":\"Open Homes\",\"link\":\"http:\\/\\/szwebprofile.com\\/PHP\\/wiseberry\\/public\\/search\"},{\"title\":\"Renting With Wiseberry\",\"link\":\"http:\\/\\/szwebprofile.com\\/PHP\\/wiseberry\\/public\\/search\"},{\"title\":\"Applying For A Property\",\"link\":\"http:\\/\\/szwebprofile.com\\/PHP\\/wiseberry\\/public\\/search\"},{\"title\":\"Notice To Repair\",\"link\":\"http:\\/\\/szwebprofile.com\\/PHP\\/wiseberry\\/public\\/search\"},{\"title\":\"Notice To Vacate\",\"link\":\"http:\\/\\/szwebprofile.com\\/PHP\\/wiseberry\\/public\\/search\"}]},{\"title\":\"SELL\",\"submenu\":[{\"title\":\"Selling With Wiseberry\",\"link\":\"http:\\/\\/szwebprofile.com\\/PHP\\/wiseberry\\/public\\/search\"},{\"title\":\"Appraisal\",\"link\":\"http:\\/\\/szwebprofile.com\\/PHP\\/wiseberry\\/public\\/search\"},{\"title\":\"Recently Sold\",\"link\":\"http:\\/\\/szwebprofile.com\\/PHP\\/wiseberry\\/public\\/search\"}]},{\"title\":\"MANAGE\",\"submenu\":[{\"title\":\"Managing With Wiseberry\",\"link\":\"http:\\/\\/szwebprofile.com\\/PHP\\/wiseberry\\/public\\/search\"},{\"title\":\"Appraisal\",\"link\":\"http:\\/\\/szwebprofile.com\\/PHP\\/wiseberry\\/public\\/search\"},{\"title\":\"Recently Leased\",\"link\":\"http:\\/\\/szwebprofile.com\\/PHP\\/wiseberry\\/public\\/search\"},{\"title\":\"Switching To Wiseberry\",\"link\":\"http:\\/\\/szwebprofile.com\\/PHP\\/wiseberry\\/public\\/search\"}]},{\"title\":\"ABOUT US\",\"submenu\":[{\"title\":\"About Us\",\"link\":\"http:\\/\\/szwebprofile.com\\/PHP\\/wiseberry\\/public\\/search\"},{\"title\":\"Work With Us\",\"link\":\"http:\\/\\/szwebprofile.com\\/PHP\\/wiseberry\\/public\\/search\"},{\"title\":\"Open Your Own Business\",\"link\":\"http:\\/\\/szwebprofile.com\\/PHP\\/wiseberry\\/public\\/search\"},{\"title\":\"Blogs\",\"link\":\"http:\\/\\/szwebprofile.com\\/PHP\\/wiseberry\\/public\\/search\"},{\"title\":\"Subscribe\",\"link\":\"http:\\/\\/szwebprofile.com\\/PHP\\/wiseberry\\/public\\/search\"}]},{\"title\":\"CONTACT US\",\"submenu\":[{\"title\":\"Contact Us\",\"link\":\"http:\\/\\/szwebprofile.com\\/PHP\\/wiseberry\\/public\\/search\"},{\"title\":\"Find An Office\",\"link\":\"http:\\/\\/szwebprofile.com\\/PHP\\/wiseberry\\/public\\/search\"},{\"title\":\"Find An Agent\",\"link\":\"http:\\/\\/szwebprofile.com\\/PHP\\/wiseberry\\/public\\/search\"}]}]}'),
(5, 'Search', '3', '<p>test</p>', NULL, NULL, NULL, NULL, 0, 1, 1, '2021-06-05 05:46:25', '2021-06-12 03:32:59', NULL, '{\"title\":\"Search\",\"wf\":{\"desc\":\"<p>test<\\/p>\",\"image\":\"1623454379.jpeg\",\"button_link\":\"http:\\/\\/szwebprofile.com\\/PHP\\/wiseberry\\/public\\/homepage\"},\"wmm\":{\"desc\":\"<p>If you ask any one of our offices what sets Wiseberry apart from other real estate groups, you will receive an unanimous answer; our culture.<\\/p>\\r\\n<p>All aspects of Wiseberry reflect our underlying culture of conducting business in a caring, trustworthy, transparent, ethical and honest and professional manner.<\\/p>\",\"image\":\"wmm_1623454379.jpeg\",\"button_link\":\"http:\\/\\/szwebprofile.com\\/PHP\\/wiseberry\\/public\\/homepages\"}}');

-- --------------------------------------------------------

--
-- Table structure for table `password_histories`
--

CREATE TABLE `password_histories` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `password_histories`
--

INSERT INTO `password_histories` (`id`, `user_id`, `password`, `created_at`, `updated_at`) VALUES
(1, 4, '$2y$10$xKe4HuZHMwEhgAy4oc/gL.kuzyTBG2abqUCDi7tId7os6NyLCDS3O', '2021-05-28 18:01:26', '2021-05-28 18:01:26'),
(2, 5, '$2y$10$/bLOZXcBir3MLwkCzE6gM.rK0pTilU4oU6HM.AqWxYNVli5mBEnL2', '2021-05-28 18:01:26', '2021-05-28 18:01:26');

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `permissions`
--

CREATE TABLE `permissions` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `sort` smallint(5) UNSIGNED NOT NULL DEFAULT 0,
  `status` tinyint(1) NOT NULL DEFAULT 1,
  `created_by` int(10) UNSIGNED DEFAULT NULL,
  `updated_by` int(10) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `permissions`
--

INSERT INTO `permissions` (`id`, `name`, `display_name`, `sort`, `status`, `created_by`, `updated_by`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'view-backend', 'View Backend', 1, 1, 1, NULL, '2021-05-28 18:01:23', '2021-05-28 18:01:23', NULL),
(2, 'view-frontend', 'View Frontend', 2, 1, 1, NULL, '2021-05-28 18:01:23', '2021-05-28 18:01:23', NULL),
(3, 'view-access-management', 'View Access Management', 3, 1, 1, NULL, '2021-05-28 18:01:23', '2021-05-28 18:01:23', NULL),
(4, 'view-user-management', 'View User Management', 4, 1, 1, NULL, '2021-05-28 18:01:23', '2021-05-28 18:01:23', NULL),
(5, 'view-active-user', 'View Active User', 5, 1, 1, NULL, '2021-05-28 18:01:23', '2021-05-28 18:01:23', NULL),
(6, 'view-deactive-user', 'View Deactive User', 6, 1, 1, NULL, '2021-05-28 18:01:23', '2021-05-28 18:01:23', NULL),
(7, 'view-deleted-user', 'View Deleted User', 7, 1, 1, NULL, '2021-05-28 18:01:23', '2021-05-28 18:01:23', NULL),
(8, 'show-user', 'Show User Details', 8, 1, 1, NULL, '2021-05-28 18:01:23', '2021-05-28 18:01:23', NULL),
(9, 'create-user', 'Create User', 9, 1, 1, NULL, '2021-05-28 18:01:23', '2021-05-28 18:01:23', NULL),
(10, 'edit-user', 'Edit User', 9, 1, 1, NULL, '2021-05-28 18:01:23', '2021-05-28 18:01:23', NULL),
(11, 'delete-user', 'Delete User', 10, 1, 1, NULL, '2021-05-28 18:01:23', '2021-05-28 18:01:23', NULL),
(12, 'activate-user', 'Activate User', 11, 1, 1, NULL, '2021-05-28 18:01:23', '2021-05-28 18:01:23', NULL),
(13, 'deactivate-user', 'Deactivate User', 12, 1, 1, NULL, '2021-05-28 18:01:23', '2021-05-28 18:01:23', NULL),
(14, 'login-as-user', 'Login As User', 13, 1, 1, NULL, '2021-05-28 18:01:23', '2021-05-28 18:01:23', NULL),
(15, 'clear-user-session', 'Clear User Session', 14, 1, 1, NULL, '2021-05-28 18:01:23', '2021-05-28 18:01:23', NULL),
(16, 'view-role-management', 'View Role Management', 15, 1, 1, NULL, '2021-05-28 18:01:23', '2021-05-28 18:01:23', NULL),
(17, 'create-role', 'Create Role', 16, 1, 1, NULL, '2021-05-28 18:01:23', '2021-05-28 18:01:23', NULL),
(18, 'edit-role', 'Edit Role', 17, 1, 1, NULL, '2021-05-28 18:01:23', '2021-05-28 18:01:23', NULL),
(19, 'delete-role', 'Delete Role', 18, 1, 1, NULL, '2021-05-28 18:01:23', '2021-05-28 18:01:23', NULL),
(20, 'view-permission-management', 'View Permission Management', 19, 1, 1, NULL, '2021-05-28 18:01:23', '2021-05-28 18:01:23', NULL),
(21, 'create-permission', 'Create Permission', 20, 1, 1, NULL, '2021-05-28 18:01:23', '2021-05-28 18:01:23', NULL),
(22, 'edit-permission', 'Edit Permission', 21, 1, 1, NULL, '2021-05-28 18:01:23', '2021-05-28 18:01:23', NULL),
(23, 'delete-permission', 'Delete Permission', 22, 1, 1, NULL, '2021-05-28 18:01:23', '2021-05-28 18:01:23', NULL),
(24, 'view-page', 'View Page', 23, 1, 1, NULL, '2021-05-28 18:01:23', '2021-05-28 18:01:23', NULL),
(25, 'create-page', 'Create Page', 24, 1, 1, NULL, '2021-05-28 18:01:23', '2021-05-28 18:01:23', NULL),
(26, 'edit-page', 'Edit Page', 25, 1, 1, NULL, '2021-05-28 18:01:23', '2021-05-28 18:01:23', NULL),
(27, 'delete-page', 'Delete Page', 26, 1, 1, NULL, '2021-05-28 18:01:23', '2021-05-28 18:01:23', NULL),
(28, 'view-email-template', 'View Email Templates', 27, 1, 1, NULL, '2021-05-28 18:01:23', '2021-05-28 18:01:23', NULL),
(29, 'create-email-template', 'Create Email Templates', 28, 1, 1, NULL, '2021-05-28 18:01:23', '2021-05-28 18:01:23', NULL),
(30, 'edit-email-template', 'Edit Email Templates', 29, 1, 1, NULL, '2021-05-28 18:01:23', '2021-05-28 18:01:23', NULL),
(31, 'delete-email-template', 'Delete Email Templates', 30, 1, 1, NULL, '2021-05-28 18:01:23', '2021-05-28 18:01:23', NULL),
(32, 'edit-settings', 'Edit Settings', 31, 1, 1, NULL, '2021-05-28 18:01:23', '2021-05-28 18:01:23', NULL),
(33, 'view-blog-category', 'View Blog Categories Management', 32, 1, 1, NULL, '2021-05-28 18:01:23', '2021-05-28 18:01:23', NULL),
(34, 'create-blog-category', 'Create Blog Category', 33, 1, 1, NULL, '2021-05-28 18:01:23', '2021-05-28 18:01:23', NULL),
(35, 'edit-blog-category', 'Edit Blog Category', 34, 1, 1, NULL, '2021-05-28 18:01:23', '2021-05-28 18:01:23', NULL),
(36, 'delete-blog-category', 'Delete Blog Category', 35, 1, 1, NULL, '2021-05-28 18:01:23', '2021-05-28 18:01:23', NULL),
(37, 'view-blog-tag', 'View Blog Tags Management', 36, 1, 1, NULL, '2021-05-28 18:01:23', '2021-05-28 18:01:23', NULL),
(38, 'create-blog-tag', 'Create Blog Tag', 37, 1, 1, NULL, '2021-05-28 18:01:23', '2021-05-28 18:01:23', NULL),
(39, 'edit-blog-tag', 'Edit Blog Tag', 38, 1, 1, NULL, '2021-05-28 18:01:23', '2021-05-28 18:01:23', NULL),
(40, 'delete-blog-tag', 'Delete Blog Tag', 39, 1, 1, NULL, '2021-05-28 18:01:23', '2021-05-28 18:01:23', NULL),
(41, 'view-blog', 'View Blogs Management', 40, 1, 1, NULL, '2021-05-28 18:01:23', '2021-05-28 18:01:23', NULL),
(42, 'create-blog', 'Create Blog', 41, 1, 1, NULL, '2021-05-28 18:01:23', '2021-05-28 18:01:23', NULL),
(43, 'edit-blog', 'Edit Blog', 42, 1, 1, NULL, '2021-05-28 18:01:23', '2021-05-28 18:01:23', NULL),
(44, 'delete-blog', 'Delete Blog', 43, 1, 1, NULL, '2021-05-28 18:01:23', '2021-05-28 18:01:23', NULL),
(45, 'view-faq', 'View FAQ Management', 44, 1, 1, NULL, '2021-05-28 18:01:23', '2021-05-28 18:01:23', NULL),
(46, 'create-faq', 'Create FAQ', 45, 1, 1, NULL, '2021-05-28 18:01:23', '2021-05-28 18:01:23', NULL),
(47, 'edit-faq', 'Edit FAQ', 46, 1, 1, NULL, '2021-05-28 18:01:23', '2021-05-28 18:01:23', NULL),
(48, 'delete-faq', 'Delete FAQ', 47, 1, 1, NULL, '2021-05-28 18:01:23', '2021-05-28 18:01:23', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `permission_role`
--

CREATE TABLE `permission_role` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `permission_id` bigint(20) UNSIGNED NOT NULL,
  `role_id` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `permission_role`
--

INSERT INTO `permission_role` (`id`, `permission_id`, `role_id`) VALUES
(1, 1, 2),
(2, 3, 2),
(3, 4, 2),
(4, 5, 2),
(5, 6, 2),
(6, 7, 2),
(7, 8, 2),
(8, 16, 2),
(9, 20, 2),
(10, 24, 2),
(11, 25, 2),
(12, 26, 2),
(13, 27, 2),
(14, 28, 2),
(15, 29, 2),
(16, 30, 2),
(17, 31, 2),
(18, 33, 2),
(19, 34, 2),
(20, 35, 2),
(21, 36, 2),
(22, 37, 2),
(23, 38, 2),
(24, 39, 2),
(25, 40, 2),
(26, 41, 2),
(27, 42, 2),
(28, 43, 2),
(29, 44, 2),
(30, 45, 2),
(31, 46, 2),
(32, 47, 2),
(33, 48, 2),
(34, 2, 3);

-- --------------------------------------------------------

--
-- Table structure for table `permission_user`
--

CREATE TABLE `permission_user` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `permission_id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `permission_user`
--

INSERT INTO `permission_user` (`id`, `permission_id`, `user_id`) VALUES
(1, 42, 2),
(2, 34, 2),
(3, 38, 2),
(4, 29, 2),
(5, 46, 2),
(6, 25, 2),
(7, 44, 2),
(8, 36, 2),
(9, 40, 2),
(10, 31, 2),
(11, 48, 2),
(12, 27, 2),
(13, 43, 2),
(14, 35, 2),
(15, 39, 2),
(16, 30, 2),
(17, 47, 2),
(18, 26, 2),
(19, 8, 2),
(20, 3, 2),
(21, 5, 2),
(22, 1, 2),
(23, 33, 2),
(24, 37, 2),
(25, 41, 2),
(26, 6, 2),
(27, 7, 2),
(28, 28, 2),
(29, 45, 2),
(30, 24, 2),
(31, 20, 2),
(32, 16, 2),
(33, 4, 2),
(34, 2, 3);

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

CREATE TABLE `roles` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `all` tinyint(1) NOT NULL DEFAULT 0,
  `sort` smallint(5) UNSIGNED NOT NULL DEFAULT 0,
  `status` tinyint(1) NOT NULL DEFAULT 1,
  `created_by` int(10) UNSIGNED DEFAULT NULL,
  `updated_by` int(10) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `roles`
--

INSERT INTO `roles` (`id`, `name`, `all`, `sort`, `status`, `created_by`, `updated_by`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'Administrator', 1, 1, 1, 1, NULL, '2021-05-28 18:01:22', '2021-05-28 18:01:22', NULL),
(2, 'Executive', 0, 2, 1, 1, NULL, '2021-05-28 18:01:22', '2021-05-28 18:01:22', NULL),
(3, 'User', 0, 3, 1, 1, NULL, '2021-05-28 18:01:22', '2021-05-28 18:01:22', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `role_user`
--

CREATE TABLE `role_user` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `role_id` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `role_user`
--

INSERT INTO `role_user` (`id`, `user_id`, `role_id`) VALUES
(1, 1, 1),
(2, 2, 2),
(3, 3, 3);

-- --------------------------------------------------------

--
-- Table structure for table `sessions`
--

CREATE TABLE `sessions` (
  `id` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` int(10) UNSIGNED DEFAULT NULL,
  `ip_address` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `user_agent` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `payload` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `last_activity` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `social_accounts`
--

CREATE TABLE `social_accounts` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `provider` varchar(32) COLLATE utf8mb4_unicode_ci NOT NULL,
  `provider_id` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `avatar` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `uuid` char(36) COLLATE utf8mb4_unicode_ci NOT NULL,
  `first_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `last_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `avatar_type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'gravatar',
  `avatar_location` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `password_changed_at` timestamp NULL DEFAULT NULL,
  `active` tinyint(3) UNSIGNED NOT NULL DEFAULT 1,
  `confirmation_code` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `confirmed` tinyint(1) NOT NULL DEFAULT 0,
  `timezone` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `last_login_at` timestamp NULL DEFAULT NULL,
  `last_login_ip` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `to_be_logged_out` tinyint(1) NOT NULL DEFAULT 0,
  `status` tinyint(1) NOT NULL DEFAULT 1,
  `created_by` int(10) UNSIGNED DEFAULT NULL,
  `updated_by` int(10) UNSIGNED DEFAULT NULL,
  `is_term_accept` tinyint(1) NOT NULL DEFAULT 0 COMMENT ' 0 = not accepted,1 = accepted',
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `uuid`, `first_name`, `last_name`, `email`, `avatar_type`, `avatar_location`, `password`, `password_changed_at`, `active`, `confirmation_code`, `confirmed`, `timezone`, `last_login_at`, `last_login_ip`, `to_be_logged_out`, `status`, `created_by`, `updated_by`, `is_term_accept`, `remember_token`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, '5b4daf56-8c54-4925-8b61-373b44fea9d9', 'Wiseberry', 'Admin', 'admin@admin.com', 'gravatar', NULL, '$2y$10$q/gvy0lznSFuYbl69WKCFewNyG/cSzaOPlRu5kxOYyLkzaWwDMk8O', NULL, 1, '520c5c05d62d62fffecd28fb3b7bed6b', 1, 'America/New_York', '2021-06-03 18:27:43', '127.0.0.1', 0, 1, 1, NULL, 0, 'jtI1jagAe7UC4QrfZnRJgizJDIGTmCwRlmOnrvxdKHh9Pie8ZKL6vLU2xdjO', '2021-05-28 19:39:26', '2021-06-03 18:27:44', NULL),
(2, 'baba8ae9-96a9-4332-8449-9fc9e54c6554', 'Justin', 'Bevan', 'executive@executive.com', 'gravatar', NULL, '$2y$10$04q5hLlKkxLbKgWPqZvE2uvcL8Dw.pwvCZF6DE/xBVxQcK489lHba', NULL, 1, 'f58e893a5b615809fae75515bf705e25', 1, NULL, NULL, NULL, 0, 1, 1, NULL, 0, NULL, '2021-05-28 19:39:26', '2021-05-28 19:39:26', NULL),
(3, '0aa6561c-b766-4535-9aca-0631d2b46f73', 'User', 'Test', 'user@user.com', 'gravatar', NULL, '$2y$10$UesRCAKtLigtie4kO6AY8.TXGHTQ/KaCpdrPnJn1mTT0lHQPxaSt2', NULL, 1, '0e96f21ee3b3a3acbd76a24fb0ebb34b', 1, NULL, NULL, NULL, 0, 1, 1, NULL, 0, NULL, '2021-05-28 19:39:26', '2021-05-28 19:39:26', NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `blogs`
--
ALTER TABLE `blogs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `blog_categories`
--
ALTER TABLE `blog_categories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `blog_map_categories`
--
ALTER TABLE `blog_map_categories`
  ADD PRIMARY KEY (`id`),
  ADD KEY `blog_map_categories_blog_id_index` (`blog_id`),
  ADD KEY `blog_map_categories_category_id_index` (`category_id`);

--
-- Indexes for table `blog_map_tags`
--
ALTER TABLE `blog_map_tags`
  ADD PRIMARY KEY (`id`),
  ADD KEY `blog_map_tags_blog_id_index` (`blog_id`),
  ADD KEY `blog_map_tags_tag_id_index` (`tag_id`);

--
-- Indexes for table `blog_tags`
--
ALTER TABLE `blog_tags`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cache`
--
ALTER TABLE `cache`
  ADD UNIQUE KEY `cache_key_unique` (`key`);

--
-- Indexes for table `email_templates`
--
ALTER TABLE `email_templates`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `faqs`
--
ALTER TABLE `faqs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `jobs`
--
ALTER TABLE `jobs`
  ADD PRIMARY KEY (`id`),
  ADD KEY `jobs_queue_index` (`queue`);

--
-- Indexes for table `ledgers`
--
ALTER TABLE `ledgers`
  ADD PRIMARY KEY (`id`),
  ADD KEY `ledgers_recordable_type_recordable_id_index` (`recordable_type`,`recordable_id`),
  ADD KEY `ledgers_user_id_user_type_index` (`user_id`,`user_type`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `oauth_access_tokens`
--
ALTER TABLE `oauth_access_tokens`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_access_tokens_user_id_index` (`user_id`);

--
-- Indexes for table `oauth_auth_codes`
--
ALTER TABLE `oauth_auth_codes`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_auth_codes_user_id_index` (`user_id`);

--
-- Indexes for table `oauth_clients`
--
ALTER TABLE `oauth_clients`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_clients_user_id_index` (`user_id`);

--
-- Indexes for table `oauth_personal_access_clients`
--
ALTER TABLE `oauth_personal_access_clients`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `oauth_refresh_tokens`
--
ALTER TABLE `oauth_refresh_tokens`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pages`
--
ALTER TABLE `pages`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `pages_page_slug_unique` (`page_slug`);

--
-- Indexes for table `password_histories`
--
ALTER TABLE `password_histories`
  ADD PRIMARY KEY (`id`),
  ADD KEY `password_histories_user_id_foreign` (`user_id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `permissions`
--
ALTER TABLE `permissions`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `permissions_name_unique` (`name`);

--
-- Indexes for table `permission_role`
--
ALTER TABLE `permission_role`
  ADD PRIMARY KEY (`id`),
  ADD KEY `permission_role_permission_id_foreign` (`permission_id`),
  ADD KEY `permission_role_role_id_foreign` (`role_id`);

--
-- Indexes for table `permission_user`
--
ALTER TABLE `permission_user`
  ADD PRIMARY KEY (`id`),
  ADD KEY `permission_user_permission_id_foreign` (`permission_id`),
  ADD KEY `permission_user_user_id_foreign` (`user_id`);

--
-- Indexes for table `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `roles_name_unique` (`name`);

--
-- Indexes for table `role_user`
--
ALTER TABLE `role_user`
  ADD PRIMARY KEY (`id`),
  ADD KEY `role_user_user_id_foreign` (`user_id`),
  ADD KEY `role_user_role_id_foreign` (`role_id`);

--
-- Indexes for table `sessions`
--
ALTER TABLE `sessions`
  ADD UNIQUE KEY `sessions_id_unique` (`id`);

--
-- Indexes for table `social_accounts`
--
ALTER TABLE `social_accounts`
  ADD PRIMARY KEY (`id`),
  ADD KEY `social_accounts_user_id_foreign` (`user_id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `blogs`
--
ALTER TABLE `blogs`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `blog_categories`
--
ALTER TABLE `blog_categories`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `blog_map_categories`
--
ALTER TABLE `blog_map_categories`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `blog_map_tags`
--
ALTER TABLE `blog_map_tags`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `blog_tags`
--
ALTER TABLE `blog_tags`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `email_templates`
--
ALTER TABLE `email_templates`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=51;

--
-- AUTO_INCREMENT for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `faqs`
--
ALTER TABLE `faqs`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `jobs`
--
ALTER TABLE `jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `ledgers`
--
ALTER TABLE `ledgers`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;

--
-- AUTO_INCREMENT for table `oauth_clients`
--
ALTER TABLE `oauth_clients`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `oauth_personal_access_clients`
--
ALTER TABLE `oauth_personal_access_clients`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `pages`
--
ALTER TABLE `pages`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `password_histories`
--
ALTER TABLE `password_histories`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `permissions`
--
ALTER TABLE `permissions`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=49;

--
-- AUTO_INCREMENT for table `permission_role`
--
ALTER TABLE `permission_role`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=35;

--
-- AUTO_INCREMENT for table `permission_user`
--
ALTER TABLE `permission_user`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=35;

--
-- AUTO_INCREMENT for table `roles`
--
ALTER TABLE `roles`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `role_user`
--
ALTER TABLE `role_user`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `social_accounts`
--
ALTER TABLE `social_accounts`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `password_histories`
--
ALTER TABLE `password_histories`
  ADD CONSTRAINT `password_histories_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`);

--
-- Constraints for table `permission_role`
--
ALTER TABLE `permission_role`
  ADD CONSTRAINT `permission_role_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `permission_role_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `permission_user`
--
ALTER TABLE `permission_user`
  ADD CONSTRAINT `permission_user_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `permission_user_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `role_user`
--
ALTER TABLE `role_user`
  ADD CONSTRAINT `role_user_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `role_user_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `social_accounts`
--
ALTER TABLE `social_accounts`
  ADD CONSTRAINT `social_accounts_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
