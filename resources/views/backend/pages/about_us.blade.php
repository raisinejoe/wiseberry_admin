<div class="card-body">
    <div class="row">
        <div class="col-sm-5">
            <h4 class="card-title mb-0">
                {{ __('labels.backend.access.pages.management') }}
                <small class="text-muted">{{ (isset($page)) ? __('labels.backend.access.pages.edit') : __('labels.backend.access.pages.create') }}</small>
            </h4>
        </div>
        <!--col-->
    </div>
    <!--row-->

    <hr>

    <div class="row mt-4 mb-4">

        <div class="col">
            
            <div class="form-group row">
                <div class="col-md-2">
                    Title
                </div>
                @if(isset($pagedata))    
                    <div class="col-md-10">
                        <input type="text" value="{{$pagedata->title}}"  name="title" class="form-control">
                    </div> 
                @else
                    <div class="col-md-10">
                        <input type="text"  name="title" class="form-control">
                    </div> 
                @endif
            </div><br><br>
            <div class="form-group row">
                <div class="col-md-2">
                    Meta Title
                </div>
                @if(isset($pagedata))    
                    <div class="col-md-10">
                        <input type="text" value="{{$pagedata->meta_title}}" name="meta_title" class="form-control">
                    </div> 
                @else
                    <div class="col-md-10">
                        <input type="text"  name="meta_title" class="form-control">
                    </div> 
                @endif
            </div><br><br>
            <div class="form-group row">
                <div class="col-md-2">
                    Meta Keywords
                </div>
                @if(isset($pagedata))    
                    <div class="col-md-10">
                        <input type="text" value="{{$pagedata->meta_keywords}}" name="meta_keywords" class="form-control">
                    </div> 
                @else
                    <div class="col-md-10">
                        <input type="text"  name="meta_keywords" class="form-control">
                    </div> 
                @endif
            </div><br><br>
            <div class="form-group row">
                <div class="col-md-2">
                    Meta Description
                </div>
                @if(isset($pagedata))    
                    <div class="col-md-10">
                        <textarea name="meta_desc" class="form-control">{{$pagedata->meta_desc}}</textarea> 
                    </div> 
                @else
                    <div class="col-md-10">
                        <input type="text"  name="meta_desc" class="form-control">
                    </div> 
                @endif
            </div><br><br>
            
            <div class="form-group row">
                <div class="col-md-2">
                    Banner
                </div>

                <div class="col-md-10">
                    <input type="file" name="banner_image" class="form-control">

                    @if(isset($pagedata))    
                        <br><img width="100" height="100" src="{{$page->url}}{{$pagedata->banner}}">


                    @endif
                </div>
                <!--col-->
            </div>

            <h4>OUR MISSION</h4><br><br>

            <div class="form-group row">
                <div class="col-md-2">
                    Heading
                </div>

                @if(isset($pagedata))    
                    <div class="col-md-10">
                        <input type="text"  value="{{$data_p1->heading}}" name="heading_1" class="form-control">
                    </div> 
                @else
                    <div class="col-md-10">
                        <input type="text"   name="heading_1" class="form-control">
                    </div> 
                @endif
                
            </div>
            
            <div class="form-group row">
                <div class="col-md-2">
                    Upload
                </div>

                <div class="col-md-10">
                    <input type="file" name="image" class="form-control">

                    @if(isset($pagedata))    
                        <br><img width="100" height="100" src="{{$page->url}}{{$data_p1->image}}">

                        <input type="hidden" name="image_1" value="{{$data_p1->image}}">
                        <input type="hidden" name="image_2" value="{{$pagedata->banner}}">
                        <input type="hidden" name="image_3" value="{{$data_p2->image}}">
                        <input type="hidden" name="image_4" value="{{$data_p4->image}}">
                        <input type="hidden" name="image_5" value="{{$data_p5->image}}">

                    @endif
                </div>
                <!--col-->
            </div>

            <div class="form-group row">
                {{ Form::label('description', trans('validation.attributes.backend.access.pages.description'), ['class' => 'col-md-2 from-control-label ']) }}

                <div class="col-md-10">
                    @if(isset($pagedata))
                        <textarea class="form-control" name="description">{{$data_p1->desc}}</textarea>
                    @else
                        <textarea class="form-control" name="description"></textarea>
                    @endif
                </div>
                <!--col-->
            </div><br><br>


            <h4>OUR CULTURE</h4><br><br>

            <div class="form-group row">
                <div class="col-md-2">
                    Heading
                </div>

                @if(isset($pagedata))    
                    <div class="col-md-10">
                        <input type="text"  value="{{$data_p2->heading}}" name="heading_2" class="form-control">
                    </div> 
                @else
                    <div class="col-md-10">
                        <input type="text"   name="heading_2" class="form-control">
                    </div> 
                @endif
                
            </div>
            
            <div class="form-group row">
                <div class="col-md-2">
                    Upload
                </div>

                <div class="col-md-10">
                    <input type="file" name="image_p2" class="form-control">

                    @if(isset($pagedata))    
                        <br><img width="100" height="100" src="{{$page->url}}{{$data_p2->image}}">


                    @endif
                </div>
                <!--col-->
            </div>

            <div class="form-group row">
                {{ Form::label('description', trans('validation.attributes.backend.access.pages.description'), ['class' => 'col-md-2 from-control-label ']) }}

                <div class="col-md-10">
                    @if(isset($pagedata))
                        <textarea class="form-control" name="description_p2">{{$data_p2->desc}}</textarea>
                    @else
                        <textarea class="form-control" name="description_p2"></textarea>
                    @endif
                </div>
                <!--col-->
            </div><br><br>


            <h4>WHAT WE STAND FOR</h4><br><br>

            <div class="form-group row">
                {{ Form::label('description_p2', trans('validation.attributes.backend.access.pages.description'), ['class' => 'col-md-2 from-control-label ']) }}

                <div class="col-md-10">
                    @if(isset($pagedata))
                        <textarea class="form-control" name="description_p3">{{$data_p3->desc}}</textarea>
                    @else
                        <textarea class="form-control" name="description_p3"></textarea>
                    @endif
                </div>
                <!--col-->
            </div><br><br>
            <div class="form-group row">
                <div class="col-md-2">
                    Sub Heading-1
                </div>
                @if(isset($pagedata))    
                    <div class="col-md-10">
                        <input type="text"  value="{{$data_p3->sub_head1}}" name="sub_head1" class="form-control">
                    </div> 
                @else
                    <div class="col-md-10">
                        <input type="text"   name="sub_head1" class="form-control">
                    </div> 
                @endif
            </div><br><br>
            <div class="form-group row">
                <div class="col-md-2">
                    Sub Description-1
                </div>
                @if(isset($pagedata))    
                    <div class="col-md-10">
                        <textarea class="form-control" name="sub_desc1">{{$data_p3->sub_desc1}}</textarea>
                    </div> 
                @else
                    <div class="col-md-10">
                        <textarea class="form-control" name="sub_desc1"></textarea>
                    </div> 
                @endif
            </div><br><br>
            <div class="form-group row">
                <div class="col-md-2">
                    Sub Heading-2
                </div>
                @if(isset($pagedata))    
                    <div class="col-md-10">
                        <input type="text"  value="{{$data_p3->sub_head2}}" name="sub_head2" class="form-control">
                    </div> 
                @else
                    <div class="col-md-10">
                        <input type="text"   name="sub_head2" class="form-control">
                    </div> 
                @endif
            </div><br><br>
            
            <div class="form-group row">
                <div class="col-md-2">
                    Sub Description-2
                </div>
                @if(isset($pagedata))    
                    <div class="col-md-10">
                        <textarea class="form-control" name="sub_desc2">{{$data_p3->sub_desc2}}</textarea>
                    </div> 
                @else
                    <div class="col-md-10">
                        <textarea class="form-control" name="sub_desc2"></textarea>
                    </div> 
                @endif
            </div><br><br>
            <div class="form-group row">
                <div class="col-md-2">
                    Sub Heading-3
                </div>
                @if(isset($pagedata))    
                    <div class="col-md-10">
                        <input type="text"  value="{{$data_p3->sub_head3}}" name="sub_head3" class="form-control">
                    </div> 
                @else
                    <div class="col-md-10">
                        <input type="text"   name="sub_head3" class="form-control">
                    </div> 
                @endif
            </div><br><br>
            
            <div class="form-group row">
                <div class="col-md-2">
                    Sub Description-3
                </div>
                @if(isset($pagedata))    
                    <div class="col-md-10">
                        <textarea class="form-control" name="sub_desc3">{{$data_p3->sub_desc3}}</textarea>
                    </div> 
                @else
                    <div class="col-md-10">
                        <textarea class="form-control" name="sub_desc3"></textarea>
                    </div> 
                @endif
            </div><br><br>
            <div class="form-group row">
                <div class="col-md-2">
                    Sub Heading-4
                </div>
                @if(isset($pagedata))    
                    <div class="col-md-10">
                        <input type="text"  value="{{$data_p3->sub_head4}}" name="sub_head4" class="form-control">
                    </div> 
                @else
                    <div class="col-md-10">
                        <input type="text"   name="sub_head4" class="form-control">
                    </div> 
                @endif
            </div><br><br>
            
            <div class="form-group row">
                <div class="col-md-2">
                    Sub Description-4
                </div>
                @if(isset($pagedata))    
                    <div class="col-md-10">
                        <textarea class="form-control" name="sub_desc4">{{$data_p3->sub_desc4}}</textarea>
                    </div> 
                @else
                    <div class="col-md-10">
                        <textarea class="form-control" name="sub_desc4"></textarea>
                    </div> 
                @endif
            </div><br><br>

            <h4>GIVING BACK</h4><br><br>

            <div class="form-group row">
                <div class="col-md-2">
                    Heading
                </div>

                @if(isset($pagedata))    
                    <div class="col-md-10">
                        <input type="text"  value="{{$data_p4->heading}}" name="heading_3" class="form-control">
                    </div> 
                @else
                    <div class="col-md-10">
                        <input type="text"   name="heading_3" class="form-control">
                    </div> 
                @endif
                
            </div>
            
            <div class="form-group row">
                <div class="col-md-2">
                    Upload
                </div>

                <div class="col-md-10">
                    <input type="file" name="image_p4" class="form-control">

                    @if(isset($pagedata))    
                        <br><img width="100" height="100" src="{{$page->url}}{{$data_p4->image}}">

                    @endif
                </div>
            </div>

            <div class="form-group row">
                {{ Form::label('description', trans('validation.attributes.backend.access.pages.description'), ['class' => 'col-md-2 from-control-label ']) }}

                <div class="col-md-10">
                    @if(isset($pagedata))
                        <textarea class="form-control" name="description_p4">{{$data_p4->desc}}</textarea>
                    @else
                        <textarea class="form-control" name="description_p4"></textarea>
                    @endif
                </div>
                <!--col-->
            </div><br><br>


            <h4>WISEBERRY FOUNDATION</h4><br><br>

            <div class="form-group row">
                <div class="col-md-2">
                    Heading
                </div>

                @if(isset($pagedata))    
                    <div class="col-md-10">
                        <input type="text"  value="{{$data_p5->heading}}" name="heading_4" class="form-control">
                    </div> 
                @else
                    <div class="col-md-10">
                        <input type="text"   name="heading_4" class="form-control">
                    </div> 
                @endif
                
            </div>
            
            <div class="form-group row">
                <div class="col-md-2">
                    Upload
                </div>

                <div class="col-md-10">
                    <input type="file" name="image_p5" class="form-control">

                    @if(isset($pagedata))    
                        <br><img width="100" height="100" src="{{$page->url}}{{$data_p5->image}}">

                    @endif
                </div>
            </div>

            <div class="form-group row">
                <div class="col-md-2">
                    Button Link
                </div>

                @if(isset($pagedata))    
                    <div class="col-md-10">
                        <input type="text"  value="{{$data_p5->button_link}}" name="button_link_p5" class="form-control">
                    </div> 
                @else
                    <div class="col-md-10">
                        <input type="text"   name="button_link_p5" class="form-control">
                    </div> 
                @endif
                
            </div>

            
            <!--form-group-->

            <div class="form-group row">
                {{ Form::label('description', trans('validation.attributes.backend.access.pages.description'), ['class' => 'col-md-2 from-control-label ']) }}

                <div class="col-md-10">
                    @if(isset($pagedata))
                        <textarea class="form-control" name="description_p5">{{$data_p5->desc}}</textarea>
                    @else
                        <textarea class="form-control" name="description_p5"></textarea>
                    @endif
                </div>
                <!--col-->
            </div><br><br>
        </div>
        <!--col-->
    </div>
    <!--row-->
</div>
<!--card-body-->

@section('pagescript')
<script type="text/javascript">
    FTX.Utils.documentReady(function() {
        FTX.Pages.edit.init("{{ config('locale.languages.' . app()->getLocale())[1] }}");
    });
</script>
@stop