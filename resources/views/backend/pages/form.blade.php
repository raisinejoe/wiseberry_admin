<div class="card-body">
    <div class="row">
        <div class="col-sm-5">
            <h4 class="card-title mb-0">
                {{ __('labels.backend.access.pages.management') }}
                <small class="text-muted">{{ (isset($page)) ? __('labels.backend.access.pages.edit') : __('labels.backend.access.pages.create') }}</small>
            </h4>
        </div>
        <!--col-->
    </div>
    <!--row-->

    <hr>

    <div class="row mt-4 mb-4">

        <div class="col">
            
            <div class="form-group row">
                <div class="col-md-2">
                    Title
                </div>
                @if(isset($pagedata))    
                    <div class="col-md-10">
                        <input type="text" value="{{$pagedata->title}}"  name="title" class="form-control">
                    </div> 
                @else
                    <div class="col-md-10">
                        <input type="text"  name="title" class="form-control">
                    </div> 
                @endif
            </div><br><br>
            <div class="form-group row">
                <div class="col-md-2">
                    Meta Title
                </div>
                @if(isset($pagedata))    
                    <div class="col-md-10">
                        <input type="text" value="{{$pagedata->meta_title}}" name="meta_title" class="form-control">
                    </div> 
                @else
                    <div class="col-md-10">
                        <input type="text"  name="meta_title" class="form-control">
                    </div> 
                @endif
            </div><br><br>
            <div class="form-group row">
                <div class="col-md-2">
                    Meta Keywords
                </div>
                @if(isset($pagedata))    
                    <div class="col-md-10">
                        <input type="text" value="{{$pagedata->meta_keywords}}" name="meta_keywords" class="form-control">
                    </div> 
                @else
                    <div class="col-md-10">
                        <input type="text"  name="meta_keywords" class="form-control">
                    </div> 
                @endif
            </div><br><br>
            <div class="form-group row">
                <div class="col-md-2">
                    Meta Description
                </div>
                @if(isset($pagedata))    
                    <div class="col-md-10">
                        <textarea name="meta_desc" class="form-control">{{$pagedata->meta_desc}}</textarea> 
                    </div> 
                @else
                    <div class="col-md-10">
                        <input type="text"  name="meta_desc" class="form-control">
                    </div> 
                @endif
            </div>
            <div class="form-group row">
                <div class="col-md-2">
                    Banner
                </div>

                <div class="col-md-10">
                    <input type="file" name="image_banner" class="form-control">

                    @if(isset($pagedata))    
                        <br><img width="100" height="100" src="{{$page->url}}{{$pagedata->banner}}">

                    @endif
                </div>
                <!--col-->
            </div>
            <br><br>


            <h4>FEATURED PROPERTIES</h4><br><br>

            <div class="form-group row">
                <div class="col-md-2">
                    Image-1
                </div>

                <div class="col-md-10">
                    <input type="file" name="image_fp1" class="form-control">

                    @if(isset($pagedata))    
                        <br><img width="100" height="100" src="{{$page->url}}{{$data_fp->image_1 ?? ''}}">
                    @endif
                </div>
                <!--col-->
            </div>

            <div class="form-group row">
                <div class="col-md-2">
                    Address-1
                </div>

                @if(isset($pagedata))    
                    <div class="col-md-10">
                        <input type="text"  value="{{$data_fp->add_1 ?? ''}}" name="add_1" class="form-control">
                    </div> 
                @else
                    <div class="col-md-10">
                        <input type="text"   name="add_1" class="form-control">
                    </div> 
                @endif
                
            </div>

            <div class="form-group row">
                <div class="col-md-2">
                    Features
                </div>

                @if(isset($pagedata))    
                    <div class="col-md-3">
                        <input type="text"  value="{{$data_fp->bed_1 ?? ''}}" name="bed_1" placeholder="Bed" class="form-control">
                    </div>
                    <div class="col-md-3">
                        <input type="text"  value="{{$data_fp->bath_1 ?? ''}}" name="bath_1" placeholder="Bath" class="form-control">
                    </div>
                    <div class="col-md-3">
                        <input type="text"  value="{{$data_fp->car_1 ?? ''}}" name="car_1" placeholder="Car" class="form-control">
                    </div> 
                @else
                    <div class="col-md-10">
                        <input type="text"   name="bed_1" class="form-control">
                        <input type="text"   name="bath_1" class="form-control">
                        <input type="text"   name="car_1" class="form-control">
                    </div> 
                @endif
                
            </div>


            <div class="form-group row">
                <div class="col-md-2">
                    Date
                </div>

                @if(isset($pagedata))    
                    <div class="col-md-3">
                        <input type="text"  value="{{$data_fp->date_1 ?? ''}}" name="date_1" placeholder="Date" class="form-control">
                    </div> 
                @else
                    <div class="col-md-10">
                        <input type="text"   name="date_1" class="form-control">
                    </div> 
                @endif
                
            </div>



            <div class="form-group row">
                <div class="col-md-2">
                    Image-2
                </div>

                <div class="col-md-10">
                    <input type="file" name="image_fp2" class="form-control">

                    @if(isset($pagedata))    
                        <br><img width="100" height="100" src="{{$page->url}}{{$data_fp->image_2 ?? ''}}">
                    @endif
                </div>
                <!--col-->
            </div>

            <div class="form-group row">
                <div class="col-md-2">
                    Address-2
                </div>

                @if(isset($pagedata))    
                    <div class="col-md-10">
                        <input type="text"  value="{{$data_fp->add_2 ?? ''}}" name="add_2" class="form-control">
                    </div> 
                @else
                    <div class="col-md-10">
                        <input type="text"   name="add_2" class="form-control">
                    </div> 
                @endif
                
            </div>

            <div class="form-group row">
                <div class="col-md-2">
                    Features-2
                </div>

                @if(isset($pagedata))    
                    <div class="col-md-3">
                        <input type="text"  value="{{$data_fp->bed_2 ?? ''}}" name="bed_2" placeholder="Bed" class="form-control">
                    </div>
                    <div class="col-md-3">
                        <input type="text"  value="{{$data_fp->bath_2 ?? ''}}" name="bath_2" placeholder="Bath" class="form-control">
                    </div>
                    <div class="col-md-3">
                        <input type="text"  value="{{$data_fp->car_2 ?? ''}}" name="car_2" placeholder="Car" class="form-control">
                    </div> 
                @else
                    <div class="col-md-10">
                        <input type="text"   name="bed_2" class="form-control">
                        <input type="text"   name="bath_2" class="form-control">
                        <input type="text"   name="car_2" class="form-control">
                    </div> 
                @endif
                
            </div>


            <div class="form-group row">
                <div class="col-md-2">
                    Date-2
                </div>

                @if(isset($pagedata))    
                    <div class="col-md-3">
                        <input type="text"  value="{{$data_fp->date_2 ?? ''}}" name="date_2" placeholder="Date" class="form-control">
                    </div> 
                @else
                    <div class="col-md-10">
                        <input type="text"   name="date_2" class="form-control">
                    </div> 
                @endif
                
            </div>




            <div class="form-group row">
                <div class="col-md-2">
                    Image-3
                </div>

                <div class="col-md-10">
                    <input type="file" name="image_fp3" class="form-control">

                    @if(isset($pagedata))    
                        <br><img width="100" height="100" src="{{$page->url}}{{$data_fp->image_3 ?? ''}}">
                    @endif
                </div>
                <!--col-->
            </div>

            <div class="form-group row">
                <div class="col-md-2">
                    Address-3
                </div>

                @if(isset($pagedata))    
                    <div class="col-md-10">
                        <input type="text"  value="{{$data_fp->add_3 ?? ''}}" name="add_3" class="form-control">
                    </div> 
                @else
                    <div class="col-md-10">
                        <input type="text"   name="add_3" class="form-control">
                    </div> 
                @endif
                
            </div>

            <div class="form-group row">
                <div class="col-md-2">
                    Features-3
                </div>

                @if(isset($pagedata))    
                    <div class="col-md-3">
                        <input type="text"  value="{{$data_fp->bed_3 ?? ''}}" name="bed_3" placeholder="Bed" class="form-control">
                    </div>
                    <div class="col-md-3">
                        <input type="text"  value="{{$data_fp->bath_3 ?? ''}}" name="bath_3" placeholder="Bath" class="form-control">
                    </div>
                    <div class="col-md-3">
                        <input type="text"  value="{{$data_fp->car_3 ?? ''}}" name="car_3" placeholder="Car" class="form-control">
                    </div> 
                @else
                    <div class="col-md-10">
                        <input type="text"   name="bed_3" class="form-control">
                        <input type="text"   name="bath_3" class="form-control">
                        <input type="text"   name="car_3" class="form-control">
                    </div> 
                @endif
                
            </div>


            <div class="form-group row">
                <div class="col-md-2">
                    Date-3
                </div>

                @if(isset($pagedata))    
                    <div class="col-md-3">
                        <input type="text"  value="{{$data_fp->date_3 ?? ''}}" name="date_3" placeholder="Date" class="form-control">
                    </div> 
                @else
                    <div class="col-md-10">
                        <input type="text"   name="date_3" class="form-control">
                    </div> 
                @endif
                
            </div>




            <h4>FEATURED RENTAL PROPERTIES</h4><br><br>

            <div class="form-group row">
                <div class="col-md-2">
                    Image-1
                </div>

                <div class="col-md-10">
                    <input type="file" name="image_fr1" class="form-control">

                    @if(isset($pagedata))    
                        <br><img width="100" height="100" src="{{$page->url}}{{$data_fr->image_1 ?? ''}}">
                    @endif
                </div>
                <!--col-->
            </div>

            <div class="form-group row">
                <div class="col-md-2">
                    Address-1
                </div>

                @if(isset($pagedata))    
                    <div class="col-md-10">
                        <input type="text"  value="{{$data_fr->add_1 ?? ''}}" name="add_r1" class="form-control">
                    </div> 
                @else
                    <div class="col-md-10">
                        <input type="text"   name="add_r1" class="form-control">
                    </div> 
                @endif
                
            </div>

            <div class="form-group row">
                <div class="col-md-2">
                    Features
                </div>

                @if(isset($pagedata))    
                    <div class="col-md-3">
                        <input type="text"  value="{{$data_fr->bed_1 ?? ''}}" name="bed_r1" placeholder="Bed" class="form-control">
                    </div>
                    <div class="col-md-3">
                        <input type="text"  value="{{$data_fr->bath_1 ?? ''}}" name="bath_r1" placeholder="Bath" class="form-control">
                    </div>
                    <div class="col-md-3">
                        <input type="text"  value="{{$data_fr->car_1 ?? ''}}" name="car_r1" placeholder="Car" class="form-control">
                    </div> 
                @else
                    <div class="col-md-10">
                        <input type="text"   name="bed_r1" class="form-control">
                        <input type="text"   name="bath_r1" class="form-control">
                        <input type="text"   name="car_r1" class="form-control">
                    </div> 
                @endif
                
            </div>


            <div class="form-group row">
                <div class="col-md-2">
                    Date
                </div>

                @if(isset($pagedata))    
                    <div class="col-md-3">
                        <input type="text"  value="{{$data_fr->date_1 ?? ''}}" name="date_r1" placeholder="Date" class="form-control">
                    </div> 
                @else
                    <div class="col-md-10">
                        <input type="text"   name="date_r1" class="form-control">
                    </div> 
                @endif
                
            </div>



            <div class="form-group row">
                <div class="col-md-2">
                    Image-2
                </div>

                <div class="col-md-10">
                    <input type="file" name="image_fr2" class="form-control">

                    @if(isset($pagedata))    
                        <br><img width="100" height="100" src="{{$page->url}}{{$data_fr->image_2 ?? ''}}">
                    @endif
                </div>
                <!--col-->
            </div>

            <div class="form-group row">
                <div class="col-md-2">
                    Address-2
                </div>

                @if(isset($pagedata))    
                    <div class="col-md-10">
                        <input type="text"  value="{{$data_fr->add_2 ?? ''}}" name="add_r2" class="form-control">
                    </div> 
                @else
                    <div class="col-md-10">
                        <input type="text"   name="add_r2" class="form-control">
                    </div> 
                @endif
                
            </div>

            <div class="form-group row">
                <div class="col-md-2">
                    Features-2
                </div>

                @if(isset($pagedata))    
                    <div class="col-md-3">
                        <input type="text"  value="{{$data_fr->bed_2 ?? ''}}" name="bed_r2" placeholder="Bed" class="form-control">
                    </div>
                    <div class="col-md-3">
                        <input type="text"  value="{{$data_fr->bath_2 ?? ''}}" name="bath_r2" placeholder="Bath" class="form-control">
                    </div>
                    <div class="col-md-3">
                        <input type="text"  value="{{$data_fr->car_2 ?? ''}}" name="car_r2" placeholder="Car" class="form-control">
                    </div> 
                @else
                    <div class="col-md-10">
                        <input type="text"   name="bed_r2" class="form-control">
                        <input type="text"   name="bath_r2" class="form-control">
                        <input type="text"   name="car_r2" class="form-control">
                    </div> 
                @endif
                
            </div>


            <div class="form-group row">
                <div class="col-md-2">
                    Date-2
                </div>

                @if(isset($pagedata))    
                    <div class="col-md-3">
                        <input type="text"  value="{{$data_fr->date_2 ?? ''}}" name="date_r2" placeholder="Date" class="form-control">
                    </div> 
                @else
                    <div class="col-md-10">
                        <input type="text"   name="date_r2" class="form-control">
                    </div> 
                @endif
                
            </div>




            <div class="form-group row">
                <div class="col-md-2">
                    Image-3
                </div>

                <div class="col-md-10">
                    <input type="file" name="image_fr3" class="form-control">

                    @if(isset($pagedata))    
                        <br><img width="100" height="100" src="{{$page->url}}{{$data_fr->image_3 ?? ''}}">
                    @endif
                </div>
                <!--col-->
            </div>

            <div class="form-group row">
                <div class="col-md-2">
                    Address-3
                </div>

                @if(isset($pagedata))    
                    <div class="col-md-10">
                        <input type="text"  value="{{$data_fr->add_3 ?? ''}}" name="add_r3" class="form-control">
                    </div> 
                @else
                    <div class="col-md-10">
                        <input type="text"   name="add_r3" class="form-control">
                    </div> 
                @endif
                
            </div>

            <div class="form-group row">
                <div class="col-md-2">
                    Features-3
                </div>

                @if(isset($pagedata))    
                    <div class="col-md-3">
                        <input type="text"  value="{{$data_fr->bed_3 ?? ''}}" name="bed_r3" placeholder="Bed" class="form-control">
                    </div>
                    <div class="col-md-3">
                        <input type="text"  value="{{$data_fr->bath_3 ?? ''}}" name="bath_r3" placeholder="Bath" class="form-control">
                    </div>
                    <div class="col-md-3">
                        <input type="text"  value="{{$data_fr->car_3 ?? ''}}" name="car_r3" placeholder="Car" class="form-control">
                    </div> 
                @else
                    <div class="col-md-10">
                        <input type="text"   name="bed_r3" class="form-control">
                        <input type="text"   name="bath_r3" class="form-control">
                        <input type="text"   name="car_r3" class="form-control">
                    </div> 
                @endif
                
            </div>


            <div class="form-group row">
                <div class="col-md-2">
                    Date-3
                </div>

                @if(isset($pagedata))    
                    <div class="col-md-3">
                        <input type="text"  value="{{$data_fr->date_3 ?? ''}}" name="date_r3" placeholder="Date" class="form-control">
                    </div> 
                @else
                    <div class="col-md-10">
                        <input type="text"   name="date_r3" class="form-control">
                    </div> 
                @endif
                
            </div>



            <h4>Why Wiseberry?</h4><br><br>

            <div class="form-group row">
                <div class="col-md-2">
                    Heading
                </div>

                @if(isset($pagedata))    
                    <div class="col-md-10">
                        <input type="text"  value="{{$data->heading}}" name="heading_1" class="form-control">
                    </div> 
                @else
                    <div class="col-md-10">
                        <input type="text"   name="heading_1" class="form-control">
                    </div> 
                @endif
                
            </div>
            
            <div class="form-group row">
                <div class="col-md-2">
                    Upload
                </div>

                <div class="col-md-10">
                    <input type="file" name="image" class="form-control">

                    @if(isset($pagedata))    
                        <br><img width="100" height="100" src="{{$page->url}}{{$data->image}}">

                        <input type="hidden" name="image_1" value="{{$data->image}}">
                        <input type="hidden" name="image_2" value="{{$data_ofc->image}}">
                        <input type="hidden" name="image_3" value="{{$pagedata->banner}}">

                        <input type="hidden" name="image_4" value="{{$data_fp->image_1}}">
                        <input type="hidden" name="image_5" value="{{$data_fp->image_2}}">
                        <input type="hidden" name="image_6" value="{{$data_fp->image_3}}">

                        <input type="hidden" name="image_7" value="{{$data_fr->image_1}}">
                        <input type="hidden" name="image_8" value="{{$data_fr->image_2}}">
                        <input type="hidden" name="image_9" value="{{$data_fr->image_3}}">

                    @endif
                </div>
                <!--col-->
            </div>

            <div class="form-group row">
                <div class="col-md-2">
                    Button Link (Find out more)
                </div>

                @if(isset($pagedata))    
                    <div class="col-md-10">
                        <input type="text"  value="{{$data->button_link}}" name="button_link" class="form-control">
                    </div> 
                @else
                    <div class="col-md-10">
                        <input type="text"   name="button_link" class="form-control">
                    </div> 
                @endif
                
            </div>
            <!--form-group-->

            <div class="form-group row">
                {{ Form::label('description', trans('validation.attributes.backend.access.pages.description'), ['class' => 'col-md-2 from-control-label ']) }}

                <div class="col-md-10">
                    {{ Form::textarea('description', null, ['class' => 'form-control', 'placeholder' => trans('validation.attributes.backend.access.pages.description')]) }}
                </div>
                <!--col-->
            </div><br><br>


            <h4>Open An Office</h4><br><br>

            <div class="form-group row">
                <div class="col-md-2">
                    Heading
                </div>

                @if(isset($pagedata))    
                    <div class="col-md-10">
                        <input type="text"  value="{{$data_ofc->heading}}" name="heading_2" class="form-control">
                    </div> 
                @else
                    <div class="col-md-10">
                        <input type="text"   name="heading_2" class="form-control">
                    </div> 
                @endif
                
            </div>

            <div class="form-group row">
                <div class="col-md-2">
                    Sub-Heading
                </div>

                @if(isset($pagedata))    
                    <div class="col-md-10">
                        <input type="text"  value="{{$data_ofc->sub_heading}}" name="heading_3" class="form-control">
                    </div> 
                @else
                    <div class="col-md-10">
                        <input type="text"   name="heading_3" class="form-control">
                    </div> 
                @endif
                
            </div>

            <div class="form-group row">
                <div class="col-md-2">
                    Upload
                </div>

                <div class="col-md-10">
                    <input type="file" name="image_ofc" class="form-control">

                    @if(isset($pagedata))    
                        <br><img width="100" height="100" src="{{$page->url}}{{$data_ofc->image}}">
                    @endif
                </div>
                <!--col-->
            </div>

            <div class="form-group row">
                <div class="col-md-2">
                    Button Link (Find out more)
                </div>

                @if(isset($pagedata))    
                    <div class="col-md-10">
                        <input type="text" value="{{$data_ofc->button_link}}" name="button_link_ofc" class="form-control">
                    </div> 
                @else
                    <div class="col-md-10">
                        <input type="text"  name="button_link_ofc" class="form-control">
                    </div> 
                @endif
                
            </div>
            <!--form-group-->

            <div class="form-group row">
                {{ Form::label('description_ofc', trans('validation.attributes.backend.access.pages.description'), ['class' => 'col-md-2 from-control-label ']) }}

                <div class="col-md-10">
                    @if(isset($pagedata))
                        <textarea class="form-control" name="description_ofc">{{$data_ofc->desc}}</textarea>
                    @else
                        <textarea class="form-control" name="description_ofc"></textarea>
                    @endif
                </div>
                <!--col-->
            </div><br><br>
        </div>
        <!--col-->
    </div>
    <!--row-->
</div>
<!--card-body-->

@section('pagescript')
<script type="text/javascript">
    FTX.Utils.documentReady(function() {
        FTX.Pages.edit.init("{{ config('locale.languages.' . app()->getLocale())[1] }}");
    });
</script>
@stop