<div class="card-body">
    <div class="row">
        <div class="col-sm-5">
            <h4 class="card-title mb-0">
                {{ __('labels.backend.access.pages.management') }}
                <small class="text-muted">{{ (isset($page)) ? __('labels.backend.access.pages.edit') : __('labels.backend.access.pages.create') }}</small>
            </h4>
        </div>
        <!--col-->
    </div>
    <!--row-->

    <hr>

    <div class="row mt-4 mb-4">

        <div class="col">
            
            <div class="form-group row">
                <div class="col-md-2">
                    Title
                </div>
                @if(isset($pagedata))    
                    <div class="col-md-10">
                        <input type="text" value="{{$pagedata->title}}"  name="title" class="form-control">
                    </div> 
                @else
                    <div class="col-md-10">
                        <input type="text"  name="title" class="form-control">
                    </div> 
                @endif
            </div><br><br>
            <div class="form-group row">
                <div class="col-md-2">
                    Meta Title
                </div>
                @if(isset($pagedata))    
                    <div class="col-md-10">
                        <input type="text" value="{{$pagedata->meta_title}}" name="meta_title" class="form-control">
                    </div> 
                @else
                    <div class="col-md-10">
                        <input type="text"  name="meta_title" class="form-control">
                    </div> 
                @endif
            </div><br><br>
            <div class="form-group row">
                <div class="col-md-2">
                    Meta Keywords
                </div>
                @if(isset($pagedata))    
                    <div class="col-md-10">
                        <input type="text" value="{{$pagedata->meta_keywords}}" name="meta_keywords" class="form-control">
                    </div> 
                @else
                    <div class="col-md-10">
                        <input type="text"  name="meta_keywords" class="form-control">
                    </div> 
                @endif
            </div><br><br>
            <div class="form-group row">
                <div class="col-md-2">
                    Meta Description
                </div>
                @if(isset($pagedata))    
                    <div class="col-md-10">
                        <textarea name="meta_desc" class="form-control">{{$pagedata->meta_desc}}</textarea> 
                    </div> 
                @else
                    <div class="col-md-10">
                        <input type="text"  name="meta_desc" class="form-control">
                    </div> 
                @endif
            </div><br><br>
            
            <div class="form-group row">
                <div class="col-md-2">
                    Banner
                </div>

                <div class="col-md-10">
                    <input type="file" name="image_banner" class="form-control">

                    @if(isset($pagedata))    
                        <br><img width="100" height="100" src="{{$page->url}}{{$pagedata->banner}}">

                    @endif
                </div>
                <!--col-->
            </div>

            <h4>WHY MANAGE WITH US?</h4><br><br>

            <div class="form-group row">
                <div class="col-md-2">
                    Heading
                </div>

                @if(isset($pagedata))    
                    <div class="col-md-10">
                        <input type="text"  value="{{$data_p1->heading}}" name="heading_1" class="form-control">
                    </div> 
                @else
                    <div class="col-md-10">
                        <input type="text"   name="heading_1" class="form-control">
                    </div> 
                @endif
                
            </div>
            
            <div class="form-group row">
                <div class="col-md-2">
                    Upload
                </div>

                <div class="col-md-10">
                    <input type="file" name="image_p1" class="form-control">

                    @if(isset($pagedata))    
                        <br><img width="100" height="100" src="{{$page->url}}{{$data_p1->image}}">

                        <input type="hidden" name="image_1" value="{{$data_p1->image}}">
                        <input type="hidden" name="image_2" value="{{$data_p2->image}}">
                        <input type="hidden" name="image_4" value="{{$data_p4->image}}">
                        <input type="hidden" name="image_3" value="{{$pagedata->banner}}">

                    @endif
                </div>
                <!--col-->
            </div>

            

            <div class="form-group row">
                {{ Form::label('description', trans('validation.attributes.backend.access.pages.description'), ['class' => 'col-md-2 from-control-label ']) }}

                <div class="col-md-10">
                    @if(isset($pagedata))
                        <textarea class="form-control" name="description_p1">{{$data_p1->desc}}</textarea>
                    @else
                        <textarea class="form-control" name="description_p1"></textarea>
                    @endif
                </div>
                <!--col-->
            </div><br><br>


            <h4>WHY MANAGE WITH US? - Section-2</h4><br><br>

            <div class="form-group row">
                <div class="col-md-2">
                    Upload
                </div>

                <div class="col-md-10">
                    <input type="file" name="image_p2" class="form-control">

                    @if(isset($pagedata))    
                        <br><img width="100" height="100" src="{{$page->url}}{{$data_p2->image}}">
                    @endif
                </div>
            </div>

            <div class="form-group row">
                <div class="col-md-2">
                    Button Link (Find out more)
                </div>

                @if(isset($pagedata))    
                    <div class="col-md-10">
                        <input type="text" value="{{$data_p2->button_link}}" name="button_link_p2" class="form-control">
                    </div> 
                @else
                    <div class="col-md-10">
                        <input type="text"  name="button_link_p2" class="form-control">
                    </div> 
                @endif
                
            </div>
            <!--form-group-->

            <div class="form-group row">
                {{ Form::label('description_ofc', trans('validation.attributes.backend.access.pages.description'), ['class' => 'col-md-2 from-control-label ']) }}

                <div class="col-md-10">
                    @if(isset($pagedata))
                        <textarea class="form-control" name="description_p2">{{$data_p2->desc}}</textarea>
                    @else
                        <textarea class="form-control" name="description_p2"></textarea>
                    @endif
                </div>
                <!--col-->
            </div><br><br>
            <h4>THE WISEBERRY EXPERIENCE</h4><br><br>

            <div class="form-group row">
                {{ Form::label('description', trans('validation.attributes.backend.access.pages.description'), ['class' => 'col-md-2 from-control-label ']) }}

                <div class="col-md-10">
                    @if(isset($pagedata))
                        <textarea class="form-control" name="description_p3">{{$data_p3->desc}}</textarea>
                    @else
                        <textarea class="form-control" name="description_p3"></textarea>
                    @endif
                </div>
            </div>
            <div class="form-group row">
                <label for="Tooltip-1" class="col-md-2 from-control-label ">Tooltip-1</label>
                <div class="col-md-10">

                    @if(isset($pagedata))
                        <textarea class="form-control" name="description_t1">{{$data_p3->desc_t1 ?? ''}}</textarea>
                    @else
                        <textarea class="form-control" name="description_t1"></textarea>
                    @endif
                </div>
            </div>
            <div class="form-group row">
                <label for="Tooltip-1" class="col-md-2 from-control-label ">Tooltip-2</label>
                <div class="col-md-10">

                    @if(isset($pagedata))
                        <textarea class="form-control" name="description_t2">{{$data_p3->desc_t2 ?? ''}}</textarea>
                    @else
                        <textarea class="form-control" name="description_t2"></textarea>
                    @endif
                </div>
            </div>
            <div class="form-group row">
                <label for="Tooltip-1" class="col-md-2 from-control-label ">Tooltip-3</label>
                <div class="col-md-10">

                    @if(isset($pagedata))
                        <textarea class="form-control" name="description_t3">{{$data_p3->desc_t3 ?? ''}}</textarea>
                    @else
                        <textarea class="form-control" name="description_t3"></textarea>
                    @endif
                </div>
            </div>
            <div class="form-group row">
                <label for="Tooltip-1" class="col-md-2 from-control-label ">Tooltip-4</label>
                <div class="col-md-10">

                    @if(isset($pagedata))
                        <textarea class="form-control" name="description_t4">{{$data_p3->desc_t4 ?? ''}}</textarea>
                    @else
                        <textarea class="form-control" name="description_t4"></textarea>
                    @endif
                </div>
            </div>
            <div class="form-group row">
                <label for="Tooltip-1" class="col-md-2 from-control-label ">Tooltip-5</label>
                <div class="col-md-10">

                    @if(isset($pagedata))
                        <textarea class="form-control" name="description_t5">{{$data_p3->desc_t5 ?? ''}}</textarea>
                    @else
                        <textarea class="form-control" name="description_t5"></textarea>
                    @endif
                </div>
            </div>
            <div class="form-group row">
                <label for="Tooltip-1" class="col-md-2 from-control-label ">Tooltip-6</label>
                <div class="col-md-10">

                    @if(isset($pagedata))
                        <textarea class="form-control" name="description_t6">{{$data_p3->desc_t6 ?? ''}}</textarea>
                    @else
                        <textarea class="form-control" name="description_t6"></textarea>
                    @endif
                </div>
            </div>
            <div class="form-group row">
                <label for="Tooltip-1" class="col-md-2 from-control-label ">Tooltip-7</label>
                <div class="col-md-10">

                    @if(isset($pagedata))
                        <textarea class="form-control" name="description_t7">{{$data_p3->desc_t7 ?? ''}}</textarea>
                    @else
                        <textarea class="form-control" name="description_t7"></textarea>
                    @endif
                </div>
            </div>
            <div class="form-group row">
                <label for="Tooltip-1" class="col-md-2 from-control-label ">Tooltip-8</label>
                <div class="col-md-10">

                    @if(isset($pagedata))
                        <textarea class="form-control" name="description_t8">{{$data_p3->desc_t8 ?? ''}}</textarea>
                    @else
                        <textarea class="form-control" name="description_t8"></textarea>
                    @endif
                </div>
            </div>
            
            

            <h4>MAKING THE CHANGE</h4><br><br>

            <div class="form-group row">
                <div class="col-md-2">
                    Heading
                </div>

                @if(isset($pagedata))    
                    <div class="col-md-10">
                        <input type="text"  value="{{$data_p4->heading}}" name="heading_2" class="form-control">
                    </div> 
                @else
                    <div class="col-md-10">
                        <input type="text"   name="heading_2" class="form-control">
                    </div> 
                @endif
                
            </div>

            <div class="form-group row">
                <div class="col-md-2">
                    Upload
                </div>

                <div class="col-md-10">
                    <input type="file" name="image_p4" class="form-control">

                    @if(isset($pagedata))    
                        <br><img width="100" height="100" src="{{$page->url}}{{$data_p4->image}}">
                    @endif
                </div>
            </div>

            <div class="form-group row">
                <div class="col-md-2">
                    Button Link 
                </div>

                @if(isset($pagedata))    
                    <div class="col-md-10">
                        <input type="text" value="{{$data_p4->button_link}}" name="button_link_p4" class="form-control">
                    </div> 
                @else
                    <div class="col-md-10">
                        <input type="text"  name="button_link_p4" class="form-control">
                    </div> 
                @endif
                
            </div>
            <!--form-group-->

            <div class="form-group row">
                {{ Form::label('description_ofc', trans('validation.attributes.backend.access.pages.description'), ['class' => 'col-md-2 from-control-label ']) }}

                <div class="col-md-10">
                    @if(isset($pagedata))
                        <textarea class="form-control" name="description_p4">{{$data_p4->desc}}</textarea>
                    @else
                        <textarea class="form-control" name="description_p4"></textarea>
                    @endif
                </div>
                <!--col-->
            </div><br><br>

            
        </div>
        <!--col-->
    </div>
    <!--row-->
</div>
<!--card-body-->

@section('pagescript')
<script type="text/javascript">
    FTX.Utils.documentReady(function() {
        FTX.Pages.edit.init("{{ config('locale.languages.' . app()->getLocale())[1] }}");
    });
</script>
@stop