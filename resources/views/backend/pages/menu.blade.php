<div class="card-body">
    <div class="row">
        <div class="col-sm-5">
            <h4 class="card-title mb-0">
                {{ __('labels.backend.access.pages.management') }}
                <small class="text-muted">{{ (isset($page)) ? __('labels.backend.access.pages.edit') : __('labels.backend.access.pages.create') }}</small>
            </h4>
        </div>
        <!--col-->
    </div>
    <!--row-->

    <hr>

    <div class="row mt-4 mb-4">

        <div class="col">
            
            <div class="form-group row">
                    <div class="col-md-2">
                        Title
                    </div>
                @if(isset($pagedata))    
                    <div class="col-md-10">
                        <input type="text" value="Menu" name="title" class="form-control">
                    </div> 
                @else
                    <div class="col-md-10">
                        <input type="text" value="Menu" name="title" class="form-control">
                    </div> 
                @endif
                </div><br><br>
            <?php  $i=1; ?>
            @foreach($pagedata->menus as $key=>$value)    
            <div class="form-group row">
                <div class="col-md-3">
                    <input type="hidden" value="{{$value->title}}" name="a{{$i}}" class="form-control">
                    <h4>{{$value->title}}</h4>
                </div>
                <!-- <div class="col-md-6">
                    <input type="text" value="http://szwebprofile.com/PHP/wiseberry/public/buy" name="b{{$i}}" class="form-control">
                </div> -->
            </div>
                <?php  $j=1; ?>
                <?php if(isset($value->submenu)){ ?>
                @foreach($value->submenu as $keys=>$values)
                    <div class="form-group row" style="margin-left: 5% !important;">
                        <div class="col-md-3">
                            <input type="text" value="{{$values->title}}" name="{{$i}}a{{$j}}"   class="form-control">
                        </div>
                        <div class="col-md-6">
                            <input type="text" value="{{$values->link}}" name="{{$i}}b{{$j}}" class="form-control">
                        </div>
                    </div>
                    <?php  $j++; ?>
                @endforeach
                <?php }  $i++; ?>
            @endforeach
            <!-- <div class="form-group row" style="margin-left: 5% !important;">
                <div class="col-md-3">
                    <input type="text" value="Search" name="button_link" class="form-control">
                </div>
                <div class="col-md-6">
                    <input type="text" value="http://szwebprofile.com/PHP/wiseberry/public/search" name="button_link" class="form-control">
                </div>
            </div>
            <div class="form-group row" style="margin-left: 5% !important;">
                <div class="col-md-3">
                    <input type="text" value="Open Homes" name="button_link" class="form-control">
                </div>
                <div class="col-md-6">
                    <input type="text" value="http://szwebprofile.com/PHP/wiseberry/public/open-homes" name="button_link" class="form-control">
                </div>
            </div> -->
            
        </div>
        <!--col-->
    </div>
    <!--row-->
</div>
<!--card-body-->

@section('pagescript')
<script type="text/javascript">
    FTX.Utils.documentReady(function() {
        FTX.Pages.edit.init("{{ config('locale.languages.' . app()->getLocale())[1] }}");
    });
</script>
@stop