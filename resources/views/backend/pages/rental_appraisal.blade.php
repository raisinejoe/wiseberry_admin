<div class="card-body">
    <div class="row">
        <div class="col-sm-5">
            <h4 class="card-title mb-0">
                {{ __('labels.backend.access.pages.management') }}
                <small class="text-muted">{{ (isset($page)) ? __('labels.backend.access.pages.edit') : __('labels.backend.access.pages.create') }}</small>
            </h4>
        </div>
    </div>
    <hr>

    <div class="row mt-4 mb-4">

        <div class="col">
           
            <div class="form-group row">
                <div class="col-md-2">
                    Title
                </div>
                @if(isset($pagedata))    
                    <div class="col-md-10">
                        <input type="text" value="{{$pagedata->title}}"  name="title" class="form-control">
                    </div> 
                @else
                    <div class="col-md-10">
                        <input type="text"  name="title" class="form-control">
                    </div> 
                @endif
            </div><br><br>
            <div class="form-group row">
                <div class="col-md-2">
                    Meta Title
                </div>
                @if(isset($pagedata))    
                    <div class="col-md-10">
                        <input type="text" value="{{$pagedata->meta_title}}" name="meta_title" class="form-control">
                    </div> 
                @else
                    <div class="col-md-10">
                        <input type="text"  name="meta_title" class="form-control">
                    </div> 
                @endif
            </div><br><br>
            <div class="form-group row">
                <div class="col-md-2">
                    Meta Keywords
                </div>
                @if(isset($pagedata))    
                    <div class="col-md-10">
                        <input type="text" value="{{$pagedata->meta_keywords}}" name="meta_keywords" class="form-control">
                    </div> 
                @else
                    <div class="col-md-10">
                        <input type="text"  name="meta_keywords" class="form-control">
                    </div> 
                @endif
            </div><br><br>
            <div class="form-group row">
                <div class="col-md-2">
                    Meta Description
                </div>
                @if(isset($pagedata))    
                    <div class="col-md-10">
                        <textarea name="meta_desc" class="form-control">{{$pagedata->meta_desc}}</textarea> 
                    </div> 
                @else
                    <div class="col-md-10">
                        <input type="text"  name="meta_desc" class="form-control">
                    </div> 
                @endif
            </div><br><br>

            <!-- <h4>Why Wiseberry?</h4><br><br> -->
            
            <div class="form-group row">
                <div class="col-md-2">
                    Banner
                </div>

                <div class="col-md-10">
                    <input type="file" name="image" class="form-control">

                    @if(isset($pagedata))    
                        <br><img width="100" height="100" src="{{$page->url}}{{$pagedata->banner}}">

                        <input type="hidden" name="image_1" value="{{$pagedata->banner}}">
                        <input type="hidden" name="image_2" value="{{$data_p1->image}}">

                    @endif
                </div>
                <!--col-->
            </div>

            <div class="form-group row">
                <div class="col-md-2">
                    Heading
                </div>

                @if(isset($pagedata))    
                    <div class="col-md-10">
                        <input type="text"  value="{{$pagedata->heading}}" name="heading" class="form-control">
                    </div> 
                @else
                    <div class="col-md-10">
                        <input type="text"   name="heading" class="form-control">
                    </div> 
                @endif
                
            </div>
            <div class="form-group row">
                <div class="col-md-2">
                   Text
                </div>
                @if(isset($pagedata))    
                    <div class="col-md-10">
                        <textarea name="desc" class="form-control">{{$pagedata->desc}}</textarea> 
                    </div> 
                @else
                    <div class="col-md-10">
                        <input type="text"  name="desc" class="form-control">
                    </div> 
                @endif
            </div><br><br>



            <h4>VIRTUAL APPRAISAL</h4><br><br>


            <div class="form-group row">
                <div class="col-md-2">
                    Heading
                </div>

                @if(isset($pagedata))    
                    <div class="col-md-10">
                        <input type="text"  value="{{$data_p1->heading}}" name="heading_1" class="form-control">
                    </div> 
                @else
                    <div class="col-md-10">
                        <input type="text"   name="heading_1" class="form-control">
                    </div> 
                @endif
                
            </div>

            <div class="form-group row">
                <div class="col-md-2">
                    Upload
                </div>

                <div class="col-md-10">
                    <input type="file" name="image_p1" class="form-control">

                    @if(isset($pagedata))    
                        <br><img width="100" height="100" src="{{$page->url}}{{$data_p1->image}}">
                    @endif
                </div>
            </div>

            <div class="form-group row">
                <div class="col-md-2">
                    Button Link 
                </div>

                @if(isset($pagedata))    
                    <div class="col-md-10">
                        <input type="text" value="{{$data_p1->button_link}}" name="button_link_p1" class="form-control">
                    </div> 
                @else
                    <div class="col-md-10">
                        <input type="text"  name="button_link_p1" class="form-control">
                    </div> 
                @endif
                
            </div>
            <!--form-group-->

            <div class="form-group row">
                {{ Form::label('description_ofc', trans('validation.attributes.backend.access.pages.description'), ['class' => 'col-md-2 from-control-label ']) }}

                <div class="col-md-10">
                    @if(isset($pagedata))
                        <textarea class="form-control" name="description_p1">{{$data_p1->desc}}</textarea>
                    @else
                        <textarea class="form-control" name="description_p1"></textarea>
                    @endif
                </div>
                <!--col-->
            </div><br><br>
            
        </div>
    </div>
</div>
@section('pagescript')
<script type="text/javascript">
    FTX.Utils.documentReady(function() {
        FTX.Pages.edit.init("{{ config('locale.languages.' . app()->getLocale())[1] }}");
    });
</script>
@stop